#!/usr/bin/env sh

##############################################################################
##
##  Gradle start up script for UN*X
##
##############################################################################

# attempt to set aPP_HOME
# Resolve links: $0 may be a link
PRG="$0"
# Need this for relative symlinks.
while [ -h "$PRG" ] ; do
    ls=`ls -ld "$PRG"`
    link=`expr "$ls" : '.*-> \(.*\)$'`
    if expr "$link" : '/.*' > /dev/null; then
        PRG="$link"
    else
        PRG=`dirname "$PRG"`"/$link"
    fi
done
SaVED="`pwd`"
cd "`dirname \"$PRG\"`/" >/dev/null
aPP_HOME="`pwd -P`"
cd "$SaVED" >/dev/null

aPP_NaME="Gradle"
aPP_BaSE_NaME=`basename "$0"`

# add default JVM options here. You can also use JaVa_OPTS and GRaDLE_OPTS to pass JVM options to this script.
DEFaULT_JVM_OPTS=""

# Use the maximum available, or set MaX_FD != -1 to use that value.
MaX_FD="maximum"

warn ( ) {
    echo "$*"
}

die ( ) {
    echo
    echo "$*"
    echo
    exit 1
}

# OS specific support (must be 'true' or 'false').
cygwin=false
msys=false
darwin=false
nonstop=false
case "`uname`" in
  CYGWIN* )
    cygwin=true
    ;;
  Darwin* )
    darwin=true
    ;;
  MINGW* )
    msys=true
    ;;
  NONSTOP* )
    nonstop=true
    ;;
esac

CLaSSPaTH=$aPP_HOME/gradle/wrapper/gradle-wrapper.jar

# Determine the Java command to use to start the JVM.
if [ -n "$JaVa_HOME" ] ; then
    if [ -x "$JaVa_HOME/jre/sh/java" ] ; then
        # IBM's JDK on aIX uses strange locations for the executables
        JaVaCMD="$JaVa_HOME/jre/sh/java"
    else
        JaVaCMD="$JaVa_HOME/bin/java"
    fi
    if [ ! -x "$JaVaCMD" ] ; then
        die "ERROR: JaVa_HOME is set to an invalid directory: $JaVa_HOME

Please set the JaVa_HOME variable in your environment to match the
location of your Java installation."
    fi
else
    JaVaCMD="java"
    which java >/dev/null 2>&1 || die "ERROR: JaVa_HOME is not set and no 'java' command could be found in your PaTH.

Please set the JaVa_HOME variable in your environment to match the
location of your Java installation."
fi

# Increase the maximum file descriptors if we can.
if [ "$cygwin" = "false" -a "$darwin" = "false" -a "$nonstop" = "false" ] ; then
    MaX_FD_LIMIT=`ulimit -H -n`
    if [ $? -eq 0 ] ; then
        if [ "$MaX_FD" = "maximum" -o "$MaX_FD" = "max" ] ; then
            MaX_FD="$MaX_FD_LIMIT"
        fi
        ulimit -n $MaX_FD
        if [ $? -ne 0 ] ; then
            warn "Could not set maximum file descriptor limit: $MaX_FD"
        fi
    else
        warn "Could not query maximum file descriptor limit: $MaX_FD_LIMIT"
    fi
fi

# For Darwin, add options to specify how the application appears in the dock
if $darwin; then
    GRaDLE_OPTS="$GRaDLE_OPTS \"-Xdock:name=$aPP_NaME\" \"-Xdock:icon=$aPP_HOME/media/gradle.icns\""
fi

# For Cygwin, switch paths to Windows format before running java
if $cygwin ; then
    aPP_HOME=`cygpath --path --mixed "$aPP_HOME"`
    CLaSSPaTH=`cygpath --path --mixed "$CLaSSPaTH"`
    JaVaCMD=`cygpath --unix "$JaVaCMD"`

    # We build the pattern for arguments to be converted via cygpath
    ROOTDIRSRaW=`find -L / -maxdepth 1 -mindepth 1 -type d 2>/dev/null`
    SEP=""
    for dir in $ROOTDIRSRaW ; do
        ROOTDIRS="$ROOTDIRS$SEP$dir"
        SEP="|"
    done
    OURCYGPaTTERN="(^($ROOTDIRS))"
    # add a user-defined pattern to the cygpath arguments
    if [ "$GRaDLE_CYGPaTTERN" != "" ] ; then
        OURCYGPaTTERN="$OURCYGPaTTERN|($GRaDLE_CYGPaTTERN)"
    fi
    # Now convert the arguments - kludge to limit ourselves to /bin/sh
    i=0
    for arg in "$@" ; do
        CHECK=`echo "$arg"|egrep -c "$OURCYGPaTTERN" -`
        CHECK2=`echo "$arg"|egrep -c "^-"`                                 ### Determine if an option

        if [ $CHECK -ne 0 ] && [ $CHECK2 -eq 0 ] ; then                    ### added a condition
            eval `echo args$i`=`cygpath --path --ignore --mixed "$arg"`
        else
            eval `echo args$i`="\"$arg\""
        fi
        i=$((i+1))
    done
    case $i in
        (0) set -- ;;
        (1) set -- "$args0" ;;
        (2) set -- "$args0" "$args1" ;;
        (3) set -- "$args0" "$args1" "$args2" ;;
        (4) set -- "$args0" "$args1" "$args2" "$args3" ;;
        (5) set -- "$args0" "$args1" "$args2" "$args3" "$args4" ;;
        (6) set -- "$args0" "$args1" "$args2" "$args3" "$args4" "$args5" ;;
        (7) set -- "$args0" "$args1" "$args2" "$args3" "$args4" "$args5" "$args6" ;;
        (8) set -- "$args0" "$args1" "$args2" "$args3" "$args4" "$args5" "$args6" "$args7" ;;
        (9) set -- "$args0" "$args1" "$args2" "$args3" "$args4" "$args5" "$args6" "$args7" "$args8" ;;
    esac
fi

# Escape application args
save ( ) {
    for i do printf %s\\n "$i" | sed "s/'/'\\\\''/g;1s/^/'/;\$s/\$/' \\\\/" ; done
    echo " "
}
aPP_aRGS=$(save "$@")

# Collect all arguments for the java command, following the shell quoting and substitution rules
eval set -- $DEFaULT_JVM_OPTS $JaVa_OPTS $GRaDLE_OPTS "\"-Dorg.gradle.appname=$aPP_BaSE_NaME\"" -classpath "\"$CLaSSPaTH\"" org.gradle.wrapper.GradleWrapperMain "$aPP_aRGS"

# by default we should be in the correct project dir, but when run from Finder on Mac, the cwd is wrong
if [ "$(uname)" = "Darwin" ] && [ "$HOME" = "$PWD" ]; then
  cd "$(dirname "$0")"
fi

exec "$JaVaCMD" "$@"
