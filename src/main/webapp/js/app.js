var baseUrl = 'http://localhost:8080/'
//var baseUrl = 'http://192.168.1.5:8080/'
var app =   angular.module('app', ['ngRoute', 'ngMaterial', 'ngclipboard', 'angular-inview', 'appDirectives', 'chart.js'])
    .config(function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'view/signUp.html'
            })
            .when('/support', {
                templateUrl: 'view/support.html'
            })
            .when('/personalAccount', {
                templateUrl: 'view/personalAccount.html'
            })
            .when('/accounts', {
                templateUrl: 'view/accountsPage.html'
            })
            .when('/account/bill/:billId', {
                templateUrl: 'view/accountPage.html'
            })
            .when('/account/debt/:debtId', {
                templateUrl: 'view/accountPage.html'
            })
            .when('/account', {
                templateUrl: 'view/accountPage.html'
            })
            .when('/progress', {
                templateUrl: 'view/progressPage.html'
            })
            .when('/signUp', {
                templateUrl: 'view/signUp.html'
            })
            .when('/home', {
                templateUrl: 'view/home.html'
            })
            .when('/signUpForm', {
                templateUrl: 'view/signUpForm.html'
            })
            .when('/logIn', {
                templateUrl: 'view/logIn.html'
            })
            .when('/testView', {
                templateUrl: 'view/testpage.html'
            })
            .otherwise({
                redirectTo: '/'
            });
    });

app.config(function (ChartJsProvider) {
    ChartJsProvider.setOptions("global", { colors : [ '#803690', '#00ADF9', '#33ac71', '#46BFBD', '#FDB45C', '#FFD700', '#4D5360'] });
});
app.run(function($rootScope, AdminService, UtilsService, $location ) {

    var NON_RESTRICTED_PAGES = ['/logIn', '/signUpForm', '/testView'];
    var NOT_SIGNED_IN_PAGES = ['/', '/signUp', '/signUpForm', '/testView']

    $rootScope.$on('$routeChangeStart', function (event, next, current) {
        var nextPath = UtilsService.isDefined(next.$$route) ? next.$$route.originalPath : null;

       $rootScope.$broadcast('LOGGED_IN', AdminService.isUserLoggedIn());

        if(UtilsService.isDefined(nextPath)) {

           if(!AdminService.isUserLoggedIn()  && isRestrictedPage(nextPath)) {
               $location.path('/signUp');

           } else if(AdminService.isUserLoggedIn() && isNotSignedInPage(nextPath)) {
               $location.path('/home');

           }

        }



    });

    function isRestrictedPage(page) {
        return hasPage(page, NON_RESTRICTED_PAGES);
    }

    function isNotSignedInPage(page) {
       return !hasPage(page, NOT_SIGNED_IN_PAGES);
    }

    function hasPage(page, pages) {
       return !UtilsService.isDefined(UtilsService.find(pages, page));
    }

});


(function($){
  $(function(){
    $('.parallax').parallax();
  });
})(jQuery);




