app.factory('BankAccountService', function($http, $log, UtilsService, ServiceUtils) {
    var genericDescription = 'Check, credit, and debit information regarding this bank';
    var baseUri = 'banking/';


    var floridaAccount = {
        displayName: 'CCU of Florida',
        description: genericDescription,
        userName: 'myUserName',
        password: 'password',
        debitCard: createCard('David Turner', 5555555555555, '03/20', 111, 'Visa'),
        creditCard: createCard('David S Turner', 55555555555, '03/20', 111, 'Visa'),
        checkInfo: createCheckInfo(55555555, 555555555)
    };

    var ccuAccount = {
        displayName: 'Communit America Credit Union',
        description: genericDescription,
        userName: 'myUserName',
        password: 'myPassword!',
        debitCard: createCard('David S Turner', 55555555555, '02/20', 111, 'Visa'),
        checkInfo: createCheckInfo(555555555, 555555555555)
    };

    var academyAccount = {
        displayName: 'Academy Bank',
        userName: 'N/A',
        password: 'N/A',
        description: genericDescription,
        debitCard: createCard('David Turner', 555555555, '03/20', 111, 'Visa'),
    };

    function createCard(name, number, exp, cvv, cardType) {
        return {
            name: name,
            number: number,
            exp: exp,
            cvv: cvv,
            cardType: cardType,
        };
    }

    function createCheckInfo(routingNum, accNum) {
        return {
            routingNumber: routingNum,
            accountNumber: accNum
        };
    }
    return {

        getBankAccs: function() {
            return [
                floridaAccount, ccuAccount, academyAccount
            ]
        },

        getFloridaAcc: function() {
            return floridaAccount;
        },

        getAcademyAcc: function() {
            return academyAccount;
        },

        getCcuAcc: function() {
            return ccuAccount;
        },

        retrieveBankAccounts: function(personalAccountId) {
            var method = "BankService.retrieveAllBankAccs(): ";
            var accounts = [];
            $log.log(method + "request recieved!");
             var promise = $http.get(baseUrl + baseUri + 'findAllBanks/' + personalAccountId).success(function(response) {
                 $log.log(method + "Service Success...");

                return response;

            }).error(function(response) {
                 $log.debug(method + "Service Fail...");
                 return response;
             });


            return promise;

        },

        createBankAccount: function(bankAccount) {
            var method = "BankService.createBankAccount(): ";
            $log.log(method + "request recieved!");
             var promise = $http.post(baseUrl + baseUri + 'create/bankAcc', bankAccount).success(function(response) {
                 $log.log(method + "Service Success...");

                return response;

            }).error(function(response) {
                 $log.debug(method + "Service Fail...");
                 return response;
             });


            return promise;
        },

        updateBankAccount: function(bankAccount) {
            var method = "BankService.updateBankAccount(): ";
            $log.log(method + "request recieved!");
             var promise = $http.put(baseUrl + baseUri + 'update/bankAcc', bankAccount).success(function(response) {
                 $log.log(method + "Service Success...");

                return response;

            }).error(function(response) {
                 $log.debug(method + "Service Fail...");
                 return response;
             });


            return promise;
        },

        deleteBankAccount: function(bankAccId) {
            var method = "BankService.deleteBankAcc(): ";
            $log.log(method + "request recieved!");
             var promise = $http.delete(baseUrl + baseUri + 'delete/bankAcc?bankId=' + bankAccId).success(function(response) {
                 $log.log(method + "Service Success...");

                return response;

            }).error(function(response) {
                 $log.debug(method + "Service Fail...");
                 return response;
             });


            return promise;
        },

        deleteCard: function(cardId) {
            var method = "BankService.deleteCard(): ";
            $log.log(method + "request recieved!");
             var promise = $http.delete(baseUrl + baseUri + 'delete/card?cardId=' + cardId).success(function(response) {
                 $log.log(method + "Service Success...");

                return response;

            }).error(function(response) {
                 $log.debug(method + "Service Fail...");
                 return response;
             });


            return promise;
        },


        createCard: function(card) {
            var method = "BankService.createCard(): ";
            $log.log(method + "request recieved!");
             var promise = $http.post(baseUrl + baseUri + 'create/card', card).success(function(response) {
                 $log.log(method + "Service Success...");

                return response;

            }).error(function(response) {
                 $log.debug(method + "Service Fail...");
                 return response;
             });


            return promise;
        },

        updateCard: function(card) {
            var method = "BankService.updateCard(): ";
            $log.log(method + "request recieved!");
             var promise = $http.put(baseUrl + baseUri + 'update/card', card).success(function(response) {
                 $log.log(method + "Service Success...");

                return response;

            }).error(function(response) {
                 $log.debug(method + "Service Fail...");
                 return response;
             });


            return promise;
        }


    };


});