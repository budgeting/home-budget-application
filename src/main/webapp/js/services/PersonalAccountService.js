app.factory('PersonalAccountService', function($http, $log, UtilsService, ServiceUtils) {
    var baseUri = 'personalAccount/';

    return {

        findAccount: function(id) {
            var method = "PersonalAccountService.findAccount(): ";
            var accounts = [];
            $log.log(method + "request received! Id: [" + id + "]");
             var promise = $http.get(baseUrl + baseUri + 'find?id=' + id).success(function(response) {
                 $log.log(method + "Service Success...");

                return response;

            }).error(function(response) {
                 $log.debug(method + "Service Fail...");
                 return response;
             });


            return promise;

        },


        login: function(accountReq) {
            var method = "PersonalAccountService.login(): ";

            $log.log(method + "request received! Email: [" + accountReq.email + "]");
             var promise = $http.post(baseUrl + baseUri + 'login', accountReq).success(function(response) {
                 $log.log(method + "Service Success...");

                return response;

            }).error(function(response) {
                 $log.debug(method + "Service Fail...");
                 return response;
             });


            return promise;


        },

        findAccountByEmail: function(email) {
            var method = "PersonalAccountService.findAccountByEmail(): ";
            var accounts = [];
            $log.log(method + "request received! email: [" + email + "]");
             var promise = $http.put(baseUrl + baseUri + 'findByEmail/', {email: email}).success(function(response) {
                 $log.log(method + "Service Success...");

                return response;

            }).error(function(response) {
                 $log.debug(method + "Service Fail...");
                 return response;
             });


            return promise;

        },

        createAccount: function(account) {
            var method = "PersonalAccountService.findAccount(): ";
            var accounts = [];
            $log.log(method + "request received!");
             var promise = $http.post(baseUrl + baseUri + 'create', account).success(function(response) {
                 $log.log(method + "Service Success...");

                return response;

            }).error(function(response) {
                 $log.debug(method + "Service Fail...");
                 return response;
             });


            return promise;

        },

        updateAccount: function(account) {
            var method = "PersonalAccountService.updateAccount(): ";
            var accounts = [];
            $log.log(method + "request received!");
             var promise = $http.put(baseUrl + baseUri + 'update', account).success(function(response) {
                 $log.log(method + "Service Success...");

                return response;

            }).error(function(response) {
                 $log.debug(method + "Service Fail...");
                 return response;
             });


            return promise;
        },

        updateAccountPassword: function(password, id) {
            var method = "PersonalAccountService.updateAccountPassword(): ";
            var accounts = [];
            $log.log(method + "request received!");
             var promise = $http.put(baseUrl + baseUri + 'updatePassword', {password: password, accountId: id}).success(function(response) {
                 $log.log(method + "Service Success...");

                return response;

            }).error(function(response) {
                 $log.debug(method + "Service Fail...");
                 return response;
             });


            return promise;
        },

        updateAccountEarnings: function(monthlyEarnings, id) {
            var method = "PersonalAccountService.updateAccountEarnings(): ";
            var accounts = [];
            $log.log(method + "request received!");
             var promise = $http.put(baseUrl + baseUri + 'updateEarnings', {monthlyEarnings: monthlyEarnings, accountId: id}).success(function(response) {
                 $log.log(method + "Service Success...");

                return response;

            }).error(function(response) {
                 $log.debug(method + "Service Fail...");
                 return response;
             });


            return promise;
        },

        updateAccountFinances: function(savePercentage, id) {
            var method = "PersonalAccountService.updateAccountFinances(): ";
            var accounts = [];
            $log.log(method + "request received!");
             var promise = $http.put(baseUrl + baseUri + 'updateFinances', {savePercentage: savePercentage, accountId: id}).success(function(response) {
                 $log.log(method + "Service Success...");

                return response;

            }).error(function(response) {
                 $log.debug(method + "Service Fail...");
                 return response;
             });


            return promise;
        },
        deleteAccount: function(id) {
            var method = "PersonalAccountService.deleteAccount(): ";
            var accounts = [];
            $log.log(method + "request received! Id: [" + id + "]");
             var promise = $http.delete(baseUrl + baseUri + 'delete?id=' + id).success(function(response) {
                 $log.log(method + "Service Success...");

                return response;

            }).error(function(response) {
                 $log.debug(method + "Service Fail...");
                 return response;
             });


            return promise;
        }
    };


});