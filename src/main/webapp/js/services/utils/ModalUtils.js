app.factory('ModalUtils', function($log, UtilsService) {

    return {

         openModal: function(modal) {

            (function ($) {
                $(function () {
                    //initialize all modals
                    $('.modal').modal();
                    $(modal).modal('open');
                });

            })(jQuery);
            $('select').material_select();



        },

        closeModal :function(modal) {
           $(modal).modal('close');

        }

    };


});