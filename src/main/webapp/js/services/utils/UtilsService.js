app.factory('UtilsService', function () {
   
   function isObjDefined(obj) {
       return !_.isNull(obj)  && ! _.isUndefined(obj);
   };
   
   
   return {
       isDefined: function(obj) {
           return isObjDefined(obj); 
       },
       
       removeItemFromList: function(list, obj) {
           if(isObjDefined(list) && !_.isEmpty(list)) {
               
               var index = list.indexOf(obj);
		
				if(index > -1) {
                    list.splice(index, 1);
				}
           }
           
           return list;
       },
       
       isListEmpty: function(list) {
           if(isObjDefined(list)) {
               return _.isEmpty(list);
           }
         
           return true;
       },
       
       stringMatch: function(string1, string2) {
           return isObjDefined(string1) && isObjDefined(string2) && _.isEqual(string1, string2);
       },

       validateRegularExpression: function(regExp, string) {
            return isObjDefined(string) ? regExp.test(string) : false;
       },
       
       isStringEmpty: function(string) {
		   var isEmpty = true;
		   if(isObjDefined(string) && _.isString(string)) {
			   isEmpty = _.isEmpty(string);
		   }
		   
		   return isEmpty;
       },
	   
	   trimCharacters: function(string, characterCount) {
		   var trimText = string;
		   if(isObjDefined(string) && _.isString(string)) {
			   
			   if(string.length > characterCount) {
				   trimText = string.substring(0, characterCount) + '...'
			   }
		   }
 
		   return trimText;
	   },

	   find: function(list, value) {
	        return  _.find(list, function(item){ return _.isEqual(value, item); });
	   },

     //Map<Key, List<Value>>
     addListItemToMap: function(map, key, value) {
        if(isObjDefined(map)) {

          var mapValue = map[key];

          if(isObjDefined(mapValue)) {
            mapValue.push(value);
            
          } else {
            map[key] = [value];
          }

        }
     }
       
   };
   
   
});
