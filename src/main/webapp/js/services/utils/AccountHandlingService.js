app.factory('AccountHandlingService', function($log, UtilsService) {
    var account = {};
    var accountTransactionMessage = '';


      function printMessage(message) {
          Materialize.toast(message, 4000)
      }

    return {

        getAccount: function() {
            return account;
        },

        getAccountType: function(acc) {
            return UtilsService.isDefined(acc) && UtilsService.isDefined(acc.interest) ? "Debt" : "Billing";
        },

        setAccount: function(acc) {
            account = acc;
        },

        isDebtAcc: function(acc) {
            return UtilsService.isDefined(acc) && UtilsService.isDefined(acc.interest) ;
        },

        setAccountTransactionMessage: function(message) {
            accountTransactionMessage = message;
        },

        displayRecentAccountTransaction: function() {
            if(UtilsService.isDefined(accountTransactionMessage) && !UtilsService.isStringEmpty(accountTransactionMessage)) {
                printMessage(accountTransactionMessage);
                accountTransactionMessage = '';
            }
        }

    };


});