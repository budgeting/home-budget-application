app.factory('ServiceUtils', function ( UtilsService ) {


    return {

        hasPayload: function(response) {
            return UtilsService.isDefined(response) && UtilsService.isDefined(response.payload);
        },

        isSuccessful: function(response) {
            return UtilsService.isDefined(response) && response.status === 'OK';
        },


    };





});
