app.factory('BillingService', function($http, $log, UtilsService, ServiceUtils) {
    var baseUri = 'billing/';

    return {

        retrieveBillingAccsByTime: function(personalAccountId) {
            var method = "BillingService.retrieveBillingAccsByTime(): ";
            var accounts = [];
            $log.log(method + "request recieved!");
             var promise = $http.get(baseUrl + baseUri + 'billingAccs/timeformat/' + personalAccountId).success(function(response) {
                 $log.log(method + "Service Success...");

                return response;

            }).error(function(response) {
                 $log.debug(method + "Service Fail...");
                 return response;
             });


            return promise;

        },

          retrieveDebtAccsByTime: function(personalAccountId) {
                var method = "BillingService.retrieveDebtAccsByTime(): ";
                var accounts = [];
                $log.log(method + "request recieved!");
                 var promise = $http.get(baseUrl + baseUri + 'debtAccs/timeformat/' + personalAccountId).success(function(response) {
                     $log.log(method + "Service Success...");

                    return response;

                }).error(function(response) {
                     $log.debug(method + "Service Fail...");
                     return response;
                 });


                return promise;

            },
              fetchDebtAcc: function(accId) {
                    var method = "BillingService.fetchDebtAcc(): ";
                    var accounts = [];
                    $log.log(method + "request recieved!");
                     var promise = $http.get(baseUrl + baseUri + 'debtAcc?accId='+accId).success(function(response) {
                         $log.log(method + "Service Success...");

                        return response;

                    }).error(function(response) {
                         $log.debug(method + "Service Fail...");
                         return response;
                     });


                    return promise;

                },

                 fetchBillAcc: function(accId) {
                    var method = "BillingService.fetchBillAcc(): ";
                    var accounts = [];
                    $log.log(method + "request recieved!");
                     var promise = $http.get(baseUrl + baseUri + 'billingAcc?accId=' +accId).success(function(response) {
                         $log.log(method + "Service Success...");

                        return response;

                    }).error(function(response) {
                         $log.debug(method + "Service Fail...");
                         return response;
                     });


                    return promise;

                },

             createBillingAcc: function(billingAcc) {
                    var method = "BillingService.createBillingAcc(): ";
                    var accounts = [];
                    $log.log(method + "request recieved!");
                     var promise = $http.post(baseUrl + baseUri + 'create/billingAcc', billingAcc).success(function(response) {
                         $log.log(method + "Service Success...");

                        return response;

                    }).error(function(response) {
                         $log.debug(method + "Service Fail...");
                         return response;
                     });


                    return promise;

                },


          updateBillingAcc: function(billingAcc) {
                var method = "BillingService.updateBillingAcc(): ";
                var accounts = [];
                $log.log(method + "request recieved!");
                 var promise = $http.put(baseUrl + baseUri + 'update/billingAcc', billingAcc).success(function(response) {
                     $log.log(method + "Service Success...");

                    return response;

                }).error(function(response) {
                     $log.debug(method + "Service Fail...");
                     return response;
                 });


                return promise;

            },

         createDebtAcc: function(billingAcc) {
                var method = "BillingService.createDebtAcc(): ";
                var accounts = [];
                $log.log(method + "request recieved!");
                 var promise = $http.post(baseUrl + baseUri + 'create/debtAcc', billingAcc).success(function(response) {
                     $log.log(method + "Service Success...");

                    return response;

                }).error(function(response) {
                     $log.debug(method + "Service Fail...");
                     return response;
                 });


                return promise;

            },


          updateDebtAcc: function(debtAcc) {
                var method = "BillingService.updateDebtAcc(): ";
                var accounts = [];
                $log.log(method + "request recieved!");
                 var promise = $http.put(baseUrl + baseUri + 'update/debtAcc', debtAcc).success(function(response) {
                     $log.log(method + "Service Success...");

                    return response;

                }).error(function(response) {
                     $log.debug(method + "Service Fail...");
                     return response;
                 });


                return promise;

            },

            deactivateDebtAcc: function(accId) {
                var method = "BankService.deactivateDebtAcc(): ";
                $log.log(method + "request recieved!");
                 var promise = $http.post(baseUrl + baseUri + 'deactivate/debtAcc?accId=' + accId).success(function(response) {
                     $log.log(method + "Service Success...");

                    return response;

                }).error(function(response) {
                     $log.debug(method + "Service Fail...");
                     return response;
                 });


                return promise;
            },

            deactivateBillingAcc: function(accId) {
                var method = "BankService.deactivateBillingAcc(): ";
                $log.log(method + "request recieved!");
                 var promise = $http.post(baseUrl + baseUri + 'deactivate/billingAcc?accId=' + accId).success(function(response) {
                     $log.log(method + "Service Success...");

                    return response;

                }).error(function(response) {
                     $log.debug(method + "Service Fail...");
                     return response;
                 });


                return promise;
            }

    };


});