app.factory('BillingCycleService', function($http, $log, UtilsService, ServiceUtils) {
    var baseUri = 'monthlyCycle/';

    return {

        findBillingCycle: function(id) {
            var method = "BillingCycleService.findBillingCycle(): ";
            $log.log(method + "request received! Id: [" + id + "]");
             var promise = $http.get(baseUrl + baseUri + 'find/' + id).success(function(response) {
                 $log.log(method + "Service Success...");

                return response;

            }).error(function(response) {
                 $log.debug(method + "Service Fail...");
                 return response;
             });


            return promise;

        },

        findBillingCycleYearRange: function(personalAccId) {
             var method = "BillingCycleService.findBillingCycleYearRange(): ";
             $log.log(method + "request received! personalAccId: [" + personalAccId + "]");
              var promise = $http.get(baseUrl + baseUri + 'findYearRange/' + personalAccId).success(function(response) {
                  $log.log(method + "Service Success...");

                 return response;

             }).error(function(response) {
                  $log.debug(method + "Service Fail...");
                  return response;
              });


             return promise;

        },

        findBillingHistoryByYear: function(personalAccId, year) {
             var method = "BillingCycleService.findBillingHistoryByYear(): ";
             $log.log(method + "request received! personalAccId: [" + personalAccId + "] year: [" + year + "]");
              var promise = $http.get(baseUrl + baseUri + 'findByYear/' + year + '/accId/' + personalAccId).success(function(response) {
                  $log.log(method + "Service Success...");

                 return response;

             }).error(function(response) {
                  $log.debug(method + "Service Fail...");
                  return response;
              });


             return promise;

         },

        createBillingCycle: function(cycle) {
            var method = "BillingCycleService.createBillingCycle(): ";
            $log.log(method + "request received! Month: [" + cycle.month + "] Year: [ " + cycle.year + "]");
             var promise = $http.post(baseUrl + baseUri + 'create/', cycle).success(function(response) {
                 $log.log(method + "Service Success...");

                return response;

            }).error(function(response) {
                 $log.debug(method + "Service Fail...");
                 return response;
             });


            return promise;

        },

        payBill: function(bill) {
            var method = "BillingCycleService.createBillingCycle(): ";
            $log.log(method + "request received! BillAuditId: [ " + bill.billAuditId + "]");

             var promise = $http.put(baseUrl + baseUri + 'payBill/', bill).success(function(response) {
                 $log.log(method + "Service Success...");

                return response;

            }).error(function(response) {
                 $log.debug(method + "Service Fail...");
                 return response;
             });


            return promise;

        },

        payDebt: function(debt) {
            var method = "BillingCycleService.payDebt(): ";
            $log.log(method + "request received! debtAuditId:  [" + debt.debtAuditId + "]");

             var promise = $http.put(baseUrl + baseUri + 'payDebt/', debt).success(function(response) {
                 $log.log(method + "Service Success...");

                return response;

            }).error(function(response) {
                 $log.debug(method + "Service Fail...");
                 return response;
             });


            return promise;

        }
    };


});