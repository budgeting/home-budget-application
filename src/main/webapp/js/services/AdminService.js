app.factory('AdminService', function($http, $log, $window, PersonalAccountService, ServiceUtils, UtilsService) {

    var USER_ID = "userId";
    var user;

   return {

        getUser: function() {
            return user;
        },

        setUser: function(acc) {
            user = acc;
            $window.sessionStorage.setItem(USER_ID, acc.accountId)
        },

        getUserId: function() {
            return $window.sessionStorage.getItem(USER_ID);
        },

        isUserLoggedIn: function() {
            return UtilsService.isDefined($window.sessionStorage.getItem(USER_ID));
        },

        login: function(accountReq) {
            user = null;

           var promise = PersonalAccountService.login(accountReq).success(function(response) {

                if(ServiceUtils.isSuccessful(response) && ServiceUtils.hasPayload(response)) {
                    user = response.payload;
                    $window.sessionStorage.setItem(USER_ID, response.payload.accountId);
                }

           });

           return promise;

        },


        logOut: function() {
            user = null;
            $window.sessionStorage.removeItem(USER_ID);
        }

   };
});