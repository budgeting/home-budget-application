app.factory('TestService', function($http, $log) {

   return {
        simpleTest: function() {
            var method = "TestService.simpleTest(): ";
            $log.log(method + "request recieved!");
             var promise = $http.get(testUrl + 'test').success(function(response) {
                 $log.log(method + "Service Success...");
                 if(response !== null && response.data !== undefined) {

                    response = response.data;

                 } else {
                     $log.debug(method + "No Projects to return...");
                 }

                 $log.log(method + "Returning payload: ");
                 $log.log(response);

                return response;

            }).error(function(response) {
                 $log.debug(method + "Service Fail...");
                 return null;
             });


            return promise;

        },
        testDbConnection: function() {
            var method = "TestService.testDbConnection(): ";
            $log.log(method + "request recieved!");
             var promise = $http.get(baseUrl + 'testDbConnection').success(function(response) {
                 $log.log(method + "Service Success...");
                 if(response !== null && response.data !== undefined) {

                    response = response.data;

                 } else {
                     $log.debug(method + "No Projects to return...");
                 }

                 $log.log(method + "Returning payload: ");
                 $log.log(response);

                return response;

            }).error(function(response) {
                 $log.debug(method + "Service Fail...");
                 return null;
             });


            return promise;

        }

   };
});