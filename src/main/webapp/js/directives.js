var widgets = angular.module('appDirectives', []);

widgets.directive('notificationMessage', function() {

	return {
        restrict: 'E',
        scope: {
            notification: '='
        },
        controller: function($scope, $log, $rootScope) {
            var Map = {};
            initType();

            $scope.getIcon = function() {
                var type = getType();
                return type !== null ? type.icon : '';
            };

            $scope.getStyle = function() {
            	var type = getType();
            	return type !== null ? type.style : '';
            }

            $scope.close = function() {
               $scope.notification.message = '';
            };



            function initType() {
            	Map['success'] = {icon: 'fa fa-check-circle' , style: 'alert alert-success'};
                Map['error'] = {icon: 'fa fa-times-circle', style: 'alert alert-danger'};
                Map['warning'] = {icon: 'fa fa-exclamation-triangle', style: 'alert alert-warning'};
            };

            function getType() {
            	var type = null;

            	if($scope.notification !== null && $scope.notification !== undefined && $scope.notification.type !== null) {

                	var mapItem = Map[$scope.notification.type];
                    type = mapItem;


                }

            	return type;
            };


        },
        templateUrl: 'widgets/notification.html'

    }

});



