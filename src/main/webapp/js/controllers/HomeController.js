app.controller('homeController', function($rootScope, $scope, $location, BillService, $log, BankAccountService, ServiceUtils, PersonalAccountService, UtilsService, $route, ModalUtils, BillingCycleService,  AdminService, ConstantsService) {
    var personalAccountId;

    init();

    function init() {

        initVars();

        initBankAccounts();
        initMonthCycle();
    }


    function initVars() {
        $rootScope.loading = true;
        $scope.bills1 = {};
        $scope.bills2 = {};
        personalAccountId = AdminService.getUserId();
        $scope.months = ConstantsService.getMonths();


    }

    function initMonthCycle() {

        var user = AdminService.getUser();

        if(UtilsService.isDefined(user) && UtilsService.isDefined(user.monthId) && user.monthId !== 0) {
            retrieveBillingCycle(user.monthId);

        } else {
            PersonalAccountService.findAccount(personalAccountId).success(function(response) {


                if(ServiceUtils.isSuccessful(response) && ServiceUtils.hasPayload(response)) {

                    AdminService.setUser(response.payload);

                    if(UtilsService.isDefined(response.payload.monthId) && response.payload.monthId !== 0) {
                        retrieveBillingCycle(response.payload.monthId);
                    }

                }




            }).error(function(response) {
                $rootScope.loading = false;
                printMessage(response.message);

            });
        }


    }




    function retrieveBillingCycle(monthId) {
        BillingCycleService.findBillingCycle(monthId).success(function(response) {

            if(ServiceUtils.hasPayload(response)) {
                $scope.month = response.payload.month;
                $scope.year = response.payload.year;


                var debts = response.payload.debts;
                var bills = response.payload.bills;

                $scope.bills1 = composeHalfOfMonth(debts.firstMonth, bills.firstMonth);
                $scope.bills2 = composeHalfOfMonth(debts.secondMonth, bills.secondMonth);
            }

            $rootScope.loading = false;


        }).error(function(response) {
            $rootScope.loading = false;
            printMessage(response.message);
        });
    }

    function composeHalfOfMonth(debts, bills) {
        return [
                   {
                       name: "Reoccurring Bills",
                       info: "Ongoing payments on a periodic basis for a product or service",

                       accounts: bills
                   },

                   {
                       name: "Outstanding Debt",
                       info: "Outstanding money owed to outside entity",

                       accounts: debts
                    }
            ];
    }

    function initBankAccounts() {
       $scope.bankAccounts = [];
       BankAccountService.retrieveBankAccounts(personalAccountId).success(function(response) {

            if(ServiceUtils.isSuccessful(response)) {

                if(ServiceUtils.hasPayload(response)) {
                    $scope.bankAccounts = response.payload.bankAccounts;
                }

            } else {
                $rootScope.setErrorNotification(response.message);
            }

            $rootScope.loading = false;

       });
    }

    $scope.createMonthlyCycle = function(cycle) {

        if(UtilsService.isDefined(cycle)) {
             $rootScope.loading = true;
                    cycle.personalAccId = personalAccountId;
                    cycle.user = personalAccountId;
                    cycle.module = 'WebApplication';

                    BillingCycleService.createBillingCycle(cycle).success(function(response) {
                        var user = AdminService.getUser();
                        AdminService.login(user).success(function(response) {
                            init();

                        }).error(function(response) {
                            $rootScope.loading = false;
                            printMessage(response.message);

                        });

                    }).error(function(response) {
                        $rootScope.loading = false;
                        printMessage(response.message);

                    });

        } else {
            printMessage('Please provide a valid billing cycle request.')
        }


    }
    
    $scope.setBankAccount = function(account) {
        resetBankView(account);
        $scope.bankAcc = account;
    }

    $scope.showAccountingData = function(personalAcc) {
        personalAcc.showAccounting = personalAcc.bankAcc === 'Yes' ? true : false;
    }

    function resetBankView(bankAcc) {
        bankAcc.editDebitView = false;
        bankAcc.showDebit = false;
        bankAcc.editCreditView = false;
        bankAcc.showCredit = false;
        bankAcc.showCheck = false;
    }


    
    $scope.goUp = function() {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
    }

    $scope.openModal = function(modal) {
        openModal(modal);
    }


    /****** Save Operations ***********/

    $scope.initNewCard = function(bankAccId) {
        $scope.newCard = {
            fullName: "",
            cardNumber: "",
            cvv: "",
            name: "",
            vendor: "",
            expirationDate: "",
            type: false,
            bankAccId: bankAccId,
            user: personalAccountId,
            module: "WebApplication"
        };
    }

    $scope.initNewPersonalAcc = function() {
        $scope.newPersonalAcc = {
            name: '',
            description: '',
            routingNumber: '',
            accountNumber: '',
            userName: '',
            password: '',
            personalAccountId: personalAccountId,
            url: '',
            user: personalAccountId,
            module: 'WebApplication',
            bankAcc: false
        };

    }

    $scope.createPersonalAcc = function(account) {
       processBasicServiceResponse(BankAccountService.createBankAccount(account), '#bankCreation');

    }

   $scope.savePersonalAcc = function(account, modal) {
       processBasicServiceResponse(BankAccountService.updateBankAccount(account), modal);
    }


   $scope.deletePersonalAcc = function(accountId) {
        processBasicServiceResponse(BankAccountService.deleteBankAccount(accountId), '#bankProfile');
    }

   $scope.deleteCard = function(cardId) {
        processBasicServiceResponse(BankAccountService.deleteCard(cardId), '#bankModal');
    }


    $scope.createNewCard = function() {
        $scope.newCard.type = getCardType($scope.newCard);
        processBasicServiceResponse(BankAccountService.createCard($scope.newCard), '#cardCreation');
    };

    $scope.updateCard = function(card) {
         processBasicServiceResponse(BankAccountService.updateCard(card), '#bankModal');
    };


    $scope.payBillingAcc = function(account) {
        if(UtilsService.isDefined(account)) {
            if(UtilsService.isDefined(account.billAuditId)) {
                payBill(account)

            } else if(UtilsService.isDefined(account.debtAuditId)) {
                payDebt(account);
            }
        }
    }

    /* -------- Validation -------- */
    $scope.validateCvv = function(cvv) {

        if(!UtilsService.stringMatch(cvv, '') && UtilsService.isDefined(cvv)) {
            return validateCvv(cvv);

        } else {
            return true;
        }
    }

    $scope.validateCardNumber = function(cardNumber) {

        if(!UtilsService.stringMatch(cardNumber, '') && UtilsService.isDefined(cardNumber)) {
            return validateCardNumber(cardNumber);

        } else {
            return true;
        }
    }

    $scope.validateExpirationDate = function(expirationDate) {

        if(!UtilsService.stringMatch(expirationDate, '') && UtilsService.isDefined(expirationDate)) {
            return validateExpirationDate(expirationDate);

        } else {
            return true;
        }
    }

   function validateCvv(cvv) {
        var regExp = new RegExp('[0-9]{3,4}');

        return UtilsService.validateRegularExpression(regExp, cvv)
   }

  function validateCardNumber(cardNumber) {
       var regExp = new RegExp('[0-9]{16}');

       return UtilsService.validateRegularExpression(regExp, cardNumber)
  }

  function validateExpirationDate(expirationDate) {
    var pattern = new RegExp("^(0[1-9]|1[0-2]|[1-9])\/(1[4-9]|[2-9][0-9]|20[1-9][1-9])$");

    return UtilsService.validateRegularExpression(pattern, expirationDate);
  }



  $scope.isValidCard = function(card) {

      var isValid = false;

      if(UtilsService.isDefined(card)) {

          isValid = !UtilsService.isStringEmpty(card.name) &&
                     !UtilsService.isStringEmpty(card.fullName) &&
                     !UtilsService.isStringEmpty(card.vendor) &&
                     validateCvv(card.cvv) && validateCardNumber(card.cardNumber) &&
                     validateExpirationDate(card.expirationDate);

      }

      return isValid;

  }


    function payBill(bill) {

        BillingCycleService.payBill({billAuditId: bill.billAuditId, personalAccId: personalAccountId}).success(function(response) {

               if(ServiceUtils.isSuccessful(response)) {
                   bill.complete = true;
                   bill.dateCompleted = response.payload.dateCompleted;

               } else {
                   printMessage(response.message);
               }


           }).error(function(response) {
               printMessage(response.message);
           });
    }

    $scope.completeAndCreateNewBillingCycle = function() {

        if(UtilsService.isDefined($scope.month)) {

            var cycle = {};

            if(UtilsService.stringMatch('December', $scope.month)) {
                cycle.month = 'January';
                cycle.year = $scope.year + 1;

            } else {
                var months = ConstantsService.getMonths();

                var index;
                for(var i = 0; i <months.length; i++) {
                    index = i;

                    if(UtilsService.stringMatch(months[i].month, $scope.month)) {
                        break;
                    }

                }


                cycle.month = months[index+1].month;
                cycle.year = $scope.year;
            }


            $scope.createMonthlyCycle(cycle);

        }

    }

    function payDebt(debt) {
        BillingCycleService.payDebt({debtAuditId: debt.debtAuditId, personalAccId: personalAccountId, amount: debt.paying}).success(function(response) {

               if(ServiceUtils.isSuccessful(response)) {
                   var payload = response.payload;

                   debt.complete = payload.complete;
                   debt.dateCompleted = payload.dateCompleted;
                   debt.paidAmount = payload.paidAmount;
                   debt.monthlyAmount = payload.monthlyAmount;


               } else {
                   printMessage(response.message);
               }


           }).error(function(response) {
               printMessage(response.message);
           });
    }

        function processBasicServiceResponse(promise, modal) {
            promise.success(function(response) {
                   closeModal(modal);

                   if(ServiceUtils.isSuccessful(response)) {
                       serviceSuccess();


                   } else {
                       printMessage(response.message);
                   }


               }).error(function(response) {
                   closeModal(modal);
                   printMessage(response.message);
               });
        }

    /*Get Card Type by boolean value - UI uses a slider (on/off) to determine card type */
    function getCardType(card) {
        return card.type == true ? 'DEBIT' : 'CREDIT';
    }

    function serviceSuccess() {
        init();
        printMessage('Success!');
    }

    function openModal(modal) {
        ModalUtils.openModal(modal);
    }

    function closeModal(modal) {
        ModalUtils.closeModal(modal);
    }

    function printMessage(message) {
        Materialize.toast(message, 4000)
    }


});


