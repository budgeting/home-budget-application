
app.controller('signUpFormController', function($rootScope, $scope, $routeParams, $log, $location, ServiceUtils, UtilsService, PersonalAccountService, AdminService) {
       init();

    function initVars() {
          $rootScope.loading = false;
          $rootScope.clearActiveNav();
     }

     function init() {
        initVars();
     }

     $scope.signUp = function(account) {
        $rootScope.loading = true;

        account.user = "Admin";
        account.module = "WebApplication";

        PersonalAccountService.createAccount(account).success(function(response) {

                if(ServiceUtils.isSuccessful(response)) {

                    AdminService.login(account).success(function(response) {
                        printMessage('Success!');
                        $location.path('/support');


                    }).error(function(response) {
                        processFailure(response);
                    })

                } else {
                  processFailure(response);

                }


            }).error(function(response) {
                processFailure(response);
            });
     }


    /* ----------------- Sign Up Validation --------------- */
      $scope.isValidPasswordConfirmation = function() {
        var password = UtilsService.isDefined($scope.account) ? $scope.account.password : '';

        if(!UtilsService.stringMatch(password, '') && UtilsService.isDefined(password)) {
            return UtilsService.stringMatch($scope.passwordConfirmation, password);

        } else {
            return true;
        }
      }

        $scope.isValidMonthlySavings = function() {
            var savePercentage = UtilsService.isDefined($scope.account) ? $scope.account.savePercentage : 0;

            if(UtilsService.isDefined(savePercentage)) {
                return savePercentage >= 0 && savePercentage <= 100;
            }

            return true;
        }

        $scope.isValidMonthlyEarnings = function() {
            var monthlyEarnings = UtilsService.isDefined($scope.account) ? $scope.account.monthlyEarnings : 0;

            if(UtilsService.isDefined(monthlyEarnings)) {
                return monthlyEarnings >= 0;
            }

            return true;

        }

        $scope.isSignUpFormValid = function() {

            var isValid = false;

            if(UtilsService.isDefined($scope.account)) {

                isValid = !UtilsService.isStringEmpty($scope.account.name) &&
                           !UtilsService.isStringEmpty($scope.account.password) &&
                           $scope.isValidPasswordConfirmation() && !UtilsService.isStringEmpty($scope.passwordConfirmation) &&
                           $scope.isValidMonthlySavings() && $scope.isValidMonthlyEarnings() && $scope.account.monthlyEarnings > 0 && $scope.account.savePercentage >= 0 &&
                           !UtilsService.isStringEmpty($scope.account.email) && !$( "#email" ).hasClass( "invalid-field" );
            }

            return isValid;

        }



     /* ------- Convenience Methods -------- */

    function printMessage(message) {
        Materialize.toast(message, 4000)
    }

    function processFailure(response) {
        $rootScope.loading = false;
        printMessage(response.message);
    }


});

