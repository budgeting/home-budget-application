
app.controller('progressController', function($rootScope, $scope, $log, $location, AdminService, BillingCycleService, ServiceUtils, UtilsService) {
    var personalAccountId;

    init();
    function init() {
        initVars();
        retrieveYearRange();
    }

    function initVars() {
        $rootScope.loading = true;
        personalAccountId = AdminService.getUserId();
    }


    function retrieveYearRange() {
        BillingCycleService.findBillingCycleYearRange(personalAccountId).success(function(response) {

            if(ServiceUtils.isSuccessful(response)) {
                var payload = response.payload;
                setYearRange(payload);
                $scope.historyYear = payload.minYear;

//                findBillingHistory(payload.minYear);

            } else {
                processFailure(response.message);
            }

        }).error(function(response) {
            processFailure(response.message);
        })

    }

    function processFailure(message) {
            $rootScope.loading = false;
            printMessage(message);
    }

    function findBillingHistory(year) {
        BillingCycleService.findBillingHistoryByYear(personalAccountId, year).success(function(response) {

            if(ServiceUtils.isSuccessful(response)) {
                setChartData(response.payload);
                $rootScope.loading = false;

            } else {
                processFailure(response.message);
            }

        }).error(function(response) {
            processFailure(response.message);
        });
    }

    function setChartData(data) {
    $scope.series = ['Bills', 'Debt', 'Leftover Income'];

    var debt = [];
    var remainingIncome = [];
    var bills = [];
    $scope.labels = [];


    $scope.totalBillAmountPaid = data.totalBillAmountPaid;
    $scope.totalDebtAmountPaid = data.totalDebtAmountPaid;

    if(UtilsService.isDefined(data) && !UtilsService.isListEmpty(data.yearlyHistory)) {

        _.each(data.yearlyHistory, function(dataItem) {
            $scope.labels.push(dataItem.month);
            debt.push(dataItem.debtExpenses);
            bills.push(dataItem.billExpenses);
            remainingIncome.push(dataItem.remainingIncome);
        });

        $scope.data = [bills, debt, remainingIncome];

    } else {
        $scope.labels = [];
        $scope.data = [];
    }
//http://jtblin.github.io/angular-chart.js/#line-chart

//
//  $scope.labels = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
//  $scope.series = ['Series A', 'Series B'];
//
//  $scope.data = [
//    [65, 59, 80, 81, 56, 55, 40],
//    [28, 48, 40, 19, 86, 27, 90]
//  ];
    }

    function printMessage(message) {
        Materialize.toast(message, 4000)
    }

    function setYearRange(yearRange) {
        $scope.minYear = yearRange.minYear;
        $scope.maxYear = yearRange.maxYear;
    }

      $scope.$watch("historyYear", function(newValue, oldValue) {
        if ($scope.historyYear > 0) {
          findBillingHistory($scope.historyYear);
        }
      });


});

