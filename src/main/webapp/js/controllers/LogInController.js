app.controller('logInController', function($rootScope, $scope, $routeParams, $log, $location, ServiceUtils, AdminService, UtilsService, $location) {
       init();

    function initVars() {
          $rootScope.loading = false;
     }

     function init() {
        initVars();
     }


     $scope.login = function(account) {
        $rootScope.loading = true;

        AdminService.login(account).success(function(response) {

               if(ServiceUtils.isSuccessful(response)) {
                   $rootScope.$broadcast('LOGGED_IN');
                   $location.path('/home');

               } else {
                   $rootScope.loading = false;
                   printMessage(response.message);
               }


           }).error(function(response) {
               $rootScope.loading = false;
               printMessage(response.message);
           });


    }

    function printMessage(message) {
        Materialize.toast(message, 4000)
    }




});

