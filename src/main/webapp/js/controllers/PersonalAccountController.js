
app.controller('personalAccountController', function($rootScope, $scope, $log, $location, ModalUtils, AdminService, PersonalAccountService, ServiceUtils, UtilsService) {
    var personalAccountId;
    init();

    function init() {
        initVars();
        loadPersonalAccount();

        $scope.newPasswordConfirmation = '';
        $scope.newPassword = '';

    }

    function resetVars() {
        $scope.newPassword = '';
        $scope.newPasswordConfirmation = '';
        personalAccountId = AdminService.getUserId();
    }

    function initVars() {
        $rootScope.loading = true;
        resetVars();
    }

    $scope.openModal = function(modal) {
        ModalUtils.openModal(modal);
    };

    function loadPersonalAccount() {
       $scope.personalAccount = [];
       PersonalAccountService.findAccount(personalAccountId).success(function(response) {

            if(ServiceUtils.isSuccessful(response)) {

                if(ServiceUtils.hasPayload(response)) {
                    setPersonalAccount(response.payload);
                }

            } else {
                printMessage(response.message);
            }

            $rootScope.loading = false;

       });
    }

    function setPersonalAccount(account) {
        $scope.personalAccount = account;

        $scope.chartLabels = ["Savings", "Bills", "Debt", "Life Expenses"];
        $scope.chartData = [account.savings, account.monthlyBillExpenses, account.monthlyDebtExpenses, account.lifeExpenses];
    }

   $scope.updatePassword = function(account, password) {
        processBasicServiceResponse(PersonalAccountService.updateAccountPassword(password, account.accountId), '#accountOverviewModal');
        resetVars();
    }

   $scope.updateEarnings = function(account) {
        processBasicServiceResponse(PersonalAccountService.updateAccountEarnings(account.monthlyEarnings, account.accountId), '#earningsModal');
    }

   $scope.updateFinances = function(account) {
        processBasicServiceResponse(PersonalAccountService.updateAccountFinances(account.savePercentage, account.accountId), '#financeModal');
    }

    /* ---- Validation ------- */
      $scope.isValidPasswordConfirmation = function() {

        if(!UtilsService.stringMatch($scope.newPasswordConfirmation, '') && UtilsService.isDefined($scope.newPasswordConfirmation)) {
            return $scope.isPasswordChangeValid();

        } else {
            return true;
        }
      }

      $scope.isPasswordChangeValid = function() {
        return UtilsService.stringMatch($scope.newPasswordConfirmation, $scope.newPassword);
      }


       $scope.isValidSavingsPercentage = function() {
         return UtilsService.isDefined($scope.personalAccount) ? ($scope.personalAccount.savePercentage >= 0 && $scope.personalAccount.savePercentage <= 100) : false;

       }

      $scope.isValidMonthlyEarnings = function() {
        return UtilsService.isDefined($scope.personalAccount) ? ($scope.personalAccount.monthlyEarnings >= 0) : false;

      }

    function updateAccountAuditDetails(account) {
        account.module = 'WebApplication';
        account.user = personalAccountId;
    }

    function processBasicServiceResponse(promise, modal) {
        promise.success(function(response) {
               closeModal(modal);

               if(ServiceUtils.isSuccessful(response)) {
                   serviceSuccess();


               } else {
                   printMessage(response.message);
               }


           }).error(function(response) {
               closeModal(modal);
               printMessage(response.message);
           });
    }

    function serviceSuccess() {
        init();
        printMessage('Success!');
    }

    function closeModal(modal) {
        ModalUtils.closeModal(modal);
    }

    function printMessage(message) {
        Materialize.toast(message, 4000)
    }




});

