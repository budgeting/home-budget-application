
app.controller('testController', function($rootScope, BillingService, $scope, $log, $location, TestService, BankAccountService, PersonalAccountService) {

      $rootScope.loading = false;

      $scope.getBills = function() {
            BillingService.retrieveBillingAccsByTime().success(function(response) {
                displayJson('Billing Accounts', response);

            });


      };

      $scope.getDebt = function() {
         BillingService.retrieveDebtAccsByTime().success(function(response) {
            displayJson('Debt Accounts', response);
         });
      };

      $scope.getBankAccounts = function() {
             BankAccountService.retrieveBankAccounts(1).success(function(response) {
                displayJson('Banking Accounts', response);
             });

      }

      $scope.getPersonalAccount = function() {
             PersonalAccountService.findAccountByEmail('turner.scott.david@gmail.com').success(function(response) {
                displayJson('Personal Account', response);
             });

      }


       function displayJson(title, json) {
           $scope.response = json;
           $scope.message = title;

           jsonIfy();
       }

      function jsonIfy() {
          $('#json').empty();
          $('#json').append(
              $('<pre>').text(
                  JSON.stringify($scope.response, null, '  ')
              )
          );
      }
document.getElementById("json").innerHTML = JSON.stringify($scope.testContent, undefined, 2);

});

