
app.controller('accountController', function(BillingService, $rootScope, $scope, $routeParams, $log, $location, ServiceUtils, UtilsService, AccountHandlingService, AdminService ) {
       init();

    function initVars() {
          $rootScope.loading = true;


     }
      function init() {
        initVars();

        loadAccount();
        $scope.accountType = AccountHandlingService.getAccountType($scope.account);



      }

      /* ---------------Modify The Account----------------- */

      $scope.saveAccount = function(account) {

         if(UtilsService.isDefined(account)) {

            if(UtilsService.isDefined(account.billAccountId) || UtilsService.isDefined(account.debtAccountId)) {
                updateAccount(account);
            } else {
                createAccount(account);
            }
         }
      }

       $scope.deactivateAccount = function(account) {
            $rootScope.loading = true;
            var promise = AccountHandlingService.isDebtAcc(account) ? BillingService.deactivateDebtAcc(account.debtAccountId) : BillingService.deactivateBillingAcc(account.billAccountId)
            processAccountUpdateAndDeactivate(promise);
      }

       function createAccount (account) {
            $rootScope.loading = true;
            var promise = AccountHandlingService.isDebtAcc(account) ? BillingService.createDebtAcc(account) : BillingService.createBillingAcc(account)
            processAccountUpdateAndDeactivate(promise);
      }

        function updateAccount(account) {
              $rootScope.loading = true;
              var promise = AccountHandlingService.isDebtAcc(account) ? BillingService.updateDebtAcc(account) : BillingService.updateBillingAcc(account)
              processAccountUpdateResponse(promise);
        }

      function processAccountUpdateAndDeactivate(promise) {
          promise.success(function(response) {
                 $rootScope.loading = false;

                 if(ServiceUtils.isSuccessful(response)) {
                     AccountHandlingService.setAccountTransactionMessage('Success!');
                     $rootScope.goTo('/accounts');


                 } else {
                     printMessage(response.message);
                 }
             }).error(function(response) {
                 $rootScope.loading = false;
                 printMessage(response.message);
             });

      }


      function processAccountUpdateResponse(promise) {
          promise.success(function(response) {
                 $rootScope.loading = false;

                 if(ServiceUtils.isSuccessful(response)) {
                     printMessage('Success!');


                 } else {
                     printMessage(response.message);
                 }
             }).error(function(response) {
                 $rootScope.loading = false;
                 printMessage(response.message);
             });
      }

      function loadAccount() {
           var account = AccountHandlingService.getAccount();

           if(!UtilsService.isDefined(account) || _.isEmpty(account)) {

                if(UtilsService.isDefined($routeParams.billId)) {
                    processAccountRetrieval(BillingService.fetchBillAcc($routeParams.billId))

                } else if(UtilsService.isDefined($routeParams.debtId)) {
                    processAccountRetrieval(BillingService.fetchDebtAcc($routeParams.debtId))

                } else {
                    routeFailureBackToAccounts(response);
                }

           } else {
                $scope.account = account;
                $rootScope.loading = false;
           }
      }

      function processAccountRetrieval(promise) {
          promise.success(function(response) {

            if(ServiceUtils.isSuccessful(response) && ServiceUtils.hasPayload(response)) {
                $scope.account = response.payload;
                AccountHandlingService.setAccount($scope.account);
                $scope.accountType = AccountHandlingService.getAccountType($scope.account);

            }

            $rootScope.loading = false;

          }).error(function(response) {

              routeFailureBackToAccounts(response);

          });
      }

      function printMessage(message) {
          Materialize.toast(message, 4000)
      }

      function routeFailureBackToAccounts(message) {
          AccountHandlingService.setAccountTransactionMessage(message);
          $rootScope.goTo('/accounts');
      }





});

