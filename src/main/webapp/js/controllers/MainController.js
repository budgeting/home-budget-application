
app.controller('mainController', function($rootScope, $scope, $log, $location, AdminService) {
    $rootScope.loading = true;
    $scope.loggedIn = false;
    $rootScope.hardLoad = true;

    $scope.goTo = function(path) {
        $rootScope.notification = {};
        $location.path(path);
    };

    $rootScope.goTo = function(path) {
        $scope.goTo(path);
    }

    $rootScope.$on('LOGGED_IN', function(event, message) {
        $scope.loggedIn = message;
        $rootScope.hardLoad = false;
    });


    $rootScope.setNotification = function(type, message) {
        $rootScope.notification = {type, message};
    };

    $rootScope.setErrorNotification = function(message) {
        $rootScope.setNotification('error', message);
        $rootScope.loading = false;
    };

    $rootScope.clearActiveNav = function() {
        $scope.activeNav = '';
    }

    $scope.logOut = function() {
        $rootScope.loading = true;
        AdminService.logOut();
        $location.path('/logIn');
    }

    function printMessage(message) {
        Materialize.toast(message, 4000)
    }

});