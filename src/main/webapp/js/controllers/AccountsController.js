
app.controller('accountsController', function(BillingService, $rootScope, $scope, $log, $location, ServiceUtils, UtilsService, AccountHandlingService, AdminService) {
      var personalAccountId;
      init();

      function init() {
        initVars();
        fetchAccounts();
      }


      function initVars() {
          $rootScope.loading = true;
          $scope.accounts = [];
          personalAccountId = AdminService.getUserId();


          $scope.stopLoading = {
            bills: false,
            debt: false
          };

          $scope.filterItem = {
            debt: true,
            bill: true,
            first: true,
            second: true,
            payment: 1000

          };

      }

      function processLoading() {
        if($scope.stopLoading && $scope.stopLoading.debt) {
            $rootScope.loading = false;

            $scope.stopLoading.bills = false;
            $scope.stopLoading.debt = false;

            //Display any possible account transaction that may have happened on the Account page.
            AccountHandlingService.displayRecentAccountTransaction();
        }
      }

      function fetchAccounts() {
        processAccountsRetrieval(BillingService.retrieveBillingAccsByTime(personalAccountId), 'bills');
        processAccountsRetrieval(BillingService.retrieveDebtAccsByTime(personalAccountId),  'debt' );

      }

      function processAccountsRetrieval(promise, loadingProcessor) {
          promise.success(function(response) {

                 if(ServiceUtils.isSuccessful(response)) {

                      if(ServiceUtils.hasPayload(response)) {
                        $scope.accounts =  $scope.accounts.concat(response.payload.firstMonth);
                        $scope.accounts =  $scope.accounts.concat(response.payload.secondMonth);
                      }

                        $scope.stopLoading[loadingProcessor] = true;
                        processLoading();


                 } else {
                     $rootScope.loading = false;
                     printMessage(response.message);
                 }


             }).error(function(response) {
                $rootScope.loading = false;
                 printMessage(response.message);
             });
      }

      function printMessage(message) {
          Materialize.toast(message, 4000)
      }


      $scope.filter = function(value, index, array) {

        if(!$scope.filterItem.debt && UtilsService.isDefined(value.originalAmount)) {
            return false;
        }

        if(!$scope.filterItem.bill && !UtilsService.isDefined(value.originalAmount)) {
            return false;
        }

        if(!$scope.filterItem.first && UtilsService.stringMatch("FIRST", value.timeOfPayment)) {
            return false;
        }

        if(!$scope.filterItem.second  && UtilsService.stringMatch("SECOND", value.timeOfPayment)) {
            return false;
        }

        if(value.monthlyAmount > $scope.filterItem.payment) {
            return false;
        }

         return true;


      }

      $scope.setAccount = function(account) {
        AccountHandlingService.setAccount(account);
        var path = '/account';

        if(UtilsService.isDefined(account)) {
            if(UtilsService.isDefined(account.billAccountId)) {
                path += '/bill/' + account.billAccountId;

            } else if(UtilsService.isDefined(account.debtAccountId)) {
                path +=  '/debt/' + account.debtAccountId;
            }
        }

        $rootScope.goTo(path);
      }

      $scope.createBillingAcc = function() {
           AccountHandlingService.setAccount(createBillingAcc());
      }

      $scope.createDebtAcc = function() {
           AccountHandlingService.setAccount(createDebtAcc());
      }

        function createDebtAcc() {
          var account = createAccount();

          account.interest = 0;
          account.totalRemaining = 0;
          account.originalAmount = 0;
          return account;
        }

        function createBillingAcc() {
           return createAccount();
        }

        function createAccount() {

          return {
              personalAccountId: personalAccountId,
              userName : "",
              password: "",
              name: "",
              description : "",
              module: "WebApplication",
              user : "user",
              monthlyAmount: 0,
              timeOfPayment: "",
              url: ""

          };

        }



      function jsonIfy() {
          $('#json').empty();
          $('#json').append(
              $('<pre>').text(
                  JSON.stringify($scope.response, null, '  ')
              )
          );
      }

});

