package com.turnersoft.budgeting.domain.response.billingCycle.history;

public class BillingHistoryResp {
    private String month;
    private double billExpenses;
    private double debtExpenses;
    private double remainingIncome;


    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public double getBillExpenses() {
        return billExpenses;
    }

    public void setBillExpenses(double billExpenses) {
        this.billExpenses = billExpenses;
    }

    public double getDebtExpenses() {
        return debtExpenses;
    }

    public void setDebtExpenses(double debtExpenses) {
        this.debtExpenses = debtExpenses;
    }

    public double getRemainingIncome() {
        return remainingIncome;
    }

    public void setRemainingIncome(double remainingIncome) {
        this.remainingIncome = remainingIncome;
    }


}
