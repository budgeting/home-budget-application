package com.turnersoft.budgeting.domain.response.accounts.banking.card;

import java.util.ArrayList;
import java.util.List;


public class CardsResp  {
    private List<CardResp> cards = new ArrayList<>();

    public List<CardResp> getCards() {
        return cards;
    }

    public void setCards(List<CardResp> cards) {
        this.cards = cards;
    }

    public void addCard(CardResp cardResp) {
        cards.add(cardResp);
    }
}
