package com.turnersoft.budgeting.domain.response.accounts.billing.reoccuring;


import com.turnersoft.budgeting.domain.response.accounts.billing.PaymentAccountResp;

public class BillingAccountResp extends PaymentAccountResp {

    private Long billAccountId;

    public Long getBillAccountId() {
        return billAccountId;
    }

    public void setBillAccountId(Long billAccountId) {
        this.billAccountId = billAccountId;
    }



}
