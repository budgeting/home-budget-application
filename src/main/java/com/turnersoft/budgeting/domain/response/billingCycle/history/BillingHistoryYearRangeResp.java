package com.turnersoft.budgeting.domain.response.billingCycle.history;


public class BillingHistoryYearRangeResp {
    private int minYear;
    private int maxYear;

    public BillingHistoryYearRangeResp(int minYear, int maxYear) {
        this.minYear = minYear;
        this.maxYear = maxYear;
    }

    public int getMinYear() {
        return minYear;
    }

    public void setMinYear(int minYear) {
        this.minYear = minYear;
    }

    public int getMaxYear() {
        return maxYear;
    }

    public void setMaxYear(int maxYear) {
        this.maxYear = maxYear;
    }
}
