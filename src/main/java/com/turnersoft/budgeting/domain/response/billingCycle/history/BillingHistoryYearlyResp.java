package com.turnersoft.budgeting.domain.response.billingCycle.history;


import java.util.List;

public class BillingHistoryYearlyResp {

    private List<BillingHistoryResp> yearlyHistory;
    private double totalDebtAmountPaid;
    private double totalBillAmountPaid;

    public List<BillingHistoryResp> getYearlyHistory() {
        return yearlyHistory;
    }

    public void setYearlyHistory(List<BillingHistoryResp> yearlyHistory) {
        this.yearlyHistory = yearlyHistory;
    }

    public double getTotalDebtAmountPaid() {
        return totalDebtAmountPaid;
    }

    public void setTotalDebtAmountPaid(double totalDebtAmountPaid) {
        this.totalDebtAmountPaid = totalDebtAmountPaid;
    }

    public double getTotalBillAmountPaid() {
        return totalBillAmountPaid;
    }

    public void setTotalBillAmountPaid(double totalBillAmountPaid) {
        this.totalBillAmountPaid = totalBillAmountPaid;
    }
}
