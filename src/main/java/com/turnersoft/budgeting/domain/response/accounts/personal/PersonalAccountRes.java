package com.turnersoft.budgeting.domain.response.accounts.personal;


import com.turnersoft.budgeting.domain.response.accounts.AccountResp;

public class PersonalAccountRes extends AccountResp {

    private long accountId;
    private long monthId;
    private String email;
    private int savePercentage;
    private double monthlyEarnings;
    private double monthlyDebtExpenses;
    private double monthlyBillExpenses;
    private double savings;
    private double lifeExpenses;

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getSavePercentage() {
        return savePercentage;
    }

    public void setSavePercentage(int savePercentage) {
        this.savePercentage = savePercentage;
    }

    public double getMonthlyEarnings() {
        return monthlyEarnings;
    }

    public void setMonthlyEarnings(double monthlyEarnings) {
        this.monthlyEarnings = monthlyEarnings;
    }

    public double getMonthlyDebtExpenses() {
        return monthlyDebtExpenses;
    }

    public void setMonthlyDebtExpenses(double monthlyDebtExpenses) {
        this.monthlyDebtExpenses = monthlyDebtExpenses;
    }

    public double getMonthlyBillExpenses() {
        return monthlyBillExpenses;
    }

    public void setMonthlyBillExpenses(double monthlyBillExpenses) {
        this.monthlyBillExpenses = monthlyBillExpenses;
    }

    public double getSavings() {
        return savings;
    }

    public void setSavings(double savings) {
        this.savings = savings;
    }

    public double getLifeExpenses() {
        return lifeExpenses;
    }

    public void setLifeExpenses(double lifeExpenses) {
        this.lifeExpenses = lifeExpenses;
    }

    public long getMonthId() {
        return monthId;
    }

    public void setMonthId(long monthId) {
        this.monthId = monthId;
    }
}
