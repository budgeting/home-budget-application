package com.turnersoft.budgeting.domain.response;


import java.io.Serializable;

public class Response<T> implements Serializable {

    private String message;
    private Status status = Status.OK;
    private T payload;

    public Response() {}
    public Response(String message, Status status) {
        this.message = message;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void setErrorMessage(String message) {
        status = Status.ERROR;
        this.message = message;
    }

    public T getPayload() {
        return payload;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }
}
