package com.turnersoft.budgeting.domain.response.accounts.billing;


import com.turnersoft.budgeting.domain.response.accounts.AccountResp;

public abstract class PaymentAccountResp extends AccountResp {

    private double monthlyAmount;
    long personalAccountId;
    private String timeOfPayment;

    public double getMonthlyAmount() {
        return monthlyAmount;
    }

    public void setMonthlyAmount(double monthlyAmount) {
        this.monthlyAmount = monthlyAmount;
    }

    public String getTimeOfPayment() {
        return timeOfPayment;
    }

    public void setTimeOfPayment(String timeOfPayment) {
        this.timeOfPayment = timeOfPayment;
    }

    public long getPersonalAccountId() {
        return personalAccountId;
    }

    public void setPersonalAccountId(long personalAccountId) {
        this.personalAccountId = personalAccountId;
    }
}
