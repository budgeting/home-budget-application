package com.turnersoft.budgeting.domain.response.billingCycle;


import com.turnersoft.budgeting.domain.response.Response;
import com.turnersoft.budgeting.domain.response.billingCycle.debt.DebtAuditRes;
import com.turnersoft.budgeting.domain.response.billingCycle.reoccuring.BillAuditRes;

import java.util.ArrayList;
import java.util.List;

public class BillingCycleRes extends Response {

    private long personalAccId;
    private String month;
    private String timeOfMonth;
    private int year;

    private AuditsRes<BillAuditRes> bills = new AuditsRes<>();
    private AuditsRes<DebtAuditRes> debts = new AuditsRes<>();


    public long getPersonalAccId() {
        return personalAccId;
    }

    public void setPersonalAccId(long personalAccId) {
        this.personalAccId = personalAccId;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getTimeOfMonth() {
        return timeOfMonth;
    }

    public void setTimeOfMonth(String timeOfMonth) {
        this.timeOfMonth = timeOfMonth;
    }

    public AuditsRes<BillAuditRes> getBills() {
        return bills;
    }

    public void setBills(AuditsRes<BillAuditRes> bills) {
        this.bills = bills;
    }

    public AuditsRes<DebtAuditRes> getDebts() {
        return debts;
    }

    public void setDebts(AuditsRes<DebtAuditRes> debts) {
        this.debts = debts;
    }


}
