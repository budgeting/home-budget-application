package com.turnersoft.budgeting.domain.response.billingCycle.debt;


import com.turnersoft.budgeting.domain.response.accounts.AccountResp;

import java.time.LocalDateTime;

public class DebtAuditRes extends AccountResp{

    private long debtAuditId;
    private boolean complete;
    private LocalDateTime dateCompleted;
    private double monthlyAmount;
    private double paidAmount;


    public long getDebtAuditId() {
        return debtAuditId;
    }

    public void setDebtAuditId(long debtAuditId) {
        this.debtAuditId = debtAuditId;
    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    public LocalDateTime getDateCompleted() {
        return dateCompleted;
    }

    public void setDateCompleted(LocalDateTime dateCompleted) {
        this.dateCompleted = dateCompleted;
    }

    public double getMonthlyAmount() {
        return monthlyAmount;
    }

    public void setMonthlyAmount(double monthlyAmount) {
        this.monthlyAmount = monthlyAmount;
    }

    public double getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(double paidAmount) {
        this.paidAmount = paidAmount;
    }
}
