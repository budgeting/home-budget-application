package com.turnersoft.budgeting.domain.response.accounts.billing.debt;


import com.turnersoft.budgeting.domain.response.accounts.billing.PaymentAccountResp;

public class DebtAccountResp extends PaymentAccountResp {

    private long debtAccountId;
    private double interest;
    private double totalRemaining;
    private double originalAmount;


    public long getDebtAccountId() {
        return debtAccountId;
    }

    public void setDebtAccountId(long debtAccountId) {
        this.debtAccountId = debtAccountId;
    }

    public double getInterest() {
        return interest;
    }

    public void setInterest(double interest) {
        this.interest = interest;
    }

    public double getTotalRemaining() {
        return totalRemaining;
    }

    public void setTotalRemaining(double totalRemaining) {
        this.totalRemaining = totalRemaining;
    }

    public double getOriginalAmount() {
        return originalAmount;
    }

    public void setOriginalAmount(double originalAmount) {
        this.originalAmount = originalAmount;
    }
}
