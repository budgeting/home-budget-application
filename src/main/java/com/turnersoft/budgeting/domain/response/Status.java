package com.turnersoft.budgeting.domain.response;


public enum Status {
    OK,
    WARN,
    ERROR;
}
