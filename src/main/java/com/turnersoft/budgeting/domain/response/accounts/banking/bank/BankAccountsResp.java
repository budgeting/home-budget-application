package com.turnersoft.budgeting.domain.response.accounts.banking.bank;


import java.util.ArrayList;
import java.util.List;

public class BankAccountsResp {
    private List<BankAccountResp> bankAccounts = new ArrayList<>();

    public List<BankAccountResp> getBankAccounts() {
        return bankAccounts;
    }

    public void setBankAccounts(List<BankAccountResp> bankAccounts) {
        this.bankAccounts = bankAccounts;
    }

    public void addBankAccount(BankAccountResp bankAccountResp) {
        bankAccounts.add(bankAccountResp);
    }
}
