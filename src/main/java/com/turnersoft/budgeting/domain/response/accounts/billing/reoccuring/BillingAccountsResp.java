package com.turnersoft.budgeting.domain.response.accounts.billing.reoccuring;

import com.turnersoft.budgeting.model.billing.BillingAccount;

import java.util.ArrayList;
import java.util.List;

public class BillingAccountsResp {

    List<BillingAccountResp> firstMonth = new ArrayList<BillingAccountResp>();
    List<BillingAccountResp> secondMonth = new ArrayList<BillingAccountResp>();

    public List<BillingAccountResp> getFirstMonth() {
        return firstMonth;
    }

    public void setFirstMonth(List<BillingAccountResp> firstMonth) {
        this.firstMonth = firstMonth;
    }

    public List<BillingAccountResp> getSecondMonth() {
        return secondMonth;
    }

    public void setSecondMonth(List<BillingAccountResp> secondMonth) {
        this.secondMonth = secondMonth;
    }
}
