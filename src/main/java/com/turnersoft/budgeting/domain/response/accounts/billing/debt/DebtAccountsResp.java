package com.turnersoft.budgeting.domain.response.accounts.billing.debt;

import com.turnersoft.budgeting.model.billing.DebtAccount;

import java.util.ArrayList;
import java.util.List;

public class DebtAccountsResp {

    List<DebtAccountResp> firstMonth = new ArrayList<DebtAccountResp>();
    List<DebtAccountResp> secondMonth = new ArrayList<DebtAccountResp>();

    public List<DebtAccountResp> getFirstMonth() {
        return firstMonth;
    }

    public void setFirstMonth(List<DebtAccountResp> firstMonth) {
        this.firstMonth = firstMonth;
    }

    public List<DebtAccountResp> getSecondMonth() {
        return secondMonth;
    }

    public void setSecondMonth(List<DebtAccountResp> secondMonth) {
        this.secondMonth = secondMonth;
    }
}
