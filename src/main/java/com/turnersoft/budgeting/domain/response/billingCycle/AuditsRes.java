package com.turnersoft.budgeting.domain.response.billingCycle;


import com.turnersoft.budgeting.domain.response.accounts.billing.debt.DebtAccountResp;

import java.util.ArrayList;
import java.util.List;

public class AuditsRes<T> {

    List<T> firstMonth = new ArrayList<T>();
    List<T> secondMonth = new ArrayList<T>();

    public List<T> getFirstMonth() {
        return firstMonth;
    }

    public void setFirstMonth(List<T> firstMonth) {
        this.firstMonth = firstMonth;
    }

    public List<T> getSecondMonth() {
        return secondMonth;
    }

    public void setSecondMonth(List<T> secondMonth) {
        this.secondMonth = secondMonth;
    }
}
