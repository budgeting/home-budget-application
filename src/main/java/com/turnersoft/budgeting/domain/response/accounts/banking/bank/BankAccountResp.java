package com.turnersoft.budgeting.domain.response.accounts.banking.bank;


import com.turnersoft.budgeting.domain.response.accounts.AccountResp;
import com.turnersoft.budgeting.domain.response.accounts.banking.card.CardResp;

public class BankAccountResp extends AccountResp {

    private long bankAccountId;
    private long personalAccountId;
    private String routingNumber;
    private String accountNumber;
    private CardResp debitCard;
    private CardResp creditCard;

    public long getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(long bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    public String getRoutingNumber() {
        return routingNumber;
    }

    public void setRoutingNumber(String routingNumber) {
        this.routingNumber = routingNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public CardResp getDebitCard() {
        return debitCard;
    }

    public void setDebitCard(CardResp debitCard) {
        this.debitCard = debitCard;
    }

    public CardResp getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CardResp creditCard) {
        this.creditCard = creditCard;
    }


    public long getPersonalAccountId() {
        return personalAccountId;
    }

    public void setPersonalAccountId(long personalAccountId) {
        this.personalAccountId = personalAccountId;
    }
}


