package com.turnersoft.budgeting.domain.response.billingCycle.reoccuring;

import com.turnersoft.budgeting.domain.response.accounts.AccountResp;

import java.time.LocalDateTime;

public class BillAuditRes extends AccountResp {

    private long billAuditId;
    private boolean complete;
    private LocalDateTime dateCompleted;
    private double monthlyAmount;


    public long getBillAuditId() {
        return billAuditId;
    }

    public void setBillAuditId(long billAuditId) {
        this.billAuditId = billAuditId;
    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    public LocalDateTime getDateCompleted() {
        return dateCompleted;
    }

    public void setDateCompleted(LocalDateTime dateCompleted) {
        this.dateCompleted = dateCompleted;
    }

    public double getMonthlyAmount() {
        return monthlyAmount;
    }

    public void setMonthlyAmount(double monthlyAmount) {
        this.monthlyAmount = monthlyAmount;
    }

}
