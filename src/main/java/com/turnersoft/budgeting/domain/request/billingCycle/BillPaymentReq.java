package com.turnersoft.budgeting.domain.request.billingCycle;


import com.turnersoft.budgeting.domain.request.Request;

public class BillPaymentReq extends Request {
    private long personalAccId;
    private long billAuditId;

    public long getPersonalAccId() {
        return personalAccId;
    }

    public void setPersonalAccId(long personalAccId) {
        this.personalAccId = personalAccId;
    }

    public long getBillAuditId() {
        return billAuditId;
    }

    public void setBillAuditId(long billAuditId) {
        this.billAuditId = billAuditId;
    }
}
