package com.turnersoft.budgeting.domain.request.accounts.personal;


import com.turnersoft.budgeting.domain.request.accounts.AccountReq;

public class PersonalAccountReq extends AccountReq {

    private long accountId;
    private String email;
    private int savePercentage;
    private double monthlyEarnings;

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getSavePercentage() {
        return savePercentage;
    }

    public void setSavePercentage(int savePercentage) {
        this.savePercentage = savePercentage;
    }

    public double getMonthlyEarnings() {
        return monthlyEarnings;
    }

    public void setMonthlyEarnings(double monthlyEarnings) {
        this.monthlyEarnings = monthlyEarnings;
    }
}
