package com.turnersoft.budgeting.domain.request.accounts.banking;


import com.turnersoft.budgeting.domain.request.Request;

public class CardReq extends Request {
    private long cardId;
    private String fullName;
    private String cardNumber;
    private String cvv;
    private String vendor;
    private String name;
    private String expirationDate;
    private String type;
    private long bankAccId;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public long getCardId() {
        return cardId;
    }

    public void setCardId(long cardId) {
        this.cardId = cardId;
    }

    public long getBankAccId() {
        return bankAccId;
    }

    public void setBankAccId(long bankAccId) {
        this.bankAccId = bankAccId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
