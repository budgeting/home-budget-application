package com.turnersoft.budgeting.domain.request.accounts.banking;


import com.turnersoft.budgeting.domain.request.Request;
import com.turnersoft.budgeting.domain.request.accounts.AccountReq;

public class BankAccountReq extends AccountReq {
    private long bankAccountId;
    private long personalAccountId;
    private String routingNumber;
    private String accountNumber;

    public long getPersonalAccountId() {
        return personalAccountId;
    }

    public void setPersonalAccountId(long personalAccountId) {
        this.personalAccountId = personalAccountId;
    }

    public long getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(long bankAccountId) {
        this.bankAccountId = bankAccountId;
    }


    public String getRoutingNumber() {
        return routingNumber;
    }

    public void setRoutingNumber(String routingNumber) {
        this.routingNumber = routingNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }



}
