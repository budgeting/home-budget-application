package com.turnersoft.budgeting.domain.request.accounts.billing;


public class BillingAccountReq extends PaymentAccountReq {
    private Long billAccountId;

    public Long getBillAccountId() {
        return billAccountId;
    }

    public void setBillAccountId(Long billAccountId) {
        this.billAccountId = billAccountId;
    }


}
