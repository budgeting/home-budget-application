package com.turnersoft.budgeting.domain.request.accounts.billing;

import com.turnersoft.budgeting.domain.request.accounts.AccountReq;

public abstract class PaymentAccountReq extends AccountReq {

    private double monthlyAmount;
    private String timeOfPayment;
    private long personalAccountId;


    public double getMonthlyAmount() {
        return monthlyAmount;
    }

    public void setMonthlyAmount(double monthlyAmount) {
        this.monthlyAmount = monthlyAmount;
    }

    public String getTimeOfPayment() {
        return timeOfPayment;
    }

    public void setTimeOfPayment(String timeOfPayment) {
        this.timeOfPayment = timeOfPayment;
    }

    public long getPersonalAccountId() {
        return personalAccountId;
    }

    public void setPersonalAccountId(long personalAccountId) {
        this.personalAccountId = personalAccountId;
    }
}
