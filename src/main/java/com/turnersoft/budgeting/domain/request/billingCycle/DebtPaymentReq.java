package com.turnersoft.budgeting.domain.request.billingCycle;


public class DebtPaymentReq {

    private long personalAccId;
    private long debtAuditId;
    private double amount;

    public long getPersonalAccId() {
        return personalAccId;
    }

    public void setPersonalAccId(long personalAccId) {
        this.personalAccId = personalAccId;
    }

    public long getDebtAuditId() {
        return debtAuditId;
    }

    public void setDebtAuditId(long debtAuditId) {
        this.debtAuditId = debtAuditId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

}
