package com.turnersoft.budgeting.domain.request.billingCycle;


public class PayBillingCycleReq extends BillingCycleReq{

    private String timeOfMonth;

    public String getTimeOfMonth() {
        return timeOfMonth;
    }

    public void setTimeOfMonth(String timeOfMonth) {
        this.timeOfMonth = timeOfMonth;
    }
}
