package com.turnersoft.budgeting.domain.request.billingCycle;


import com.turnersoft.budgeting.domain.request.Request;

public class BillingCycleReq extends Request {

    private long personalAccId;
    private String month;
    private String timeOfMonth;
    private int year;

    public long getPersonalAccId() {
        return personalAccId;
    }

    public void setPersonalAccId(long personalAccId) {
        this.personalAccId = personalAccId;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getTimeOfMonth() {
        return timeOfMonth;
    }

    public void setTimeOfMonth(String timeOfMonth) {
        this.timeOfMonth = timeOfMonth;
    }
}
