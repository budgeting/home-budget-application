package com.turnersoft.budgeting.domain.request;

import javax.persistence.Column;
import java.time.LocalDateTime;


public abstract class Request {
    private String module;
    private String user;

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

}
