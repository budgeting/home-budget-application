package com.turnersoft.budgeting.service.billing;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.turnersoft.budgeting.dao.billing.BillingDao;
import com.turnersoft.budgeting.model.banking.constants.TimeOfMonth;
import com.turnersoft.budgeting.model.billing.BillingAccount;
import com.turnersoft.budgeting.model.billing.DebtAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BillingServiceImpl implements  BillingService {

    private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();


    @Autowired
    private BillingDao billingDao;

    @Override
    public void saveDebtAccount(DebtAccount account) {
        billingDao.saveDebtAccount(account);
    }

    @Override
    public DebtAccount findDebtAccount(long debtAccountId) {
        return billingDao.findDebtAcc(debtAccountId);
    }

    @Override
    public void deleteDebtAcc(long debtAccId) {
         billingDao.deleteDebtAcc(debtAccId);
    }

    @Override
    public void deactivateDebtAcc(long debtAccId) {
        billingDao.deactivateDebtAcc(debtAccId);
    }

    @Override
    public Map<String, List<DebtAccount>> findDebtsByTimeOfMonth(long personalAccId) {
        Map<String, List<DebtAccount>> debtsByTime = new HashMap<>();

        for(TimeOfMonth timeOfMonth : TimeOfMonth.values()) {
            debtsByTime.put(timeOfMonth.getValue(), findDebtsByTimeOfMonth(timeOfMonth.getValue(), personalAccId));
        }

        return debtsByTime;
    }

    @Override
    public List<DebtAccount> findDebtsByTimeOfMonth(String timeOfMonth, long personalAccId) {
        return billingDao.findDebtByTimeOfMonth(timeOfMonth, personalAccId);
    }

    @Override
    public List<DebtAccount> findDebtAccounts(long personalAccId) {
        return billingDao.findDebtAccounts(personalAccId);
    }

    @Override
    public void saveBillingAcc(BillingAccount billingAccount) {
        billingDao.saveBillingAcc(billingAccount);
    }

    @Override
    public BillingAccount findBillingAcc(long billingAccId) {
        return billingDao.findBillingAcc(billingAccId);
    }

    @Override
    public void deleteBillingAcc(long billingAccId) {
        billingDao.deleteBillingAcc(billingAccId);
    }

    @Override
    public void deactivateBillingAcc(long billingAccId) {
        billingDao.deactivateBillingAcc(billingAccId);
    }

    @Override
    public Map<String, List<BillingAccount>> findBillsByTimeOfMonth(long personalAccId) {
        Map<String, List<BillingAccount>> billsByTime = new HashMap<>();

        for(TimeOfMonth timeOfMonth : TimeOfMonth.values()) {
            billsByTime.put(timeOfMonth.getValue(), findBillsByTimeOfMonth(timeOfMonth.getValue(), personalAccId));
        }

        return billsByTime;
    }

    @Override
    public List<BillingAccount> findBillsByTimeOfMonth(String timeOfMonth, long personalAccId) {

        return billingDao.findBillingByTimeOfMonth(timeOfMonth, personalAccId);
    }

    @Override
    public List<BillingAccount> findBillingAccounts(long personalAccId) {
        return billingDao.findBillingAccs(personalAccId);
    }

}
