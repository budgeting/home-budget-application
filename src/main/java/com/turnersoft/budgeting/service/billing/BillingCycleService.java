package com.turnersoft.budgeting.service.billing;


import com.turnersoft.budgeting.domain.request.billingCycle.BillingCycleReq;
import com.turnersoft.budgeting.domain.request.billingCycle.PayBillingCycleReq;
import com.turnersoft.budgeting.domain.response.billingCycle.history.BillingHistoryResp;
import com.turnersoft.budgeting.domain.response.billingCycle.reoccuring.BillAuditRes;
import com.turnersoft.budgeting.model.auditing.BillAudit;
import com.turnersoft.budgeting.model.auditing.DebtAudit;
import com.turnersoft.budgeting.model.auditing.Month;

import java.util.List;

public interface BillingCycleService {

    public void createBillingCycle(BillingCycleReq request);
    public void payHalfOfBillingCycle(PayBillingCycleReq request);
    public Month retrieveMonthlyBillingCycle(long personalAccId, String month, int year);
    public Month retrieveMonthlyBillingCycle(long monthId);
    public List<Month> retrieveBillingCycleHistory(long personalAccId, int year);
    public DebtAudit payMonthlyDebtAmount(long debtAuditId, double amount);
    public BillAudit payMonthlyBill(long billAuditId);
    public BillAudit findBillAudit(long billAuditId);
    public DebtAudit findDebtAudit(long debtAuditId);
    public Integer findBillingCycleMinYear(long personalAccId);
    public Integer findBillingCycleMaxYear(long personalAccId);


}
