package com.turnersoft.budgeting.service.billing;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.turnersoft.budgeting.dao.PersonalAccountDao;
import com.turnersoft.budgeting.dao.auditing.interfaces.MonthDao;
import com.turnersoft.budgeting.dao.billing.BillingDao;
import com.turnersoft.budgeting.domain.request.billingCycle.BillingCycleReq;
import com.turnersoft.budgeting.domain.request.billingCycle.PayBillingCycleReq;
import com.turnersoft.budgeting.domain.response.billingCycle.history.BillingHistoryResp;
import com.turnersoft.budgeting.model.PersonalAccount;
import com.turnersoft.budgeting.model.auditing.BillAudit;
import com.turnersoft.budgeting.model.auditing.DebtAudit;
import com.turnersoft.budgeting.model.auditing.Month;
import com.turnersoft.budgeting.model.billing.DebtAccount;
import com.turnersoft.budgeting.utils.LogUtils;
import com.turnersoft.budgeting.utils.Utils;
import com.turnersoft.budgeting.utils.data.BillingCycleDataUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Service
public class BillingCycleServiceImpl implements BillingCycleService {
    private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();

    @Autowired
    MonthDao monthDao;

    @Autowired
    BillingDao billingDao;

    @Autowired
    PersonalAccountDao personalAccountDao;


    @Override
    public void createBillingCycle(BillingCycleReq request) {
        String method = "createBillingCycle(...): ";


        Month month = new Month(request.getMonth(), request.getYear());
        month.setBills(BillingCycleDataUtils.billsToAudits(billingDao.findBillingAccs(request.getPersonalAccId()), month));
        month.setDebts(BillingCycleDataUtils.debtsToAudits(billingDao.findDebtAccounts(request.getPersonalAccId()), month));

        PersonalAccount account = personalAccountDao.findAccount(request.getPersonalAccId());

        //Monthly earnings for this paticular monthly snapshot
        month.setEarned(account.getMontlyEarnings());
        month.setPersonalAccount(account);

        monthDao.saveMonth(month);
        account.setActiveMonth(monthDao.findByNameAndYear(request.getMonth(), request.getYear(), account.getAccountId()));
        personalAccountDao.saveAccount(account);

    }

    @Override
    public BillAudit payMonthlyBill(long billingAuditId) {
        payMonthlyBill(monthDao.findBillAudit(billingAuditId));

        return monthDao.findBillAudit(billingAuditId);
    }

    @Override
    public BillAudit findBillAudit(long billAuditId) {
        return monthDao.findBillAudit(billAuditId);
    }

    @Override
    public DebtAudit findDebtAudit(long debtAuditId) {
        return monthDao.findDebtAudit(debtAuditId);
    }

    @Override
    public Integer findBillingCycleMinYear(long personalAccId) {
        return monthDao.findMinYearByAccount(personalAccId);
    }

    @Override
    public Integer findBillingCycleMaxYear(long personalAccId) {
        return monthDao.findMaxYearByAccount(personalAccId);
    }


    @Override
    public void payHalfOfBillingCycle(PayBillingCycleReq req) {
        String method = "payHalfOfBillingCycle(...): ";
        Month month = monthDao.findByNameAndYear(req.getMonth(), req.getYear(), req.getPersonalAccId());

        payHalfOfDebtCycle(month.getDebts(), req.getTimeOfMonth());
        payHalfOfBillingCycle(month.getBills(), req.getTimeOfMonth());
    }

    @Override
    public Month retrieveMonthlyBillingCycle(long personalAccId, String month, int year) {
        String method = "retrieveMonthlyBillingCycle(...): ";

        LogUtils.printRequest(method + "request received! month: [" + month + "] year: [" +year + "]");
        return monthDao.findByNameAndYear(month, year, personalAccId);
    }

    @Override
    public Month retrieveMonthlyBillingCycle(long monthId) {
        return monthDao.find(monthId);
    }

    @Override
    public List<Month> retrieveBillingCycleHistory(long personalAccId, int year) {
        return monthDao.findMonthsByYear(year, personalAccId);
    }


    @Override
    public DebtAudit payMonthlyDebtAmount(long debtAuditId, double amount) {
        DebtAudit debtAudit  = monthDao.findDebtAudit(debtAuditId);

        if(debtAudit != null) {
            DebtAccount debtAccount = debtAudit.getDebtAccount();
            double totalAmount = amount + debtAudit.getPaidAmount();

            if(totalAmount >= debtAccount.getMonthlyAmount()) {
                debtAudit.setDateCompleted(LocalDateTime.now());
                debtAudit.setComplete(true);
            }

            debtAudit.setPaidAmount(totalAmount);
            monthDao.saveDebtAudit(debtAudit);
            adjustDebtAccountAfterPaymentAndSave(debtAccount, amount);

        }

        return monthDao.findDebtAudit(debtAuditId);
    }


    public void payHalfOfBillingCycle(Set<BillAudit> billAudits, String timeOfMonth) {

        if(Utils.hasItems(billAudits)) {

            for(BillAudit billAudit : billAudits) {

                if(Utils.isEqual(timeOfMonth, billAudit.getBillingAccount().getTimeOfPayment())) {
                    payMonthlyBill(billAudit);
                }
            }
        }
    }


    public void payHalfOfDebtCycle(Set<DebtAudit> debtAudits, String timeOfMonth) {

        if(Utils.hasItems(debtAudits)) {

            for(DebtAudit debtAudit : debtAudits) {

                if(Utils.isEqual(timeOfMonth, debtAudit.getDebtAccount().getTimeOfPayment())) {
                    payMonthlyDebt(debtAudit);
                }
            }
        }
    }



    public void payMonthlyBill(BillAudit billAudit) {

        if(billAudit != null) {
            billAudit.completePayment();
            billAudit.setAmountPaid(billAudit.getBillingAccount() != null ? billAudit.getBillingAccount().getMonthlyAmount() : 0);
            monthDao.saveBillAudit(billAudit);
        }
    }

    public void payMonthlyDebt(long debtAuditId) {
        payMonthlyDebt(monthDao.findDebtAudit(debtAuditId));
    }

    public void payMonthlyDebt(DebtAudit debtAudit)  {
        if(debtAudit != null) {
            debtAudit.completePayment();

            DebtAccount debtAccount = debtAudit.getDebtAccount();
            monthDao.saveDebtAudit(debtAudit);
            adjustDebtAccountAfterPaymentAndSave(debtAccount, debtAccount.getMonthlyAmount());

        }
    }

    private void adjustDebtAccountAfterPaymentAndSave(DebtAccount debtAccount, double totalPaid) {
        double totalRemaining = debtAccount.getTotalRemaining() - totalPaid;

        //Pay Off Account if there is nothing remaining
        if(totalRemaining <= 0) {
            debtAccount.payOffAccount();

        } else {

             //If the total remaining is less than the typical monthly amount, then lower the monthly amount for next
             //pay cycle, as there is no reason to pay more than what is owed.
             if (totalRemaining < debtAccount.getMonthlyAmount()) {
                debtAccount.setMonthlyAmount(totalRemaining);
            }

            debtAccount.setTotalRemaining(totalRemaining);
        }

        billingDao.saveDebtAccount(debtAccount);

    }

}
