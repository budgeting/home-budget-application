package com.turnersoft.budgeting.service.billing;

import com.turnersoft.budgeting.model.banking.constants.TimeOfMonth;
import com.turnersoft.budgeting.model.billing.BillingAccount;
import com.turnersoft.budgeting.model.billing.DebtAccount;

import java.util.List;
import java.util.Map;

public interface BillingService {
    public void saveDebtAccount(DebtAccount debtAccount);
    public DebtAccount findDebtAccount(long debtAccountId);
    public void deleteDebtAcc(long debtAccId);
    public void deactivateDebtAcc(long debtAccId);
    public Map<String, List<DebtAccount>> findDebtsByTimeOfMonth(long personalAccId);
    public List<DebtAccount> findDebtsByTimeOfMonth(String timeOfMonth, long personalAccId);
    public List<DebtAccount> findDebtAccounts(long personalAccId);

    public void saveBillingAcc(BillingAccount billingAccount);
    public BillingAccount findBillingAcc(long billingAccId);
    public void deleteBillingAcc(long billingAccId);
    public void deactivateBillingAcc(long billingAccId);
    public Map<String, List<BillingAccount>> findBillsByTimeOfMonth(long personalAccId);
    public List<BillingAccount> findBillsByTimeOfMonth(String timeOfMonth, long personalAccId);
    public List<BillingAccount> findBillingAccounts(long personalAccId);



}
