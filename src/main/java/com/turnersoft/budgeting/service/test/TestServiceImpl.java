package com.turnersoft.budgeting.service.test;


import com.turnersoft.budgeting.dao.test.TestDao;
import org.springframework.beans.factory.annotation.Autowired;
import com.turnersoft.budgeting.model.Test;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TestServiceImpl implements TestService {

    @Autowired
    private TestDao testDao;

    @Override
    public List<Test> testDbConnection() {
        return testDao.testDbConnection();
    }
}
