package com.turnersoft.budgeting.service.test;


import com.turnersoft.budgeting.model.Test;

import java.util.List;

public interface TestService {
    public List<Test> testDbConnection();
}
