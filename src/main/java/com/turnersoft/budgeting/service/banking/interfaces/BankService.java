package com.turnersoft.budgeting.service.banking.interfaces;


import com.turnersoft.budgeting.model.banking.BankAccount;

import java.util.List;

public interface BankService {
    public void saveBankAccount(BankAccount bankAccount);
    public List<BankAccount> findAllBankAccounts(long personalAccId);
    public BankAccount findBankAccount(long id);
    public BankAccount findByName(String name, long personalAccId);
    public void deleteBankAccount(BankAccount bankAccount);
    public void deleteBankAccount(long id);
}
