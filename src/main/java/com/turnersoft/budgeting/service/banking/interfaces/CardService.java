package com.turnersoft.budgeting.service.banking.interfaces;


import com.turnersoft.budgeting.model.banking.Card;

import java.util.List;

public interface CardService {

    void saveCard(Card card);
    List<Card> findAllCardsFromBank(long bankId);
    Card find(long id);
    Card findByName(String name);
    void deleteCard(long id);
    void deleteCard(Card card);
}
