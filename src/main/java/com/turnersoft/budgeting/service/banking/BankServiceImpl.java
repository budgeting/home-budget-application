package com.turnersoft.budgeting.service.banking;


import com.turnersoft.budgeting.dao.banking.interfaces.BankDao;
import com.turnersoft.budgeting.model.banking.BankAccount;
import com.turnersoft.budgeting.service.banking.interfaces.BankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BankServiceImpl implements BankService {

    @Autowired
    private BankDao bankDao;

    @Override
    public void saveBankAccount(BankAccount bankAccount) {
        bankDao.saveBankAccount(bankAccount);
    }

    @Override
    public List<BankAccount> findAllBankAccounts(long personalAccId) {
        return bankDao.findAll(personalAccId);
    }

    @Override
    public BankAccount findBankAccount(long id) {
        return bankDao.findById(id);
    }

    @Override
    public BankAccount findByName(String name, long personalAccId) {
        return bankDao.findByName(name, personalAccId);
    }

    @Override
    public void deleteBankAccount(BankAccount bankAccount) {
        bankDao.deleteAccount(bankAccount);
    }

    @Override
    public void deleteBankAccount(long id) {
        deleteBankAccount(findBankAccount(id));
    }
}
