package com.turnersoft.budgeting.service.banking;

import com.turnersoft.budgeting.dao.banking.interfaces.BankDao;
import com.turnersoft.budgeting.dao.banking.interfaces.CardDao;
import com.turnersoft.budgeting.model.banking.BankAccount;
import com.turnersoft.budgeting.model.banking.Card;
import com.turnersoft.budgeting.service.banking.interfaces.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CardServiceImpl implements CardService {

    @Autowired
    CardDao cardDao;

    @Autowired
    BankDao bankDao;

    @Override
    public void saveCard(Card card) {
        cardDao.saveCard(card);
    }

    @Override
    public List<Card> findAllCardsFromBank(long bankId) {
        return cardDao.findAllCardsFromBank(bankDao.findById(bankId));
    }

    @Override
    public Card find(long id) {
        return cardDao.find(id);
    }

    @Override
    public Card findByName(String name) {
        return cardDao.findByName(name);
    }

    @Override
    public void deleteCard(long id) {
        cardDao.deleteCard(id);
    }

    @Override
    public void deleteCard(Card card) {
        cardDao.deleteCard(card);
    }
}
