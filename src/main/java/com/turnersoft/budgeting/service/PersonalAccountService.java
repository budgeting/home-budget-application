package com.turnersoft.budgeting.service;

import com.turnersoft.budgeting.model.PersonalAccount;

public interface PersonalAccountService {
    public void saveAccount(PersonalAccount account);
    public void deleteAccount(long id);
    public PersonalAccount findAccount(long id);
    public PersonalAccount findByEmail(String email);
    public boolean doesAccountExist(String email);
    public boolean doesAccountExist(long id);
}
