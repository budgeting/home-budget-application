package com.turnersoft.budgeting.service;

import com.turnersoft.budgeting.dao.PersonalAccountDao;
import com.turnersoft.budgeting.model.PersonalAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonalAccountServiceImpl implements PersonalAccountService{

    @Autowired
    PersonalAccountDao personalAccountDao;

    @Override
    public void saveAccount(PersonalAccount account) {
        personalAccountDao.saveAccount(account);
    }

    @Override
    public void deleteAccount(long id) {
        personalAccountDao.deleteAccount(id);
    }

    @Override
    public PersonalAccount findAccount(long id) {
        return personalAccountDao.findAccount(id);
    }

    @Override
    public PersonalAccount findByEmail(String email) {
        return personalAccountDao.findByEmail(email);
    }

    @Override
    public boolean doesAccountExist(String email) {
        return personalAccountDao.doesAccountExist(email);
    }

    @Override
    public boolean doesAccountExist(long id) {
        return personalAccountDao.doesAccountExist(id);
    }
}
