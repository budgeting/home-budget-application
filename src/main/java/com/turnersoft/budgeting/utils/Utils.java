package com.turnersoft.budgeting.utils;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Set;

public class Utils {

    public static boolean hasItems(List list) {
        return list != null && !list.isEmpty();
    }

    public static boolean hasItems(Set set) {
        return set != null && !set.isEmpty();
    }

    public static boolean isEmpty(String string) {
        return string == null || string.isEmpty();
    }

    public static boolean isEqual(String string1, String string2) {
        boolean isEqual = false;

        if(string1 != null && string2 != null) {
            isEqual = string1.equals(string2);
        }

        return isEqual;
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
