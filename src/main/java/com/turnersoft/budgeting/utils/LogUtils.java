package com.turnersoft.budgeting.utils;


public class LogUtils {

    public static void printRequest(String method, String content) {
        printMessage(method, "Request Recieved! Request CONTENT:", content);
    }

    public static void printResponse(String method, String content, String status) {
        printMessage(method, "Response Returned [" + status + "]: Response CONTENT:", content);
    }

    public static void printRequest(String method) {
        printMessage(method, " Request Recieved!", null);
    }

    public static void printResponse(String method, String status) {
        printMessage(method, " Response Returned [" + status + "]", null);
    }

    private static void printMessage(String method, String message, String content) {
        printLine2();
        System.out.println(method + message);

        if(content != null) {
            printLine1();
            System.out.println(content);
        }

        printLine1();


    }

    private static void printLine1() {
        System.out.println("--------------------");
    }

    private static void printLine2() {
        System.out.println("====================");
    }
}
