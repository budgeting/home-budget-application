package com.turnersoft.budgeting.utils.data;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.turnersoft.budgeting.domain.request.accounts.billing.BillingAccountReq;
import com.turnersoft.budgeting.domain.request.accounts.billing.DebtAccountReq;
import com.turnersoft.budgeting.domain.request.accounts.billing.PaymentAccountReq;
import com.turnersoft.budgeting.domain.response.accounts.billing.PaymentAccountResp;
import com.turnersoft.budgeting.domain.response.accounts.billing.debt.DebtAccountResp;
import com.turnersoft.budgeting.domain.response.accounts.billing.debt.DebtAccountsResp;
import com.turnersoft.budgeting.domain.response.accounts.billing.reoccuring.BillingAccountResp;
import com.turnersoft.budgeting.domain.response.accounts.billing.reoccuring.BillingAccountsResp;
import com.turnersoft.budgeting.model.banking.constants.TimeOfMonth;
import com.turnersoft.budgeting.model.billing.BillingAccount;
import com.turnersoft.budgeting.model.billing.DebtAccount;
import com.turnersoft.budgeting.model.billing.PaymentAccount;
import com.turnersoft.budgeting.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class BillingDataUtils extends AccountUtils {

    private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();


    /* ************Request To Account ************* */
    public static DebtAccount requestToDebtAccount(DebtAccountReq request, DebtAccount debtAccount) {

            updatePaymentAccountFromRequest(debtAccount, request);
            debtAccount.setTotalRemaining(request.getTotalRemaining());
            debtAccount.setInterest(request.getInterest());
            debtAccount.setMonthlyAmount(request.getMonthlyAmount());
            debtAccount.setOriginalAmount(request.getOriginalAmount());
            populateAuditDetails(debtAccount, request, debtAccount.getDebtAccountId() == 0L);

        return debtAccount;
    }

    public static BillingAccount requestToBillingAccount(BillingAccountReq request, BillingAccount billingAccount) {

        updatePaymentAccountFromRequest(billingAccount, request);
        populateAuditDetails(billingAccount, request, billingAccount.getBillAccountId() == 0L);

        return billingAccount;
    }


    private static void updatePaymentAccountFromRequest(PaymentAccount account, PaymentAccountReq request) {
        requestToAccount(request, account);
        account.setTimeOfPayment(request.getTimeOfPayment());
        account.setMonthlyAmount(request.getMonthlyAmount());
    }

    /* ************Account To Response ************* */


    public static DebtAccountsResp debtAccountsToResponse(Map<String, List<DebtAccount>> debtAccounts) {
        DebtAccountsResp response = new DebtAccountsResp();

        response.setFirstMonth(debtAccountsToResponse(debtAccounts.get(TimeOfMonth.FIRST.getValue())));
        response.setSecondMonth(debtAccountsToResponse(debtAccounts.get(TimeOfMonth.SECOND.getValue())));

        return response;
    }

    public static BillingAccountsResp billingAccountsToResponse(Map<String, List<BillingAccount>> billingAccounts) {
        BillingAccountsResp response = new BillingAccountsResp();

        response.setFirstMonth(billingAccountsToResponse(billingAccounts.get(TimeOfMonth.FIRST.getValue())));
        response.setSecondMonth(billingAccountsToResponse(billingAccounts.get(TimeOfMonth.SECOND.getValue())));
        return response;
    }



    public static DebtAccountResp billingAccountsToResponse(DebtAccount account) {
        DebtAccountResp response = new DebtAccountResp();
        accountToResponse(account, response);
        updateResponseFromPaymentAccount(response, account);

        response.setDebtAccountId(account.getDebtAccountId());
        response.setInterest(account.getInterest());
        response.setTotalRemaining(account.getTotalRemaining());
        response.setOriginalAmount(account.getOriginalAmount());

        return response;

    }

    public static BillingAccountResp billingAccountToResponse(BillingAccount account) {
        BillingAccountResp response = new BillingAccountResp();
        accountToResponse(account, response);
        updateResponseFromPaymentAccount(response, account);

        response.setBillAccountId(account.getBillAccountId());

        return response;
    }

    private static List<DebtAccountResp> debtAccountsToResponse(List<DebtAccount> debtAccounts) {
        List<DebtAccountResp> response = new ArrayList<>();

        if(Utils.hasItems(debtAccounts)) {

            for(DebtAccount account : debtAccounts) {
                response.add(billingAccountsToResponse(account));
            }
        }
;
        return response;
    }

    public static List<BillingAccountResp> billingAccountsToResponse(List<BillingAccount> accounts) {
        List<BillingAccountResp> response = new ArrayList<>();

        if(Utils.hasItems(accounts)) {

            for(BillingAccount account : accounts) {
                response.add(billingAccountToResponse(account));
            }
        }


        return response;
    }


    private static void updateResponseFromPaymentAccount(PaymentAccountResp response, PaymentAccount account) {
        response.setTimeOfPayment(account.getTimeOfPayment());
        response.setMonthlyAmount(account.getMonthlyAmount());
        response.setPersonalAccountId(account.getPersonalAccount().getAccountId());
    }


}

