package com.turnersoft.budgeting.utils.data;


import com.turnersoft.budgeting.domain.request.Request;
import com.turnersoft.budgeting.model.AuditFields;

import java.time.LocalDateTime;

public class BaseDataUtils {

    public static void populateAuditDetails(AuditFields auditFields, Request request, boolean isNew) {
        if(isNew) {
            auditFields.setCreateTimestamp(LocalDateTime.now());
            auditFields.setCreateModule(request.getModule());
            auditFields.setCreatedBy(request.getUser());

        } else {
            auditFields.setUpdateTimestmap(LocalDateTime.now());
            auditFields.setUpdateModule(request.getModule());
            auditFields.setUpdatedBy(request.getUser());
        }
    }
}
