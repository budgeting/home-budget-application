package com.turnersoft.budgeting.utils.data;


import com.turnersoft.budgeting.domain.request.accounts.banking.BankAccountReq;
import com.turnersoft.budgeting.domain.request.accounts.banking.CardReq;
import com.turnersoft.budgeting.domain.response.accounts.banking.bank.BankAccountResp;
import com.turnersoft.budgeting.domain.response.accounts.banking.bank.BankAccountsResp;
import com.turnersoft.budgeting.domain.response.accounts.banking.card.CardResp;
import com.turnersoft.budgeting.domain.response.accounts.banking.card.CardsResp;
import com.turnersoft.budgeting.model.banking.BankAccount;
import com.turnersoft.budgeting.model.banking.Card;
import com.turnersoft.budgeting.utils.Utils;

import java.util.List;

public class BankingDataUtils extends AccountUtils {


    /* *****Account To Response***** */

    public static BankAccountResp bankAccountToResponse(BankAccount bankAccount) {
        BankAccountResp response = new BankAccountResp();

        if(bankAccount != null) {
            accountToResponse(bankAccount, response);
            response.setAccountNumber(bankAccount.getAccountNumber());
            response.setBankAccountId(bankAccount.getBankAccountId());
            response.setRoutingNumber(bankAccount.getRoutingNumber());
            response.setPersonalAccountId(bankAccount.getPersonalAccount().getAccountId());

            setAccountCardsToResponse(bankAccount, response);
        }

        return response;
    }

    private static void setAccountCardsToResponse(BankAccount account, BankAccountResp response) {
        if(Utils.hasItems(account.getAccountCards())) {

            for(Card card : account.getAccountCards()) {

                switch (card.getCardType()) {

                    case DEBIT:
                        response.setDebitCard(cardToResponse(card));
                        break;

                    case CREDIT:
                        response.setCreditCard(cardToResponse(card));
                        break;
                }

            }
        }
    }

    public static BankAccountsResp bankAccountsToResponse(List<BankAccount> bankAccountList) {
        BankAccountsResp response = new BankAccountsResp();

        if(Utils.hasItems(bankAccountList)) {
            for(BankAccount bankAccount : bankAccountList) {
                response.addBankAccount(bankAccountToResponse(bankAccount));
            }
        }

        return response;
    }

    public static CardResp cardToResponse(Card card) {
        CardResp cardResponse = new CardResp();

        if (card != null) {

            cardResponse.setFullName(card.getFullName());
            cardResponse.setBankAccId(card.getBankAccount().getBankAccountId());
            cardResponse.setCardId(card.getCardId());
            cardResponse.setExpirationDate(card.getExpirationDate());
            cardResponse.setVendor(card.getVendor());
            cardResponse.setCardNumber(card.getCardNumber());
            cardResponse.setType(card.getType());
            cardResponse.setCvv(card.getCvv());
            cardResponse.setName(card.getName());
        }


        return cardResponse;
    }

    public static CardsResp cardsToResponse(List<Card> cards) {
        CardsResp response = new CardsResp();

        if(Utils.hasItems(cards)) {
            for(Card card : cards) {
                response.addCard(cardToResponse(card));
            }
        }

        return response;
    }


    /* ******* Request To Account ********** */

    public static BankAccount requestToBankAccount(BankAccountReq request, BankAccount bankAccount) {

        bankAccount.setAccountNumber(request.getAccountNumber());
        bankAccount.setRoutingNumber(request.getRoutingNumber());
        requestToAccount(request, bankAccount);

        System.out.println(bankAccount.getBankAccountId());
        populateAuditDetails(bankAccount, request, bankAccount.getBankAccountId() == 0L);

        return bankAccount;
    }

    public static Card requestToCard(CardReq cardReq, Card card) {

        card.setFullName(cardReq.getFullName());
        card.setExpirationDate(cardReq.getExpirationDate());
        card.setVendor(cardReq.getVendor());
        card.setCardNumber(cardReq.getCardNumber());
        card.setType(cardReq.getType());
        card.setName(cardReq.getName());
        card.setCvv(cardReq.getCvv());
        populateAuditDetails(card, cardReq, cardReq.getCardId() != 0L);


        return card;
    }



}
