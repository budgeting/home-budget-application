package com.turnersoft.budgeting.utils.data;


import com.turnersoft.budgeting.domain.response.billingCycle.AuditsRes;
import com.turnersoft.budgeting.domain.response.billingCycle.history.BillingHistoryResp;
import com.turnersoft.budgeting.domain.response.billingCycle.history.BillingHistoryYearlyResp;
import com.turnersoft.budgeting.domain.response.billingCycle.reoccuring.BillAuditRes;
import com.turnersoft.budgeting.domain.response.billingCycle.BillingCycleRes;
import com.turnersoft.budgeting.domain.response.billingCycle.debt.DebtAuditRes;
import com.turnersoft.budgeting.model.auditing.BillAudit;
import com.turnersoft.budgeting.model.auditing.DebtAudit;
import com.turnersoft.budgeting.model.auditing.Month;
import com.turnersoft.budgeting.model.banking.constants.TimeOfMonth;
import com.turnersoft.budgeting.model.billing.BillingAccount;
import com.turnersoft.budgeting.model.billing.DebtAccount;
import com.turnersoft.budgeting.utils.Utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class BillingCycleDataUtils extends  AccountUtils {

    public static Set<BillAudit> billsToAudits(List<BillingAccount> billingAccounts, Month month) {
        Set<BillAudit> billAudits = new HashSet<>();

        if(Utils.hasItems(billingAccounts)) {

            for(BillingAccount bill : billingAccounts) {
                billAudits.add(new BillAudit(month, bill));
            }
        }

        return billAudits;
    }

    public static Set<DebtAudit> debtsToAudits(List<DebtAccount> debtAccounts, Month month) {
        Set<DebtAudit> debtAudits = new HashSet<>();

        if(Utils.hasItems(debtAccounts)) {

            for(DebtAccount debtAccount : debtAccounts) {
                debtAudits.add(new DebtAudit(month, debtAccount));
            }
        }

        return debtAudits;
    }

    public static BillingHistoryYearlyResp billingHistoryToResponse(List<Month> billingHistory) {
        BillingHistoryYearlyResp response = new BillingHistoryYearlyResp();
        List<BillingHistoryResp> yearlyHistory = new ArrayList<>();

        response.setYearlyHistory(yearlyHistory);

        if(Utils.hasItems(billingHistory)) {

            //Go through all 12 months - starting at january
            double totalDebtAmountPaid = 0;
            double totalBillAmountPaid = 0;
            for(com.turnersoft.budgeting.model.billing.constants.Month month : com.turnersoft.budgeting.model.billing.constants.Month.values()) {
                List<Month> monthlyHistory = billingHistory.stream().filter(m -> Utils.isEqual(m.getMonth(), month.getValue())).collect(Collectors.toList());

                if(Utils.hasItems(monthlyHistory)) {
                    BillingHistoryResp monthlyHistoryResponse = monthToBillingHistoryResp(monthlyHistory.get(0));
                     yearlyHistory.add(monthlyHistoryResponse);

                    totalBillAmountPaid += monthlyHistoryResponse.getBillExpenses();
                    totalDebtAmountPaid += monthlyHistoryResponse.getDebtExpenses();

                } else if(Utils.hasItems(yearlyHistory)) {
                    break;
                }
            }

            response.setTotalBillAmountPaid(totalBillAmountPaid);
            response.setTotalDebtAmountPaid(totalDebtAmountPaid);

        }

        return response;
    }

    private static BillingHistoryResp monthToBillingHistoryResp(Month month) {
        BillingHistoryResp response = new BillingHistoryResp();

        if(month != null) {

            response.setMonth(month.getMonth());

            if(Utils.hasItems(month.getBills())) {
                double billExpenses = month.getBills().stream().mapToDouble(BillAudit::getAmountPaid).sum();
                response.setBillExpenses(billExpenses);
            }

            if(Utils.hasItems(month.getDebts())) {
                double debtExpenses = month.getDebts().stream().mapToDouble(DebtAudit::getPaidAmount).sum();
                response.setDebtExpenses(debtExpenses);
            }


            response.setRemainingIncome(month.getEarned() - response.getBillExpenses() - response.getDebtExpenses());
        }

        return response;
    }

    public static BillingCycleRes monthToBillingCycleRes(Month month) {
        BillingCycleRes response = new BillingCycleRes();

        if(month != null) {
            response.setYear(month.getYear());
            response.setMonth(month.getMonth());
            response.setPersonalAccId(month.getPersonalAccount().getAccountId());

            response.setBills(monthBillAuditsToResponse(month));
            response.setDebts(monthDebtAuditsToResponse(month));

        }
        return response;
    }

    private static AuditsRes<BillAuditRes> monthBillAuditsToResponse(Month month) {
        AuditsRes<BillAuditRes> bills = new AuditsRes<>();

        if(Utils.hasItems(month.getBills())) {
            bills.setFirstMonth(billAuditsToResponseByTime(month.getBills(), TimeOfMonth.FIRST));
            bills.setSecondMonth(billAuditsToResponseByTime(month.getBills(), TimeOfMonth.SECOND));

        }

        return bills;
    }

    private static AuditsRes<DebtAuditRes> monthDebtAuditsToResponse(Month month) {
        AuditsRes<DebtAuditRes> debts = new AuditsRes<>();

        if(Utils.hasItems(month.getBills())) {

            debts.setFirstMonth(debtsAuditsToResponseByTime(month.getDebts(), TimeOfMonth.FIRST));
            debts.setSecondMonth(debtsAuditsToResponseByTime(month.getDebts(), TimeOfMonth.SECOND));

        }

        return debts;
    }

    private static List<BillAuditRes> billAuditsToResponseByTime(Set<BillAudit> billAudits, TimeOfMonth timeOfMonth) {
        List<BillAuditRes> bills = new ArrayList<>();

        billAudits.stream().filter(b ->
                Utils.isEqual(b.getBillingAccount().getTimeOfPayment(), timeOfMonth.getValue()))
                .forEach(b -> bills.add(billAuditToResponse(b)));

        return bills;
    }

    private static List<DebtAuditRes> debtsAuditsToResponseByTime(Set<DebtAudit> debtAudits, TimeOfMonth timeOfMonth) {
        List<DebtAuditRes> debts = new ArrayList<>();

        debtAudits.stream().filter(d ->
                Utils.isEqual(d.getDebtAccount().getTimeOfPayment(), timeOfMonth.getValue()))
                .forEach(d -> debts.add(debtAuditToResponse(d)));

        return debts;

    }

    public static BillAuditRes billAuditToResponse(BillAudit billAudit) {
        BillAuditRes bill = new BillAuditRes();

        bill.setComplete(billAudit.isComplete());
        bill.setDateCompleted(billAudit.getDateCompleted());
        bill.setBillAuditId(billAudit.getBillAuditId());

        BillingAccount billingAccount = billAudit.getBillingAccount();
        bill.setMonthlyAmount(billingAccount.getMonthlyAmount());

        accountToResponse(billingAccount, bill);

        return bill;
    }

    public static DebtAuditRes debtAuditToResponse(DebtAudit debtAudit) {
        DebtAuditRes debt = new DebtAuditRes();

        debt.setComplete(debtAudit.isComplete());
        debt.setDateCompleted(debtAudit.getDateCompleted());
        debt.setDebtAuditId(debtAudit.getDebtAuditId());

        DebtAccount debtAccount = debtAudit.getDebtAccount();
        debt.setMonthlyAmount(debtAccount.getMonthlyAmount());
        debt.setPaidAmount(debtAudit.getPaidAmount());

        accountToResponse(debtAccount, debt);

        return debt;
    }
}
