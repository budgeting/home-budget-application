package com.turnersoft.budgeting.utils.data;


import com.turnersoft.budgeting.domain.request.accounts.AccountReq;
import com.turnersoft.budgeting.domain.response.accounts.AccountResp;
import com.turnersoft.budgeting.model.Account;

public abstract class AccountUtils extends BaseDataUtils {


    /* ******* Account To Response ********** */

    public static void accountToResponse(Account account, AccountResp response) {

        if(account != null && response != null) {
            response.setDescription(account.getDescription());
            response.setName(account.getName());
            response.setUrl(account.getUrl());
            response.setPassword(account.getPassword());
            response.setUserName(account.getUserName());
        }

    }

    /* ******* Request To Account ********** */

    public static void requestToAccount(AccountReq request, Account account) {
        if(request != null && account != null) {
            account.setDescription(request.getDescription());
            account.setName(request.getName());
            account.setUrl(request.getUrl());
            account.setPassword(request.getPassword());
            account.setUserName(request.getUserName());
        }
    }

}
