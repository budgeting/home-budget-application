package com.turnersoft.budgeting.utils.data;


import com.turnersoft.budgeting.domain.request.accounts.personal.PersonalAccountReq;
import com.turnersoft.budgeting.domain.response.accounts.personal.PersonalAccountRes;
import com.turnersoft.budgeting.model.PersonalAccount;
import com.turnersoft.budgeting.model.auditing.Month;
import com.turnersoft.budgeting.model.billing.BillingAccount;
import com.turnersoft.budgeting.model.billing.PaymentAccount;
import com.turnersoft.budgeting.utils.Utils;

import java.util.List;
import java.util.Set;

public class PersonalAccountDataUtils extends AccountUtils {

    public static PersonalAccount requestToPersonalAccount(PersonalAccountReq request, PersonalAccount account) {

        requestToAccount(request, account);

        //Only set E-mail on Account Creation.
        if(account.getAccountId() == 0L) {
            account.setEmail(request.getEmail());
        }

        account.setSavePercantage(request.getSavePercentage());
        account.setMontlyEarnings(request.getMonthlyEarnings());

        populateAuditDetails(account, request, account.getAccountId() == 0L);

        return account;
    }


    public static PersonalAccountRes personalAccountToResponse(PersonalAccount account) {
        PersonalAccountRes response = null;

        if(account != null) {
            response = new PersonalAccountRes();
            accountToResponse(account, response);
            response.setEmail(account.getEmail());
            response.setSavePercentage(account.getSavePercantage());
            response.setMonthlyEarnings(account.getMontlyEarnings());
            response.setAccountId(account.getAccountId());
            response.setMonthlyBillExpenses(getTotalMonthlyAmounts(account.getBillingAccounts()));
            response.setMonthlyDebtExpenses(getTotalMonthlyAmounts(account.getDebtAccounts()));

            Month activeMonth = account.getActiveMonth();
            if(activeMonth != null) {
                response.setMonthId(activeMonth.getId());
            }

            double expenseMoney = account.getMontlyEarnings() - response.getMonthlyDebtExpenses() - response.getMonthlyBillExpenses();

            double savings = Utils.round((expenseMoney * (Double.valueOf(account.getSavePercantage()) /100)), 2);
            response.setSavings(savings);

            response.setLifeExpenses(Utils.round(expenseMoney - response.getSavings(), 2));
        }

        return  response;
    }

    private static double getTotalMonthlyAmounts(Set paymentAccounts) {
        double monthlyAmounts = 0;
        if(Utils.hasItems(paymentAccounts)) {

            for(Object payment : paymentAccounts) {

                monthlyAmounts += ((PaymentAccount) (payment)).getMonthlyAmount();
            }
        }

        return monthlyAmounts;
    }




}
