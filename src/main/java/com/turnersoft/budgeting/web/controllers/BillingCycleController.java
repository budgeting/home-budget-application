package com.turnersoft.budgeting.web.controllers;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.turnersoft.budgeting.domain.request.billingCycle.BillPaymentReq;
import com.turnersoft.budgeting.domain.request.billingCycle.BillingCycleReq;
import com.turnersoft.budgeting.domain.request.billingCycle.DebtPaymentReq;
import com.turnersoft.budgeting.domain.request.billingCycle.PayBillingCycleReq;
import com.turnersoft.budgeting.domain.response.Response;
import com.turnersoft.budgeting.domain.response.billingCycle.BillingCycleRes;
import com.turnersoft.budgeting.domain.response.billingCycle.history.BillingHistoryResp;
import com.turnersoft.budgeting.domain.response.billingCycle.debt.DebtAuditRes;
import com.turnersoft.budgeting.domain.response.billingCycle.history.BillingHistoryYearRangeResp;
import com.turnersoft.budgeting.domain.response.billingCycle.history.BillingHistoryYearlyResp;
import com.turnersoft.budgeting.domain.response.billingCycle.reoccuring.BillAuditRes;
import com.turnersoft.budgeting.model.auditing.BillAudit;
import com.turnersoft.budgeting.model.auditing.DebtAudit;
import com.turnersoft.budgeting.service.billing.BillingCycleService;
import com.turnersoft.budgeting.utils.LogUtils;
import com.turnersoft.budgeting.utils.data.BillingCycleDataUtils;
import com.turnersoft.budgeting.validation.Billing.BillingCycleValidator;
import com.turnersoft.budgeting.web.controllers.constants.HttpConstants;
import com.turnersoft.budgeting.web.utils.ControllerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/monthlyCycle")
public class BillingCycleController {

    @Autowired
    BillingCycleValidator billingCycleValidator;

    @Autowired
    BillingCycleService billingCycleService;

    private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();
    private static final String RESPONSE_TYPE = HttpConstants.JSON_TYPE;

    @RequestMapping(value = "/create", method = RequestMethod.POST, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response> createBillingCycle(@RequestBody BillingCycleReq request) {
        String method = "createBillingCycle(...): ";
        LogUtils.printRequest(method , gson.toJson(request));

        Response response = billingCycleValidator.validateBillingCycleCreation(request);

        if(ControllerUtils.isSuccessful(response)) {
            billingCycleService.createBillingCycle(request);
        }

        return ControllerUtils.getResponseEntity(response);

    }

    @RequestMapping(value = "/find", method = RequestMethod.POST, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response<BillingCycleRes>> findBillingCycle(@RequestBody BillingCycleReq request) {
        String method = "createBillingCycle(...): ";
        LogUtils.printRequest(method , gson.toJson(request));

        Response<BillingCycleRes> response = new Response<>();

        response.setPayload(BillingCycleDataUtils.monthToBillingCycleRes(billingCycleService.retrieveMonthlyBillingCycle(request.getPersonalAccId(), request.getMonth(), request.getYear())));

        return new ResponseEntity<Response<BillingCycleRes>>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/find/{id}", method = RequestMethod.GET, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response<BillingCycleRes>> findBillingCycleById(@PathVariable long id) {
        String method = "createBillingCycle(...): ";
        LogUtils.printRequest(method + " monthId: [" + id + "]");

        Response<BillingCycleRes> response = new Response<>();

        response.setPayload(BillingCycleDataUtils.monthToBillingCycleRes(billingCycleService.retrieveMonthlyBillingCycle(id)));

        return new ResponseEntity<Response<BillingCycleRes>>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/findByYear/{year}/accId/{accId}", method = RequestMethod.GET, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response<BillingHistoryYearlyResp>> findBillingHistoryByYear(@PathVariable int year, @PathVariable long accId) {
        String method = "findBillingHistoryByYear(...): ";
        LogUtils.printRequest(method + " year: [" + year + "] personalAccId: [" + accId + "]");

        Response<BillingHistoryYearlyResp> response = new Response<>();

        response.setPayload(BillingCycleDataUtils.billingHistoryToResponse(billingCycleService.retrieveBillingCycleHistory(accId, year)));

        return new ResponseEntity<Response<BillingHistoryYearlyResp>>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/findYearRange/{accId}", method = RequestMethod.GET, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response<BillingHistoryYearRangeResp>> findBillingHistoryYearRange(@PathVariable long accId) {
        String method = "findBillingHistoryByYear(...): ";
        LogUtils.printRequest(method + " personalAccId: [" + accId + "]");

        Response<BillingHistoryYearRangeResp> response = new Response<>();

        Integer minYear = billingCycleService.findBillingCycleMinYear(accId);

        if (minYear!= null) {
            Integer maxYear = billingCycleService.findBillingCycleMaxYear(accId);
            response.setPayload(new BillingHistoryYearRangeResp(minYear, maxYear));

        } else {
            response.setErrorMessage("There currently is nothing to process. Make sure you've created a monthly billing cycle.");
        }

        return new ResponseEntity<Response<BillingHistoryYearRangeResp>>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/payBill", method = RequestMethod.PUT, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response<BillAuditRes>> payMonthlyBill(@RequestBody BillPaymentReq request) {
        String method = "payMonthlyBill(...): ";
        LogUtils.printRequest(method , gson.toJson(request));

        Response<BillAuditRes> response = new Response<>();

        Response validationResponse = billingCycleValidator.validateBillPayment(request);
        if(ControllerUtils.isSuccessful(validationResponse)) {
            BillAudit billAudit = billingCycleService.payMonthlyBill(request.getBillAuditId());
            response.setPayload(BillingCycleDataUtils.billAuditToResponse(billAudit));

        } else {
            response.setErrorMessage(validationResponse.getMessage());
        }

        return new ResponseEntity<Response<BillAuditRes>> (response, HttpStatus.OK);

    }

    @RequestMapping(value = "/payDebt", method = RequestMethod.PUT, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response<DebtAuditRes>> payMonthlyDebt(@RequestBody DebtPaymentReq request) {
        String method = "payMonthlyDebt(...): ";
        LogUtils.printRequest(method , gson.toJson(request));

        Response<DebtAuditRes> response = new Response<>();

        Response validationResponse = billingCycleValidator.validateDebtPayment(request);
        if(ControllerUtils.isSuccessful(validationResponse)) {
           DebtAudit debtAudit = billingCycleService.payMonthlyDebtAmount(request.getDebtAuditId(), request.getAmount());
           response.setPayload(BillingCycleDataUtils.debtAuditToResponse(debtAudit));

        } else {
            response.setErrorMessage(validationResponse.getMessage());
        }

        return new ResponseEntity<Response<DebtAuditRes>> (response, HttpStatus.OK);
    }

    @RequestMapping(value = "/payHalf", method = RequestMethod.PUT, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response> payHalfOfBillingCycle(@RequestBody PayBillingCycleReq request) {
        String method = "payHalfOfBillingCycle(...): ";
        LogUtils.printRequest(method , gson.toJson(request));

        Response response = billingCycleValidator.validateBillingCyclePayment(request);

        if(ControllerUtils.isSuccessful(response)) {
            billingCycleService.payHalfOfBillingCycle(request);
        }


        return ControllerUtils.getResponseEntity(response);


    }




}


