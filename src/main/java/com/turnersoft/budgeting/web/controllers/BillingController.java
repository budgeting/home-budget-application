package com.turnersoft.budgeting.web.controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.turnersoft.budgeting.domain.request.accounts.billing.BillingAccountReq;
import com.turnersoft.budgeting.domain.request.accounts.billing.DebtAccountReq;
import com.turnersoft.budgeting.domain.response.Response;
import com.turnersoft.budgeting.domain.response.accounts.billing.debt.DebtAccountResp;
import com.turnersoft.budgeting.domain.response.accounts.billing.debt.DebtAccountsResp;
import com.turnersoft.budgeting.domain.response.accounts.billing.reoccuring.BillingAccountResp;
import com.turnersoft.budgeting.domain.response.accounts.billing.reoccuring.BillingAccountsResp;
import com.turnersoft.budgeting.model.PersonalAccount;
import com.turnersoft.budgeting.model.billing.BillingAccount;
import com.turnersoft.budgeting.model.billing.DebtAccount;
import com.turnersoft.budgeting.service.PersonalAccountService;
import com.turnersoft.budgeting.service.billing.BillingService;
import com.turnersoft.budgeting.utils.data.BillingDataUtils;
import com.turnersoft.budgeting.utils.LogUtils;
import com.turnersoft.budgeting.validation.Billing.DebtValidator;
import com.turnersoft.budgeting.web.controllers.constants.HttpConstants;
import com.turnersoft.budgeting.web.utils.ControllerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/billing")
public class BillingController {

    @Autowired
    BillingService billingService;

    @Autowired
    PersonalAccountService personalAccountService;

    @Autowired
    DebtValidator debtValidator;

    private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();
    private static final String RESPONSE_TYPE = HttpConstants.JSON_TYPE;

    /* ************************************ Debt Accounts ********************************************/

    @RequestMapping(value = "/create/debtAcc", method = RequestMethod.POST, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response> createBankAccount(@RequestBody DebtAccountReq request) {
        String method = "createDebtAccount(...): ";
        LogUtils.printRequest(method, gson.toJson(request));

        Response response = debtValidator.validate(request);

        if(ControllerUtils.isSuccessful(response)) {
            billingService.saveDebtAccount(BillingDataUtils.requestToDebtAccount(request, new DebtAccount(personalAccountService.findAccount(request.getPersonalAccountId()))));
        }

        return ControllerUtils.getResponseEntity(response);
    }


    @RequestMapping(value = "/update/debtAcc", method = RequestMethod.PUT, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response> updateDebtAccount(@RequestBody DebtAccountReq request) {
        String method = "updateDebtAcc(...): ";
        LogUtils.printRequest(method, gson.toJson(request));

        Response response = debtValidator.validate((request));

        if(ControllerUtils.isSuccessful(response)) {
            DebtAccount debtAccount = billingService.findDebtAccount(request.getDebtAccountId());

            if( debtAccount != null) {

                billingService.saveDebtAccount(BillingDataUtils.requestToDebtAccount(request, debtAccount));
            } else {
                response.setErrorMessage("The Account specified does not exist in the system");
            }
        }

        return ControllerUtils.getResponseEntity(response);
    }

    @RequestMapping(value = "/debtAccs/timeformat/{personalAccId}", method = RequestMethod.GET, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response<DebtAccountsResp>> getDebtAccountsByTimeOfMonth(@PathVariable long personalAccId) {
        String method = "getDebtAccountsByTimeOfMonth(...): ";
        LogUtils.printRequest(method + "request received!");

        Response<DebtAccountsResp> response = new Response<DebtAccountsResp>();
        response.setPayload(BillingDataUtils.debtAccountsToResponse(billingService.findDebtsByTimeOfMonth(personalAccId)));
        LogUtils.printResponse(method, gson.toJson(response.getPayload()));


        return new ResponseEntity<Response<DebtAccountsResp>>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/debtAcc", method = RequestMethod.GET, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response<DebtAccountResp>> getDebtAccount(@RequestParam long accId) {
        String method = "getDebtAccount(...): ";
        LogUtils.printRequest(method + "request received. DebtAccId : [ " + accId + "] !");

        Response<DebtAccountResp> response = new Response<DebtAccountResp>();
        response.setPayload(BillingDataUtils.billingAccountsToResponse(billingService.findDebtAccount(accId)));

        return new ResponseEntity<Response<DebtAccountResp>>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/delete/debtAcc", method = RequestMethod.DELETE, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response> deleteDebtAcc(@RequestParam long accId) {
        String method = "deleteDebtAcc(...): ";
        LogUtils.printRequest(method + "debtAccId: [" + accId + "]");

        billingService.deleteDebtAcc(accId);
        return ControllerUtils.getResponseEntity(new Response());
    }


    @RequestMapping(value = "/deactivate/debtAcc", method = RequestMethod.POST, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response> deactivateDebtAcc(@RequestParam long accId) {
        String method = "deactivateDebtAcc(...): ";
        LogUtils.printRequest(method + "debtAccId: [" + accId + "]");

        billingService.deactivateDebtAcc(accId);
        return ControllerUtils.getResponseEntity(new Response());
    }


    /* ************************************** Billing Accounts ********************************************/


    @RequestMapping(value = "/create/billingAcc", method = RequestMethod.POST, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response> createBillingAcc(@RequestBody BillingAccountReq request) {
        String method = "createBillingAcc(...): ";
        LogUtils.printRequest(method, gson.toJson(request));

        Response response = debtValidator.validate(request);

        if(ControllerUtils.isSuccessful(response)) {
            billingService.saveBillingAcc(BillingDataUtils.requestToBillingAccount(request, new BillingAccount(personalAccountService.findAccount(request.getPersonalAccountId()))));
        }

        return ControllerUtils.getResponseEntity(response);
    }


    @RequestMapping(value = "/update/billingAcc", method = RequestMethod.PUT, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response> updateBillingAcc(@RequestBody BillingAccountReq request) {
        String method = "updateBillingAcc(...): ";
        LogUtils.printRequest(method, gson.toJson(request));

        Response response = debtValidator.validate((request));

        if(ControllerUtils.isSuccessful(response)) {
            BillingAccount billingAccount = billingService.findBillingAcc(request.getBillAccountId());

            if( billingAccount != null) {

                billingService.saveBillingAcc(BillingDataUtils.requestToBillingAccount(request, billingAccount));
            } else {
                response.setErrorMessage("The Account specified does not exist in the system");
            }
        }

        return ControllerUtils.getResponseEntity(response);
    }


    @RequestMapping(value = "/billingAccs/timeformat/{personalAccId}", method = RequestMethod.GET, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response<BillingAccountsResp>> getBillingAccountsByTimeOfMonth(@PathVariable long personalAccId) {
        String method = "getBillingAccountsByTimeOfMonth(...): ";
        LogUtils.printRequest(method + "request received!");

        Response<BillingAccountsResp> response = new Response<BillingAccountsResp>();
        response.setPayload(BillingDataUtils.billingAccountsToResponse(billingService.findBillsByTimeOfMonth(personalAccId)));
        LogUtils.printResponse(method, gson.toJson(response.getPayload()));

        return new ResponseEntity<Response<BillingAccountsResp>>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/billingAcc", method = RequestMethod.GET, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response<BillingAccountResp>> getBillingAccount(@RequestParam long accId) {
        String method = "getBillingAccount(...): ";
        LogUtils.printRequest(method + "request received. BillingAccId : [" + accId + "] !");

        Response<BillingAccountResp> response = new Response<BillingAccountResp>();
        response.setPayload(BillingDataUtils.billingAccountToResponse(billingService.findBillingAcc(accId)));

        return new ResponseEntity<Response<BillingAccountResp>>(response, HttpStatus.OK);
    }



    @RequestMapping(value = "/delete/billingAcc", method = RequestMethod.DELETE, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response> deleteBillingAcc(@RequestParam long accId) {
        String method = "deleteBillingAcc(...): ";
        LogUtils.printRequest(method + "billingAccId: [" + accId + "]");

        billingService.deleteBillingAcc(accId);
        return ControllerUtils.getResponseEntity(new Response());
    }

    @RequestMapping(value = "/deactivate/billingAcc", method = RequestMethod.POST, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response> deactivateBillingAcc(@RequestParam long accId) {
        String method = "deactivateBillingAcc(...): ";
        LogUtils.printRequest(method + "accId: [" + accId + "]");

        billingService.deactivateBillingAcc(accId);
        return ControllerUtils.getResponseEntity(new Response());
    }





}
