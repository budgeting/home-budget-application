package com.turnersoft.budgeting.web.controllers;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.turnersoft.budgeting.domain.request.accounts.banking.BankAccountReq;
import com.turnersoft.budgeting.domain.request.accounts.banking.CardReq;
import com.turnersoft.budgeting.domain.response.accounts.banking.bank.BankAccountsResp;
import com.turnersoft.budgeting.domain.response.accounts.banking.card.CardsResp;
import com.turnersoft.budgeting.domain.response.Response;
import com.turnersoft.budgeting.model.PersonalAccount;
import com.turnersoft.budgeting.model.banking.BankAccount;
import com.turnersoft.budgeting.model.banking.Card;
import com.turnersoft.budgeting.service.PersonalAccountService;
import com.turnersoft.budgeting.service.banking.interfaces.BankService;
import com.turnersoft.budgeting.service.banking.interfaces.CardService;
import com.turnersoft.budgeting.utils.data.BankingDataUtils;
import com.turnersoft.budgeting.utils.LogUtils;
import com.turnersoft.budgeting.validation.Banking.BankValidator;
import com.turnersoft.budgeting.validation.Banking.card.CardValidator;
import com.turnersoft.budgeting.web.controllers.constants.HttpConstants;
import com.turnersoft.budgeting.web.utils.ControllerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/banking")
public class BankController {

    private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();
    public static final String RESPONSE_TYPE = HttpConstants.JSON_TYPE;


    @Autowired
    BankService bankService;

    @Autowired
    CardService cardService;

    @Autowired
    PersonalAccountService personalAccountService;

    @Autowired
    CardValidator updateCardValidator;

    @Autowired
    CardValidator newCardValidator;

    @Autowired
    BankValidator bankValidator;


    @RequestMapping(value = "/create/bankAcc", method = RequestMethod.POST, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response> createBankAccount(@RequestBody BankAccountReq request) {
        String method = "createBankAccount(...): ";
        LogUtils.printRequest(method, gson.toJson(request));

        Response response = bankValidator.validate(request);

        if(ControllerUtils.isSuccessful(response)) {
            bankService.saveBankAccount(BankingDataUtils.requestToBankAccount(request, new BankAccount(personalAccountService.findAccount(request.getPersonalAccountId()))));
        }

        return ControllerUtils.getResponseEntity(response);
    }

    @RequestMapping(value = "/update/bankAcc", method = RequestMethod.PUT, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response> updateBankAccount(@RequestBody BankAccountReq request) {
        String method = "updateBankAccount(...): ";
        LogUtils.printRequest(method, gson.toJson(request));

        Response response = bankValidator.validate((request));

        if(ControllerUtils.isSuccessful(response)) {
            BankAccount bankAccount = bankService.findBankAccount(request.getBankAccountId());

            if( bankAccount != null) {
                bankService.saveBankAccount(BankingDataUtils.requestToBankAccount(request, bankAccount));
            } else {
                response.setErrorMessage("The Account specified does not exist in the system");
            }
        }

        return ControllerUtils.getResponseEntity(response);
    }

    @RequestMapping(value = "/delete/bankAcc", method = RequestMethod.DELETE, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response> deleteBankAccount(@RequestParam long bankId) {
        String method = "deleteBankAccount(...): ";
        LogUtils.printRequest(method, "bankId: [" + bankId + "]");

        bankService.deleteBankAccount(bankId);
        return ControllerUtils.getResponseEntity(new Response());
    }


    @RequestMapping(value = "/findAllBanks/{personalAccId}", method = RequestMethod.GET, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response<BankAccountsResp>> findAllBankAccounts(@PathVariable long personalAccId) {
        Response<BankAccountsResp> response = new Response<BankAccountsResp>();

        String method  = "findAllBankAccounts(...): ";
        LogUtils.printRequest(method);

        BankAccountsResp bankAccountsResp = BankingDataUtils.bankAccountsToResponse(bankService.findAllBankAccounts(personalAccId));
        LogUtils.printResponse(method, gson.toJson(bankAccountsResp), "OK");

        response.setPayload(bankAccountsResp);

        return new ResponseEntity<Response<BankAccountsResp>>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/create/card", method = RequestMethod.POST, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response> createCard(@RequestBody CardReq cardReq) {
        String method = "createCard(...): ";
        LogUtils.printRequest(method, gson.toJson(cardReq));

        Response response = newCardValidator.validateCardRequest(cardReq);

        if(ControllerUtils.isSuccessful(response)) {
            cardService.saveCard(BankingDataUtils.requestToCard(cardReq, new Card(bankService.findBankAccount(cardReq.getBankAccId()))));
        }

        return ControllerUtils.getResponseEntity(response);
    }

    @RequestMapping(value = "/update/card", method = RequestMethod.PUT, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response> updateCard(@RequestBody CardReq cardReq) {
        String method = "updateCard(...): ";
        LogUtils.printRequest(method, gson.toJson(cardReq));
        Response response = new Response();

        Card card = cardService.find((cardReq.getCardId()));

        if(card != null) {

            response = updateCardValidator.validateCardRequest(cardReq);

            if(ControllerUtils.isSuccessful(response)) {
                cardService.saveCard(BankingDataUtils.requestToCard(cardReq, card));
            }

        } else {
            response.setErrorMessage("Unable to find card requested");
        }

        return ControllerUtils.getResponseEntity(response);

    }
    @RequestMapping(value = "/delete/card", method = RequestMethod.DELETE, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response> deleteCard(@RequestParam long cardId) {
        String method = "deleteCard(...): ";
        LogUtils.printRequest(method, "cardId: " + cardId);

        cardService.deleteCard(cardId);
        return ControllerUtils.getResponseEntity(new Response());
    }


    @RequestMapping(value = "/findAllCards", method = RequestMethod.GET, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response<CardsResp>> findAllCards(@RequestParam long bankId) {
        Response<CardsResp> response = new Response<>();

        String method  = "findAllBankAccounts(...): ";
        LogUtils.printRequest(method, "bankId: [" + bankId + "]");

        List<Card> cards = cardService.findAllCardsFromBank(bankId);
        CardsResp cardsResp = BankingDataUtils.cardsToResponse(cardService.findAllCardsFromBank(bankId));

        response.setPayload(cardsResp);

        LogUtils.printResponse(method, gson.toJson(cardsResp), "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    private Card getCardById(long id) {
        return cardService.find(id);
    }



}
