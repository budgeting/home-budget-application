package com.turnersoft.budgeting.web.controllers;

import com.turnersoft.budgeting.dao.banking.interfaces.CardDao;
import com.turnersoft.budgeting.model.AuditFields;
import com.turnersoft.budgeting.model.Test;
import com.turnersoft.budgeting.model.banking.BankAccount;
import com.turnersoft.budgeting.model.banking.Card;
import com.turnersoft.budgeting.model.banking.constants.CardType;
import com.turnersoft.budgeting.service.test.TestServiceImpl;
import com.turnersoft.budgeting.service.banking.interfaces.BankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDateTime;
import java.util.List;

@Controller
public class TestController {

    @Autowired
    TestServiceImpl testService;

    @Autowired
    CardDao cardDao;

    @Autowired
    BankService bankService;

    @RequestMapping(value = "/test", method = RequestMethod.GET, produces={"application/json"})
    @ResponseBody
    public ResponseEntity<Test> test() {
        Test test = new Test();
        test.setTestId(1L);
        test.setText("Test Message");
        return new ResponseEntity<Test>(test, HttpStatus.OK);
    }


}
