package com.turnersoft.budgeting.web.controllers;


import com.fasterxml.jackson.databind.node.TextNode;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.turnersoft.budgeting.domain.request.accounts.personal.PersonalAccountReq;
import com.turnersoft.budgeting.domain.response.Response;
import com.turnersoft.budgeting.domain.response.accounts.AccountResp;
import com.turnersoft.budgeting.domain.response.accounts.personal.PersonalAccountRes;
import com.turnersoft.budgeting.model.PersonalAccount;
import com.turnersoft.budgeting.service.PersonalAccountService;
import com.turnersoft.budgeting.utils.LogUtils;
import com.turnersoft.budgeting.utils.Utils;
import com.turnersoft.budgeting.utils.data.PersonalAccountDataUtils;
import com.turnersoft.budgeting.validation.personal.PersonalAccountValidator;
import com.turnersoft.budgeting.web.controllers.constants.HttpConstants;
import com.turnersoft.budgeting.web.utils.ControllerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/personalAccount")
public class PersonalAccountController {

    private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();
    private static final String RESPONSE_TYPE = HttpConstants.JSON_TYPE;

    @Autowired
    PersonalAccountService personalAccountService;

    @Autowired
    PersonalAccountValidator personalAccountValidator;


    @RequestMapping(value = "/create", method = RequestMethod.POST, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response> createPersonalAccount(@RequestBody PersonalAccountReq request) {
        String method = "createPersonalAccount(...): ";
        LogUtils.printRequest(method, gson.toJson(request));

        Response response = personalAccountValidator.validate(request);

        if(ControllerUtils.isSuccessful(response)) {
            personalAccountService.saveAccount(PersonalAccountDataUtils.requestToPersonalAccount(request, new PersonalAccount()));
        }

        return ControllerUtils.getResponseEntity(response);
    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response> updatePersonalAccount(@RequestBody PersonalAccountReq request) {
        String method = "updatePersonalAccount(...): ";
        LogUtils.printRequest(method, gson.toJson(request));

        Response response = personalAccountValidator.validate(request);

        if(ControllerUtils.isSuccessful(response)) {
            PersonalAccount account = personalAccountService.findAccount(request.getAccountId());

            if( account != null) {
                personalAccountService.saveAccount(PersonalAccountDataUtils.requestToPersonalAccount(request,account));
            } else {
                response.setErrorMessage("The Account specified does not exist in the system");
            }
        }

        return ControllerUtils.getResponseEntity(response);
    }

    @RequestMapping(value = "/updatePassword", method = RequestMethod.PUT, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response> updatePersonalAccountPassword(@RequestBody PersonalAccountReq request) {
        String method = "updatePersonalAccountPassword(...): ";
        LogUtils.printRequest(method, gson.toJson(request));

        Response response = personalAccountValidator.validatePassword(request);

        if(ControllerUtils.isSuccessful(response)) {
            PersonalAccount account = personalAccountService.findAccount(request.getAccountId());

            if( account != null) {
                account.setPassword(request.getPassword());
                personalAccountService.saveAccount(account);
            } else {
                response.setErrorMessage("The Account specified does not exist in the system");
            }
        }

        return ControllerUtils.getResponseEntity(response);
    }

    @RequestMapping(value = "/updateEarnings", method = RequestMethod.PUT, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response> updatePersonalAccountEarnings(@RequestBody PersonalAccountReq request) {
        String method = "updatePersonalAccountEarnings(...): ";
        LogUtils.printRequest(method, gson.toJson(request));
        Response response = new Response();

        PersonalAccount account = personalAccountService.findAccount(request.getAccountId());

        if( account != null) {
            account.setMontlyEarnings(request.getMonthlyEarnings());
            personalAccountService.saveAccount(account);
        } else {
            response.setErrorMessage("The Account specified does not exist in the system");

        }


        return ControllerUtils.getResponseEntity(response);
    }

    @RequestMapping(value = "/updateFinances", method = RequestMethod.PUT, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response> updatePersonalAccountFinances(@RequestBody PersonalAccountReq request) {
        String method = "updatePersonalAccountFinances(...): ";
        LogUtils.printRequest(method, gson.toJson(request));
        Response response = new Response();

        PersonalAccount account = personalAccountService.findAccount(request.getAccountId());

        if( account != null) {
            account.setSavePercantage(request.getSavePercentage());
            personalAccountService.saveAccount(account);
        } else {
            response.setErrorMessage("The Account specified does not exist in the system");
        }


        return ControllerUtils.getResponseEntity(response);
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response<PersonalAccountRes>> loginIn(@RequestBody PersonalAccountReq request) {
        String method = "loginIn(...): ";
        LogUtils.printRequest(method, gson.toJson(request));
        Response<PersonalAccountRes> response = new Response();

        PersonalAccount account = personalAccountService.findByEmail(request.getEmail());

        if( account != null) {

            if(Utils.isEqual(account.getPassword(), request.getPassword())) {
                response.setPayload(PersonalAccountDataUtils.personalAccountToResponse(account));
            } else {
                response.setErrorMessage("The password for this account is not valid, please try again.");
            }

        } else {
            response.setErrorMessage("The Account specified does not exist in the system");
        }


        return new ResponseEntity<Response<PersonalAccountRes>>(response, HttpStatus.OK);
    }


    @RequestMapping(value = "/find", method = RequestMethod.GET, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response<PersonalAccountRes>> findPersonalAccount(@RequestParam long id) {
        String method = "findPersonalAccount(...): ";
        LogUtils.printRequest(method + "Account Id: [" + id + "] ");

        PersonalAccountRes response =  PersonalAccountDataUtils.personalAccountToResponse(personalAccountService.findAccount(id));
        Response<PersonalAccountRes> serviceResponse = new Response<>();
        serviceResponse.setPayload(response);

        return new ResponseEntity<Response<PersonalAccountRes>>(serviceResponse, HttpStatus.OK);
    }

    @RequestMapping(value = "/findByEmail", method = RequestMethod.PUT, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<AccountResp> findPersonalAccountByEmail(@RequestBody PersonalAccountReq request) {
        String method = "findPersonalAccountByEmail(...): ";
        LogUtils.printRequest(method + " Email: [" + request.getEmail() +"]");

        AccountResp response  = PersonalAccountDataUtils.personalAccountToResponse(personalAccountService.findByEmail(request.getEmail()));

        return new ResponseEntity<AccountResp>(response, HttpStatus.OK);
    }


    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces={RESPONSE_TYPE})
    @ResponseBody
    public ResponseEntity<Response> deletePersonalAccount(@RequestParam long id) {
        String method = "deletePersonalAccount(...): ";
        LogUtils.printRequest(method + "Account Id: [" + id + "]");

        personalAccountService.deleteAccount(id);

        return ControllerUtils.getResponseEntity(new Response());
    }



}
