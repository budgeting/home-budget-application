package com.turnersoft.budgeting.web.utils;


import com.turnersoft.budgeting.domain.response.Response;
import com.turnersoft.budgeting.domain.response.Status;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ControllerUtils {

    public static boolean isSuccessful(Response response) {
        return response != null && response.getStatus() != null && response.getStatus().equals(Status.OK);
    }

    public static ResponseEntity<Response> getResponseEntity(Response response) {
        return new ResponseEntity<Response>(response, HttpStatus.OK);
    }
}
