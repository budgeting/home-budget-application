package com.turnersoft.budgeting.model.billing;


import com.turnersoft.budgeting.model.Account;
import com.turnersoft.budgeting.model.PersonalAccount;

import javax.persistence.*;

@Entity
@Table(name = "DEBT_ACC")
@NamedQueries({
        @NamedQuery(name = "DebtAccount.findByTimeOfPayment", query = "select d from DebtAccount d where d.timeOfPayment = :timeOfPayment and d.recordStatus = 'A' and d.personalAccount.accountId = :personalAccId and d.recordStatus = 'A'"),
        @NamedQuery(name = "DebtAccount.findActiveAccounts" , query = "select d from DebtAccount d where d.recordStatus = 'A' and d.personalAccount.accountId = :personalAccId")
})
public class DebtAccount extends PaymentAccount {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "DEBT_ACC_ID")
    private long debtAccountId;

    @Column(name = "INTEREST")
    private double interest;

    @Column(name = "TOTAL_REMAINING")
    private double totalRemaining;

    @Column(name = "ORIGINAL_AMOUNT")
    private double originalAmount;

    public DebtAccount(PersonalAccount personalAccount) {
        setPersonalAccount(personalAccount);
    }

    public DebtAccount() {}


    public long getDebtAccountId() {
        return debtAccountId;
    }

    public void setDebtAccountId(long debtAccountId) {
        this.debtAccountId = debtAccountId;
    }

    public double getInterest() {
        return interest;
    }

    public void setInterest(double interest) {
        this.interest = interest;
    }

    public double getTotalRemaining() {
        return totalRemaining;
    }

    public void setTotalRemaining(double totalRemaining) {
        this.totalRemaining = totalRemaining;
    }

    public double getOriginalAmount() {
        return originalAmount;
    }

    public void setOriginalAmount(double originalAmount) {
        this.originalAmount = originalAmount;
    }

    public void payOffAccount() {
        this.totalRemaining = 0;
        setMonthlyAmount(0);
        setRecordStatus("D");

    }

}
