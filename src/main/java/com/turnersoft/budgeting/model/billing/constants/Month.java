package com.turnersoft.budgeting.model.billing.constants;

public enum Month {
    JAN("January", 1, "01"),
    FEB("February", 2, "02"),
    MAR("March", 3, "03"),
    APR("April", 4, "04"),
    MAY("May", 5, "05"),
    JUN("June", 6, "06"),
    JUL("July", 7, "07"),
    AUG("August", 8, "08"),
    SEP("September", 9, "09"),
    OCT("October", 10, "10"),
    NOV("November", 11, "11"),
    DEC("December", 12, "12");


    private String value;
    private String numberValue;
    private int order;

     Month(String value, int order, String numberValue) {
        this.value = value;
        this.order = order;
        this.numberValue = numberValue;
    }

    public String getValue() {
         return value;
    }

    public int getOrder() {
         return order;
    }

    public String getNumberValue() {
         return numberValue;
    }
}
