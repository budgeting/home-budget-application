package com.turnersoft.budgeting.model.billing.constants;


public enum TimeOfMonth {

    FIRST("FIRST"),
    SECOND("SECOND");

    private String value;

     TimeOfMonth(String value) {
        this.value = value;
    }

    public String getValue() {
         return value;
    }
}
