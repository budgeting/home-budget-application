package com.turnersoft.budgeting.model.billing;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.turnersoft.budgeting.model.Account;
import com.turnersoft.budgeting.model.PersonalAccount;
import com.turnersoft.budgeting.model.auditing.BillAudit;
import com.turnersoft.budgeting.model.banking.BankAccount;

import javax.persistence.*;

@Entity
@Table(name = "BILL_ACC")
@NamedQueries({
        @NamedQuery(name = "BillingAccount.findByTimeOfPayment", query = "select b from BillingAccount b where b.timeOfPayment = :timeOfPayment and b.recordStatus = 'A' and b.personalAccount.accountId = :personalAccId"),
        @NamedQuery(name = "BillingAccount.findActiveAccounts", query = "select b from BillingAccount b where b.recordStatus = 'A' and b.personalAccount.accountId = :personalAccId")
})
public class BillingAccount extends PaymentAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "BILL_ACC_ID")
    private long billAccountId;

    public BillingAccount(PersonalAccount personalAccount) {
        setPersonalAccount(personalAccount);
    }

    public BillingAccount() {}



    public long getBillAccountId() {
        return billAccountId;
    }

    public void setBillAccountId(long billAccountId) {
        this.billAccountId = billAccountId;
    }
}
