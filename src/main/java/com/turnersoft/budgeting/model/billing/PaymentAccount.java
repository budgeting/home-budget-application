package com.turnersoft.budgeting.model.billing;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.turnersoft.budgeting.model.Account;
import com.turnersoft.budgeting.model.PersonalAccount;
import com.turnersoft.budgeting.model.banking.BankAccount;
import com.turnersoft.budgeting.model.banking.Card;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@MappedSuperclass
public abstract class PaymentAccount extends Account {

    @Column(name = "MONTHLY_AMOUNT")
    private double monthlyAmount;

    @Column(name = "TIME_OF_PAYMENT")
    private String timeOfPayment;

    @ManyToOne @JsonIgnore
    @JoinColumn(name = "PERSONAL_ACC_ID",nullable = false)
    private PersonalAccount personalAccount;


    public double getMonthlyAmount() {
        return monthlyAmount;
    }


    public String getTimeOfPayment() {
        return timeOfPayment;
    }

    public void setTimeOfPayment(String timeOfPayment) {
        this.timeOfPayment = timeOfPayment;
    }

    public void setMonthlyAmount(double monthlyAmount) {
        this.monthlyAmount = monthlyAmount;
    }


    public PersonalAccount getPersonalAccount() {
        return personalAccount;
    }

    public void setPersonalAccount(PersonalAccount personalAccount) {
        this.personalAccount = personalAccount;
    }
}
