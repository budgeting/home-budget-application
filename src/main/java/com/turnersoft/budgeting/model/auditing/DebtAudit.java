package com.turnersoft.budgeting.model.auditing;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.turnersoft.budgeting.model.AuditFields;
import com.turnersoft.budgeting.model.banking.BankAccount;
import com.turnersoft.budgeting.model.billing.BillingAccount;
import com.turnersoft.budgeting.model.billing.DebtAccount;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "DEBT_AUDIT", uniqueConstraints = {@UniqueConstraint(columnNames = {"month_id", "debt_acc_id"})})
public class DebtAudit extends PaymentAudit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "DEBT_AUDIT_ID")
    private long debtAuditId;

    @Column(name = "PAID_AMOUNT")
    private double paidAmount;

    @ManyToOne @JsonIgnore
    @JoinColumn(name = "DEBT_ACC_ID",nullable = false)
    private DebtAccount debtAccount;

    public DebtAudit() {}

    public DebtAudit(Month month, DebtAccount account) {
        setMonth(month);
        this.debtAccount = account;
    }

    public long getDebtAuditId() {
        return debtAuditId;
    }

    public void setDebtAuditId(long debtAuditId) {
        this.debtAuditId = debtAuditId;
    }


    public DebtAccount getDebtAccount() {
        return debtAccount;
    }

    public void setDebtAccount(DebtAccount debtAccount) {
        this.debtAccount = debtAccount;
    }

    public double getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(double paidAmount) {
        this.paidAmount = paidAmount;
    }

    @Override
    public void completePayment() {
        super.completePayment();
        paidAmount = debtAccount.getMonthlyAmount();
    }
}
