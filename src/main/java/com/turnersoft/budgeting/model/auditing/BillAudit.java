package com.turnersoft.budgeting.model.auditing;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.turnersoft.budgeting.model.AuditFields;
import com.turnersoft.budgeting.model.banking.BankAccount;
import com.turnersoft.budgeting.model.billing.BillingAccount;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "BILL_AUDIT", uniqueConstraints = {@UniqueConstraint(columnNames = {"month_id", "bill_acc_id"})})
public class BillAudit extends PaymentAudit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "BILL_AUDIT_ID")
    private long billAuditId;

    @ManyToOne @JsonIgnore
    @JoinColumn(name = "BILL_ACC_ID",nullable = false)
    private BillingAccount billingAccount;

    @Column(name = "AMOUNT_PAID")
    private double amountPaid;

    public BillAudit(Month month, BillingAccount billingAccount) {
        setMonth(month);
        this.billingAccount =  billingAccount;
    }

    public BillAudit() {}

    public long getBillAuditId() {
        return billAuditId;
    }

    public void setBillAuditId(long billAuditId) {
        this.billAuditId = billAuditId;
    }

    public BillingAccount getBillingAccount() {
        return billingAccount;
    }

    public void setBillingAccount(BillingAccount billingAccount) {
        this.billingAccount = billingAccount;
    }

    public double getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(double amountPaid) {
        this.amountPaid = amountPaid;
    }
}
