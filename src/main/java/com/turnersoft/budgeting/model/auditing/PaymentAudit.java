package com.turnersoft.budgeting.model.auditing;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.turnersoft.budgeting.model.AuditFields;

import javax.persistence.*;
import java.time.LocalDateTime;

@MappedSuperclass
public abstract class PaymentAudit extends AuditFields {

    @Column(name="COMPLETE")
    private boolean complete = false;

    @Column(name="DATE_COMPLETE")
    private LocalDateTime dateCompleted;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "MONTH_ID",nullable = false)
    private Month month;

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    public LocalDateTime getDateCompleted() {
        return dateCompleted;
    }

    public void setDateCompleted(LocalDateTime dateCompleted) {
        this.dateCompleted = dateCompleted;
    }

    public Month getMonth() {
        return month;
    }

    public void setMonth(Month month) {
        this.month = month;
    }

    public void completePayment() {
        this.complete = true;
        this.dateCompleted = LocalDateTime.now();
    }

    public long getPersonalAccId() {
        return month.getPersonalAccount().getAccountId();
    }
}

