package com.turnersoft.budgeting.model.auditing;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.turnersoft.budgeting.model.AuditFields;
import com.turnersoft.budgeting.model.PersonalAccount;
import com.turnersoft.budgeting.model.banking.Card;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "MONTH", uniqueConstraints = {@UniqueConstraint(columnNames = {"month", "year", "personal_acc_id"})})
@NamedQueries({
        @NamedQuery(name = "Month.findByYear", query = "select m from Month m where m.year = :year and personalAccount.accountId = :accountId"),
        @NamedQuery(name = "Month.findByMonthAndYear", query = "select m from Month m where m.year = :year and m.month = :month and personalAccount.accountId = :accountId"),
        @NamedQuery(name = "Month.findMinYearByAccount", query ="select min(m.year) from Month m where personalAccount.accountId = :accountId"),
        @NamedQuery(name = "Month.findMaxYearByAccount", query ="select max(m.year) from Month m where personalAccount.accountId = :accountId")
})
public class Month extends AuditFields {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "MONTH_ID")
    private long id;

    @ManyToOne @JsonIgnore
    @JoinColumn(name = "PERSONAL_ACC_ID",nullable = false)
    private PersonalAccount personalAccount;

    @Column(name = "YEAR")
    private int year;

    @Column(name = "MONTH")
    private String month;

    @Column(name = "EARNED")
    private double earned;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "month")
    private Set<DebtAudit> debts = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "month")
    private Set<BillAudit> bills = new HashSet<>();


    public Month(String month, int year) {
        this.month = month;
        this.year = year;
    }

    public Month() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public double getEarned() {
        return earned;
    }

    public void setEarned(double earned) {
        this.earned = earned;
    }

    public Set<DebtAudit> getDebts() {
        return debts;
    }

    public void setDebts(Set<DebtAudit> debts) {
        this.debts = debts;
    }

    public Set<BillAudit> getBills() {
        return bills;
    }

    public void setBills(Set<BillAudit> bills) {
        this.bills = bills;
    }

    public PersonalAccount getPersonalAccount() {
        return personalAccount;
    }

    public void setPersonalAccount(PersonalAccount personalAccount) {
        this.personalAccount = personalAccount;
    }



}
