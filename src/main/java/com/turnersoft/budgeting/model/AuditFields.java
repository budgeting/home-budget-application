package com.turnersoft.budgeting.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@MappedSuperclass
public abstract class AuditFields implements Serializable {

    public static String ACTIVE = "A";
    public static String DEACTIVE = "D";

    @Column(name = "RECORD_STATUS")
    private String recordStatus = ACTIVE;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATE_MODULE")
    private String createModule;

    @Column(name = "CREATE_TIMESTAMP")
    private LocalDateTime createTimestamp;

    @Column(name = "UPDATE_MODULE")
    private String updateModule;

    @Column(name = "UPDATED_BY")
    private String updatedBy;

    @Column(name = "UPDATE_TIMESTAMP")
    private LocalDateTime updateTimestmap;

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreateModule() {
        return createModule;
    }

    public void setCreateModule(String createModule) {
        this.createModule = createModule;
    }

    public LocalDateTime getCreateTimestamp() {
        return createTimestamp;
    }

    public void setCreateTimestamp(LocalDateTime createTimestamp) {
        this.createTimestamp = createTimestamp;
    }

    public String getUpdateModule() {
        return updateModule;
    }

    public void setUpdateModule(String updateModule) {
        this.updateModule = updateModule;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public LocalDateTime getUpdateTimestmap() {
        return updateTimestmap;
    }

    public void setUpdateTimestmap(LocalDateTime updateTimestmap) {
        this.updateTimestmap = updateTimestmap;
    }

    public void activate() {
        this.recordStatus = ACTIVE;
    }

    public void deactivate() {
        this.recordStatus = DEACTIVE;
    }


}
