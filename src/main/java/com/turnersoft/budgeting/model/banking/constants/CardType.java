package com.turnersoft.budgeting.model.banking.constants;


public enum CardType {

    DEBIT("DEBIT", "Debit"),
    CREDIT("CREDIT", "Credit");

    private String value;
    private String display;

    CardType(String value, String display) {
        this.value = value;
        this.display = display;
    }

    public String getValue() {
        return value;
    }

    public String getDisplay() {
        return display;
    }
}
