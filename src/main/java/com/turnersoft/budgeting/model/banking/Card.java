package com.turnersoft.budgeting.model.banking;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.turnersoft.budgeting.model.AuditFields;
import com.turnersoft.budgeting.model.banking.constants.CardType;

import javax.persistence.*;

@Entity
@Table(name = "CARD", uniqueConstraints = {@UniqueConstraint(columnNames = {"BANK_ACC_ID", "TYPE"})})
@NamedQueries({
        @NamedQuery(name ="Card.findByBank", query = "select c from Card c where c.bankAccount =:bankId"),
        @NamedQuery(name = "Card.findByName", query = "select c from Card c where c.name = :name"),
        @NamedQuery(name = "Card.findByBankAndType", query = "select c from Card c where c.bankAccount =:bankId and c.type = :type")
})
public class Card extends AuditFields {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CARD_ID")
    private long cardId;

    @Column(name = "FULL_NAME")
    private String fullName;

    @Column(name = "CARD_NUMBER")
    private String cardNumber;

    @Column(name = "CVV")
    private String cvv;

    @Column(name = "VENDOR")
    private String vendor;

    @Column(name = "NAME")
    private String name;

    @Column(name = "EXPIRATION_DATE")
    private String expirationDate;

    @Column(name = "TYPE")
    private String type;

    @ManyToOne @JsonIgnore
    @JoinColumn(name = "BANK_ACC_ID",nullable = false)
    private BankAccount bankAccount;

    public Card() {}

    public Card (BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public long getCardId() {
        return cardId;
    }

    public void setCardId(long cardId) {
        this.cardId = cardId;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public CardType getCardType() {
        return CardType.valueOf(type);
    }
}
