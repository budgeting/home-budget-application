package com.turnersoft.budgeting.model.banking;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.turnersoft.budgeting.model.Account;
import com.turnersoft.budgeting.model.PersonalAccount;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "BANK_ACC", uniqueConstraints = {@UniqueConstraint(columnNames = { "personal_acc_id", "name"})})
@NamedQueries({
        @NamedQuery(name = "BankAccount.findByName", query = "select b from BankAccount b where b.name = :name and b.personalAccount.accountId = :personalAccId"),
        @NamedQuery(name = "BankAccount.findActiveAccounts" , query = "select b from BankAccount b where b.recordStatus = 'A' and b.personalAccount.accountId = :personalAccId")

})
public class BankAccount extends Account {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "BANK_ACC_ID")
    private long bankAccountId;


    @Column(name = "ROUTING_NUMBER")
    private String routingNumber;

    @Column(name = "ACOUNT_NUMBER")
    private String accountNumber;

    @ManyToOne @JsonIgnore
    @JoinColumn(name = "PERSONAL_ACC_ID",nullable = false)
    private PersonalAccount personalAccount;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "bankAccount")
    private List<Card> accountCards = new ArrayList<Card>();

    public BankAccount(PersonalAccount personalAccount) {
        this.personalAccount = personalAccount;
    }

    public BankAccount() {}

    public PersonalAccount getPersonalAccount() {
        return personalAccount;
    }

    public void setPersonalAccount(PersonalAccount personalAccount) {
        this.personalAccount = personalAccount;
    }

    public long getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(long bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    public String getRoutingNumber() {
        return routingNumber;
    }

    public void setRoutingNumber(String routingNumber) {
        this.routingNumber = routingNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public void setAccountCards(List<Card> accountCards) {
        this.accountCards = accountCards;
    }

    public List<Card> getAccountCards() {
        return accountCards;
    }

    public void addCard(Card card) {

        if(card != null) {
            accountCards.add(card);
        }
    }
}
