package com.turnersoft.budgeting.model.banking.constants;

/**
 * Created by turne on 8/23/2017.
 */
public enum TimeOfMonth {
    FIRST("FIRST"),
    SECOND("SECOND");

    private String value;
     TimeOfMonth(String value) {
        this.value = value;
    }

    public String getValue() {
         return value;
    }


}
