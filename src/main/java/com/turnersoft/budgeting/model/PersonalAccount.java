package com.turnersoft.budgeting.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.turnersoft.budgeting.model.auditing.Month;
import com.turnersoft.budgeting.model.banking.BankAccount;
import com.turnersoft.budgeting.model.billing.BillingAccount;
import com.turnersoft.budgeting.model.billing.DebtAccount;
import com.turnersoft.budgeting.model.billing.PaymentAccount;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "PERSONAL_ACC", uniqueConstraints = {@UniqueConstraint(columnNames = {"email"})})
@NamedQueries({
        @NamedQuery(name = "PersonalAccount.findByEmail", query = "select a from PersonalAccount a where a.email = :email")

})
public class PersonalAccount extends Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ACC_ID")
    long accountId;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "SAVE_PERCENTAGE")
    private int savePercantage;

    @Column(name = "MONTHLY_EARNINGS")
    private double montlyEarnings;

    @OneToOne
    @JoinColumn(name = "ACTIVE_MONTH_ID",nullable = true)
    private Month activeMonth;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "personalAccount")
    private Set<Month> months = new HashSet<>();

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "personalAccount")
    private Set<BillingAccount> billingAccounts = new HashSet<>();

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "personalAccount")
    private Set<DebtAccount> debtAccounts = new HashSet<>();

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "personalAccount")
    private Set<BankAccount> bankAccounts = new HashSet<>();

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getSavePercantage() {
        return savePercantage;
    }

    public void setSavePercantage(int savePercantage) {
        this.savePercantage = savePercantage;
    }

    public double getMontlyEarnings() {
        return montlyEarnings;
    }

    public void setMontlyEarnings(double montlyEarnings) {
        this.montlyEarnings = montlyEarnings;
    }

    public Set<BillingAccount> getBillingAccounts() {
        return billingAccounts;
    }

    public void setBillingAccounts(Set<BillingAccount> billingAccounts) {
        this.billingAccounts = billingAccounts;
    }

    public Set<DebtAccount> getDebtAccounts() {
        return debtAccounts;
    }

    public void setDebtAccounts(Set<DebtAccount> debtAccounts) {
        this.debtAccounts = debtAccounts;
    }

    public Set<BankAccount> getBankAccounts() {
        return bankAccounts;
    }

    public void setBankAccounts(Set<BankAccount> bankAccounts) {
        this.bankAccounts = bankAccounts;
    }

    public Set<Month> getMonths() {
        return months;
    }

    public void setMonths(Set<Month> months) {
        this.months = months;
    }

    public Month getActiveMonth() {
        return activeMonth;
    }

    public void setActiveMonth(Month activeMonth) {
        this.activeMonth = activeMonth;
    }
}
