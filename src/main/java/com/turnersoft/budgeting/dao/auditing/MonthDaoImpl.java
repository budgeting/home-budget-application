package com.turnersoft.budgeting.dao.auditing;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.turnersoft.budgeting.dao.JpaTemplateImpl;
import com.turnersoft.budgeting.dao.auditing.interfaces.MonthDao;
import com.turnersoft.budgeting.model.auditing.BillAudit;
import com.turnersoft.budgeting.model.auditing.DebtAudit;
import com.turnersoft.budgeting.model.auditing.Month;
import com.turnersoft.budgeting.model.banking.Card;
import com.turnersoft.budgeting.utils.Utils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MonthDaoImpl extends JpaTemplateImpl implements MonthDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public void saveMonth(Month month) {
        save(month);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Month> findMonthsByYear(int year, long personalAccId) {
        Session session = sessionFactory.openSession();
        List<Month> months = session.getNamedQuery("Month.findByYear")
                .setParameter("year", year).setParameter("accountId", personalAccId)
                .getResultList();

        session.close();

        return months;
    }

    @Override
    public Month find(long id) {
        return (Month) find(id, Month.class);
    }

    @Override
    public Month findByNameAndYear(String month, int year, long personalAccId) {

        Session session = sessionFactory.openSession();
        List<Month> months = session.getNamedQuery("Month.findByMonthAndYear")
                .setParameter("year", year).setParameter("month", month).setParameter("accountId", personalAccId)
                .getResultList();
        session.close();

        return Utils.hasItems(months) ? months.get(0) : null;
    }

    @Override

    public Integer findMinYearByAccount(long personalAccId) {
        Session session = sessionFactory.openSession();

        List<Integer> minYear = session.getNamedQuery("Month.findMinYearByAccount")
                .setParameter("accountId", personalAccId)
                .getResultList();
        session.close();

        return Utils.hasItems(minYear) ? minYear.get(0) : null;
    }

    @Override
    public Integer findMaxYearByAccount(long personalAccountId) {
        Session session = sessionFactory.openSession();

        List<Integer> maxYear = session.getNamedQuery("Month.findMaxYearByAccount")
                .setParameter("accountId", personalAccountId)
                .getResultList();
        session.close();

        return Utils.hasItems(maxYear) ? maxYear.get(0) : null;

    }

    @Override
    public void deleteMonth(long id) {
        deleteMonth(find(id));
    }

    @Override
    public void deleteMonth(Month month) {
        delete(month);
    }

    @Override
    public void saveBillAudit(BillAudit billAudit) {
        save(billAudit);
    }

    @Override
    public BillAudit findBillAudit(long billingAuditId) {
        return (BillAudit) find(billingAuditId, BillAudit.class);
    }

    @Override
    public DebtAudit findDebtAudit(long debtAuditId) {
        return (DebtAudit) find(debtAuditId, DebtAudit.class);
    }

    @Override
    public void saveDebtAudit(DebtAudit debtAudit) {
        save(debtAudit);
    }

}
