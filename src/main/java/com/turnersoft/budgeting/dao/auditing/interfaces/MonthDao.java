package com.turnersoft.budgeting.dao.auditing.interfaces;


import com.turnersoft.budgeting.model.auditing.BillAudit;
import com.turnersoft.budgeting.model.auditing.DebtAudit;
import com.turnersoft.budgeting.model.auditing.Month;
import com.turnersoft.budgeting.model.banking.BankAccount;
import com.turnersoft.budgeting.model.banking.Card;

import java.util.List;

public interface MonthDao {
    Month find(long id);
    List<Month> findMonthsByYear(int year, long personalAccId);
    Month findByNameAndYear(String month, int year, long personalAcId);
    Integer findMinYearByAccount(long personalAccId);
    Integer findMaxYearByAccount(long personalAccId);

    public BillAudit findBillAudit(long billingAuditId);
    public DebtAudit findDebtAudit(long debtAuditId);

    void saveMonth(Month month);
    public void saveDebtAudit(DebtAudit debtAudit);
    public void saveBillAudit(BillAudit billAudit);

    void deleteMonth(long id);
    void deleteMonth(Month month);


}
