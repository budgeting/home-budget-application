package com.turnersoft.budgeting.dao.test;


import com.turnersoft.budgeting.model.Test;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface TestDao {

    public List<Test> testDbConnection();
}
