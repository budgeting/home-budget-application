package com.turnersoft.budgeting.dao.test;


import com.turnersoft.budgeting.model.Test;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

@Repository
public class TestDaoImpl implements TestDao {

    @Autowired
    private SessionFactory sessionFactory;


    @Override
    public List<Test> testDbConnection() {
        // Open a session
        Session session = sessionFactory.openSession();

        // DEPRECATED as of Hibernate 5.2.0
        // List<Category> categories = session.createCriteria(Category.class).list();

        // Create CriteriaBuilder
        CriteriaBuilder builder = session.getCriteriaBuilder();

        // Create CriteriaQuery
        CriteriaQuery<Test> criteria = builder.createQuery(Test.class);

        // Specify criteria root
        criteria.from(Test.class);

        // Execute query
        List<Test> tests = session.createQuery(criteria).getResultList();

        // Close session
        session.close();

        return tests;
    }
}
