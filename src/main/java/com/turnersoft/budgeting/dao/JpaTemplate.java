package com.turnersoft.budgeting.dao;

import com.turnersoft.budgeting.model.AuditFields;
import org.hibernate.Session;


public interface JpaTemplate {
    public void save(Object object);
    public Object  find(long id, Class classItem);
    public void delete(Object object);
    public void deactivate(AuditFields object);

}
