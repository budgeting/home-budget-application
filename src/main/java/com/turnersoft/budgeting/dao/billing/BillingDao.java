package com.turnersoft.budgeting.dao.billing;


import com.turnersoft.budgeting.domain.response.accounts.billing.reoccuring.BillingAccountResp;
import com.turnersoft.budgeting.model.auditing.BillAudit;
import com.turnersoft.budgeting.model.auditing.DebtAudit;
import com.turnersoft.budgeting.model.billing.BillingAccount;
import com.turnersoft.budgeting.model.billing.DebtAccount;

import java.util.List;

public interface BillingDao {
    public void saveDebtAccount(DebtAccount account);
    public DebtAccount findDebtAcc(long debtAccountId);
    public List<DebtAccount> findDebtByTimeOfMonth(String timeOfMonth, long personalAccId);
    public List<DebtAccount> findDebtAccounts(long personalAccId);
    public void deleteDebtAcc(long debtAccId);
    public void deactivateDebtAcc(long debtAccId);


    public void saveBillingAcc(BillingAccount account);
    public BillingAccount findBillingAcc(long billingAccId);
    public List<BillingAccount> findBillingByTimeOfMonth(String timeOfMonth, long personalAccId);
    public List<BillingAccount> findBillingAccs(long personalAccId);
    public void deleteBillingAcc(long billingAccId);
    public void deactivateBillingAcc(long billingAccId);



}
