package com.turnersoft.budgeting.dao.billing;


import com.turnersoft.budgeting.dao.JpaTemplateImpl;
import com.turnersoft.budgeting.model.AuditFields;
import com.turnersoft.budgeting.model.auditing.BillAudit;
import com.turnersoft.budgeting.model.auditing.DebtAudit;
import com.turnersoft.budgeting.model.banking.BankAccount;
import com.turnersoft.budgeting.model.billing.BillingAccount;
import com.turnersoft.budgeting.model.billing.DebtAccount;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public class BillingDaoImpl extends JpaTemplateImpl implements  BillingDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public void saveDebtAccount(DebtAccount account) {
        save(account);
    }

    @Override
    public DebtAccount findDebtAcc(long debtAccountId) {
        return (DebtAccount) find(debtAccountId, DebtAccount.class);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<DebtAccount> findDebtByTimeOfMonth(String timeOfMonth, long personalAccId) {
        Session session = sessionFactory.openSession();
        List<DebtAccount> debtAccounts = (List<DebtAccount>) session.getNamedQuery("DebtAccount.findByTimeOfPayment")
                .setParameter("timeOfPayment", timeOfMonth).setParameter("personalAccId", personalAccId).getResultList();

        session.close();

        return  debtAccounts;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<DebtAccount> findDebtAccounts(long personalAccId) {
        Session session = sessionFactory.openSession();
        List<DebtAccount> accounts = (List<DebtAccount>) session.getNamedQuery("DebtAccount.findActiveAccounts")
                .setParameter("personalAccId", personalAccId).getResultList();
        session.close();

        return accounts;
    }

    @Override
    public void deleteDebtAcc(long debtAccId) {
        delete(findDebtAcc(debtAccId));
    }

    @Override
    public void deactivateDebtAcc(long debtAccId) {
        deactivate(findDebtAcc(debtAccId));
    }



    @Override
    public void saveBillingAcc(BillingAccount account) {
        save(account);
    }

    @Override
    public BillingAccount findBillingAcc(long billingAccId) {
        return (BillingAccount) find(billingAccId, BillingAccount.class);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<BillingAccount> findBillingByTimeOfMonth(String timeOfMonth, long personalAccId) {
        Session session = sessionFactory.openSession();

        List<BillingAccount> billingAccounts = (List<BillingAccount>) session.getNamedQuery("BillingAccount.findByTimeOfPayment")
                .setParameter("timeOfPayment", timeOfMonth).setParameter("personalAccId", personalAccId).getResultList();

        session.close();

        return  billingAccounts;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<BillingAccount> findBillingAccs(long personalAccId) {
        Session session = sessionFactory.openSession();
        List<BillingAccount> accounts = (List<BillingAccount>) session.getNamedQuery("BillingAccount.findActiveAccounts").setParameter("personalAccId", personalAccId).getResultList();
        session.close();

        return accounts;
    }

    @Override
    public void deleteBillingAcc(long billingAccId) {
        delete(findBillingAcc(billingAccId));
    }

    @Override
    public void deactivateBillingAcc(long billingAccId) {
        deactivate(findBillingAcc(billingAccId));
    }




}
