package com.turnersoft.budgeting.dao.banking.interfaces;

import com.turnersoft.budgeting.model.banking.BankAccount;
import com.turnersoft.budgeting.model.banking.Card;

import java.util.List;

public interface CardDao {
    void saveCard(Card card);
    List<Card> findAllCardsFromBank(BankAccount bankAccount);
    Card find(long id);
    Card findByName(String name);
    void deleteCard(long id);
    void deleteCard(Card card);
}
