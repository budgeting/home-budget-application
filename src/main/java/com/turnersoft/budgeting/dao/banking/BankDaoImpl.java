package com.turnersoft.budgeting.dao.banking;


import com.turnersoft.budgeting.dao.JpaTemplateImpl;
import com.turnersoft.budgeting.dao.banking.interfaces.BankDao;
import com.turnersoft.budgeting.model.banking.BankAccount;
import com.turnersoft.budgeting.model.billing.DebtAccount;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

@Repository
public class BankDaoImpl  extends JpaTemplateImpl implements BankDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public void saveBankAccount(BankAccount account) {
        save(account);
    }

    @Override
    public List<BankAccount> findAll(long personalAccId) {
        Session session = sessionFactory.openSession();

        List<BankAccount> accounts = (List<BankAccount>) session.getNamedQuery("BankAccount.findActiveAccounts")
                .setParameter("personalAccId", personalAccId).getResultList();
        session.close();

        return accounts;
    }

    private List<BankAccount> findAll() {
        Session session = sessionFactory.openSession();

        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<BankAccount> criteria = builder.createQuery(BankAccount.class);
        criteria.from(BankAccount.class);

        List<BankAccount> bankAccounts = session.createQuery(criteria).getResultList();

        session.close();

        return bankAccounts;
    }

    @Override
    public BankAccount findById(long id) {
        return (BankAccount) find(id, BankAccount.class);
    }

    @Override
    public BankAccount findByName(String name, long personalAccId) {
        Session session = sessionFactory.openSession();
        BankAccount bankAccount = (BankAccount) session.getNamedQuery("BankAccount.findByName")
                .setParameter("name", name).setParameter("personalAccId", personalAccId)
                .getSingleResult();
        session.close();

        return  bankAccount;
    }

    @Override
    public void deleteAccount(long id) {
        deleteAccount(findById(id));
    }

    @Override
    public void deleteAccount(BankAccount account) {
        delete(account);
    }


}
