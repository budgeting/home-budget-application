package com.turnersoft.budgeting.dao.banking.interfaces;

import com.turnersoft.budgeting.model.banking.BankAccount;



import com.turnersoft.budgeting.model.banking.BankAccount;

import java.util.List;

public interface BankDao {
    void saveBankAccount(BankAccount account);
    void deleteAccount(BankAccount account);
    List<BankAccount> findAll(long personalAccId);
    BankAccount findById(long id);
    BankAccount findByName(String name, long personalAccId);
    void deleteAccount(long id);
}
