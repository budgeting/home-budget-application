package com.turnersoft.budgeting.dao.banking;


import com.turnersoft.budgeting.dao.JpaTemplateImpl;
import com.turnersoft.budgeting.dao.banking.interfaces.BankDao;
import com.turnersoft.budgeting.dao.banking.interfaces.CardDao;
import com.turnersoft.budgeting.model.banking.BankAccount;
import com.turnersoft.budgeting.model.banking.Card;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CardDaoImpl extends JpaTemplateImpl implements CardDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public void saveCard(Card card) {
        save(card);
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public List<Card> findAllCardsFromBank(BankAccount bankAccount) {

        Session session = sessionFactory.openSession();
        List<Card> cards = session.getNamedQuery("Card.findByBank")
                .setParameter("bankId", bankAccount)
                .getResultList();
        session.close();

        return cards;
    }

    @Override
    public Card find(long id) {
        return (Card) find(id, Card.class);
    }

    @Override
    public Card findByName(String name) {
        Session session = sessionFactory.openSession();
        Card card = (Card) session.getNamedQuery("Card.findByName")
                .setParameter("name", name).getSingleResult();

        return card;
    }

    @Override
    public void deleteCard(long id) {
        deleteCard(find(id));
    }

    @Override
    public void deleteCard(Card card) {
        delete(card);
    }

}
