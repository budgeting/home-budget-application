package com.turnersoft.budgeting.dao;


import com.turnersoft.budgeting.model.PersonalAccount;
import com.turnersoft.budgeting.model.billing.BillingAccount;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PersonalAccountDaoImpl extends  JpaTemplateImpl implements  PersonalAccountDao{

    @Autowired
    SessionFactory session;


    @Override
    public void saveAccount(PersonalAccount account) {
        save(account);
    }

    @Override
    public PersonalAccount findAccount(long id) {
        return (PersonalAccount) find(id, PersonalAccount.class);
    }

    @Override
    @SuppressWarnings("unchecked")
    public PersonalAccount findByEmail(String email) {
        Session session = sessionFactory.openSession();
        System.out.println("Email: " + email);
        List<PersonalAccount> accounts = (List<PersonalAccount>) session.getNamedQuery("PersonalAccount.findByEmail").
                setParameter("email", email).list();

        session.close();

        return accounts != null && !accounts.isEmpty() ? accounts.get(0) : null;
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean doesAccountExist(String email) {
        Session session = sessionFactory.openSession();
        List<Long> accounts = (List<Long>) session.createQuery("select a.accountId from PersonalAccount a where a.email = :email").setParameter("email", email).getResultList();
        session.close();

        return accounts != null && !accounts.isEmpty();
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean doesAccountExist(long id) {
        Session session = sessionFactory.openSession();
        List<Long> accounts = (List<Long>) session.createQuery("select a.accountId from PersonalAccount a where a.accountId = :accountId").setParameter("accountId", id).getResultList();
        session.close();

        return accounts != null && !accounts.isEmpty();
    }

    @Override
    public void deleteAccount(long id) {
        delete(findAccount(id));
    }
}
