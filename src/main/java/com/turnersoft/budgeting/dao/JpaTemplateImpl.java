package com.turnersoft.budgeting.dao;


import com.turnersoft.budgeting.model.AuditFields;
import com.turnersoft.budgeting.model.banking.BankAccount;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;



@Component
public class JpaTemplateImpl implements JpaTemplate {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public void save(Object object) {
        Session session = startTransaction();
        session.saveOrUpdate(object);
        closeAndCommit(session);
    }

    @Override
    public Object find(long id, Class classItem) {
        Session session = sessionFactory.openSession();
        Object object = session.get(classItem, id);
        session.close();
        return object;
    }

    @Override
    public void delete(Object object) {
        Session session = startTransaction();
        session.delete(object);
        closeAndCommit(session);
    }

    @Override
    public void deactivate(AuditFields object) {
        if(object != null) {
            object.deactivate();
            save(object);
        }

    }

    private Session startTransaction() {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        return  session;
    }
    private void closeAndCommit(Session session) {
        session.getTransaction().commit();
        session.close();
    }



}
