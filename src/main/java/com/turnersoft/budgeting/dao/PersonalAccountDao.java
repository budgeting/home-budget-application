package com.turnersoft.budgeting.dao;


import com.turnersoft.budgeting.model.PersonalAccount;

public interface PersonalAccountDao {
    public void saveAccount(PersonalAccount account);
    public PersonalAccount findAccount(long id);
    public boolean doesAccountExist(String email);
    public boolean doesAccountExist(long id);
    public PersonalAccount findByEmail(String email);
    public void deleteAccount(long id);
}
