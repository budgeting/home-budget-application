package com.turnersoft.budgeting.validation;


import com.turnersoft.budgeting.domain.request.accounts.AccountReq;
import com.turnersoft.budgeting.domain.response.Response;
import com.turnersoft.budgeting.service.PersonalAccountService;
import com.turnersoft.budgeting.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AccountValidator {

    @Autowired
    PersonalAccountService personalAccountService;

    public void validateAccountReq(AccountReq req, Response response) {
        if(Utils.isEmpty(req.getName())) {
            response.setErrorMessage("Account Name is null or invalid");
            return;
        }

        if(Utils.isEmpty(req.getPassword())) {
            response.setErrorMessage("Account Name is null or invalid");
            return;
        }

        if(Utils.isEmpty(req.getUserName())) {
            response.setErrorMessage("UserName is null or invalid");
            return;
        }


    }
}
