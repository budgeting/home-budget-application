package com.turnersoft.budgeting.validation.personal;


import com.turnersoft.budgeting.domain.request.accounts.banking.BankAccountReq;
import com.turnersoft.budgeting.domain.request.accounts.personal.PersonalAccountReq;
import com.turnersoft.budgeting.domain.response.Response;
import com.turnersoft.budgeting.service.PersonalAccountService;
import com.turnersoft.budgeting.utils.Utils;
import com.turnersoft.budgeting.validation.AccountValidator;
import com.turnersoft.budgeting.web.utils.ControllerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("personalAccountValidator")
public class PersonalAccountValidator extends AccountValidator {

    @Autowired
    PersonalAccountService personalAccountService;

    public Response validate(PersonalAccountReq request) {
        Response response = new Response();

        if(request != null) {

            if(Utils.isEmpty(request.getName())) {
                response.setErrorMessage("Account Name is null or invalid");
                return response;
            }

            if(Utils.isEmpty(request.getPassword())) {
                response.setErrorMessage("Account Name is null or invalid");
                return response;
            }

            if(ControllerUtils.isSuccessful(response)) {

                if(request.getAccountId() == 0L && personalAccountService.doesAccountExist(request.getEmail())) {
                    response.setErrorMessage("An account of this e-mail address already exists.");
                }
            }

        } else {
            response.setErrorMessage("Null Request for Personal Account");
        }

        return response;
    }

    public Response validatePassword(PersonalAccountReq req) {
        Response response = new Response();

        if(Utils.isEmpty(req.getPassword())) {
            response.setErrorMessage("Password cannot be empty or null");
        }

        return response;
    }


}
