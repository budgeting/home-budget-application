package com.turnersoft.budgeting.validation.Banking.card;

import com.turnersoft.budgeting.domain.request.accounts.banking.CardReq;
import com.turnersoft.budgeting.domain.response.Response;
import com.turnersoft.budgeting.model.banking.BankAccount;
import com.turnersoft.budgeting.model.banking.constants.CardType;


public interface CardValidation {

    public Response validateCardType(BankAccount bankAccount, CardReq cardReq, CardType cardType);
}
