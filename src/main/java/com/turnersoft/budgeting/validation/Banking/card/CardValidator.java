package com.turnersoft.budgeting.validation.Banking.card;

import com.turnersoft.budgeting.domain.request.accounts.banking.CardReq;
import com.turnersoft.budgeting.domain.response.Response;
import com.turnersoft.budgeting.model.banking.BankAccount;
import com.turnersoft.budgeting.model.banking.constants.CardType;
import com.turnersoft.budgeting.service.banking.interfaces.BankService;
import com.turnersoft.budgeting.utils.Utils;
import com.turnersoft.budgeting.web.utils.ControllerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("cardValidator")
public abstract class CardValidator implements CardValidation{

    @Autowired
    BankService bankService;

    public Response validateCardRequest(CardReq request) {
        Response response = new Response();

        if(request != null) {
            validateRequestItems(response, request);

            if(ControllerUtils.isSuccessful(response)) {
                BankAccount bankAccount = bankService.findBankAccount(request.getBankAccId());

                if(bankAccount == null) {
                    response.setErrorMessage("Unable to find banking information");
                    return response;
                }

                try {
                    CardType cardType = CardType.valueOf(request.getType());

                    if(cardType == null) {
                        response.setErrorMessage("The card type specified is invalid.");
                        return  response;
                    }

                    response = validateCardType(bankAccount, request, cardType);


                } catch(IllegalArgumentException exe) {
                    response.setErrorMessage("The Card Type Specified is not a valid option. Please choose between DEBIT, or CREDIT");
                }

            }
        }

        return response;
    }


    private void validateRequestItems(Response response, CardReq cardReq) {
        if(Utils.isEmpty(cardReq.getName())) {
            response.setErrorMessage("Card Name is empty or invalid.");
            return;
        }

        if(Utils.isEmpty(cardReq.getFullName())) {
            response.setErrorMessage("Full Name is empty or invalid.");
            return;
        }

        if(Utils.isEmpty(cardReq.getCardNumber())) {
            response.setErrorMessage("Card Number is empty or invalid.");
            return;        }

        if(Utils.isEmpty(cardReq.getCvv())) {
            response.setErrorMessage("CVV is empty or invalid.");
            return;        }

        if(Utils.isEmpty(cardReq.getExpirationDate())) {
            response.setErrorMessage("Card expiration date is empty or invalid.");
            return;        }

        if(Utils.isEmpty(cardReq.getVendor())) {
            response.setErrorMessage("Card Vendor is empty or invalid.");
            return;        }

        if(Utils.isEmpty(cardReq.getType())) {
            response.setErrorMessage("Card Type is empty or invalid.");
            return;        }


    }


}
