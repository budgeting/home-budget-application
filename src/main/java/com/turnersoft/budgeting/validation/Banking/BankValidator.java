package com.turnersoft.budgeting.validation.Banking;

import com.turnersoft.budgeting.domain.request.accounts.banking.BankAccountReq;
import com.turnersoft.budgeting.domain.response.Response;
import com.turnersoft.budgeting.service.PersonalAccountService;
import com.turnersoft.budgeting.utils.Utils;
import com.turnersoft.budgeting.validation.AccountValidator;
import com.turnersoft.budgeting.web.utils.ControllerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("bankValidator")
public class BankValidator extends AccountValidator {

    @Autowired
    PersonalAccountService personalAccountService;

    public Response validate(BankAccountReq request) {
        Response response = new Response();

        if(request != null) {

            if(!personalAccountService.doesAccountExist(request.getPersonalAccountId())) {
                response.setErrorMessage("Personal Account is not specified.");
                return response;
            }

            validateAccountReq(request, response);


        } else {
            response.setErrorMessage("Null Request for Bank Account");
        }

        return response;
    }

}
