package com.turnersoft.budgeting.validation.Banking.card;


import com.turnersoft.budgeting.domain.request.accounts.banking.CardReq;
import com.turnersoft.budgeting.domain.response.Response;
import com.turnersoft.budgeting.model.banking.BankAccount;
import com.turnersoft.budgeting.model.banking.Card;
import com.turnersoft.budgeting.model.banking.constants.CardType;
import com.turnersoft.budgeting.utils.Utils;
import org.springframework.stereotype.Component;

@Component("newCardValidator")
public class NewCardValidator extends CardValidator {


    @Override
    public Response validateCardType(BankAccount bankAccount, CardReq request, CardType cardType) {
        Response response = new Response();
        if(hasCardType(bankAccount, cardType)) {
            response.setErrorMessage(bankAccount.getName() + " already has a " + cardType.getDisplay() + " card associated.");
            return  response;
        }

        return response;

    }


    private boolean hasCardType(BankAccount account, CardType cardType) {
        boolean hasCardType = false;
        if(Utils.hasItems(account.getAccountCards())) {

            for(Card card : account.getAccountCards()) {
                if(card.getCardType().equals(cardType)) {
                    hasCardType = true;
                    break;
                }
            }
        }

        return hasCardType;
    }
}
