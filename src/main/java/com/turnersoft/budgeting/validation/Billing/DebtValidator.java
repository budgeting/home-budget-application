package com.turnersoft.budgeting.validation.Billing;


import com.turnersoft.budgeting.domain.request.accounts.billing.BillingAccountReq;
import com.turnersoft.budgeting.domain.request.accounts.billing.DebtAccountReq;
import com.turnersoft.budgeting.domain.request.accounts.billing.PaymentAccountReq;
import com.turnersoft.budgeting.domain.response.Response;
import com.turnersoft.budgeting.model.banking.constants.TimeOfMonth;
import com.turnersoft.budgeting.service.PersonalAccountService;
import com.turnersoft.budgeting.utils.Utils;
import com.turnersoft.budgeting.validation.AccountValidator;
import com.turnersoft.budgeting.web.utils.ControllerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("debtValidator")
public class DebtValidator extends AccountValidator {

    @Autowired
    PersonalAccountService personalAccountService;

    public Response validate(BillingAccountReq request) {
        Response response = new Response();

        if(request != null) {
            validatePaymentRequestItems(response, request);
        } else {
            response.setErrorMessage("Null Request for Debt Account");
        }


        return response;
    }

    public Response validate(DebtAccountReq request) {
        Response response = new Response();

        if(request != null) {
            validateRequestItems(response, request);
        } else {
            response.setErrorMessage("Null Request for Debt Account");
        }


        return response;
    }


    private void validateRequestItems(Response response, DebtAccountReq req) {
        validatePaymentRequestItems(response, req);

        if(ControllerUtils.isSuccessful(response)) {
            if(req.getOriginalAmount() < 0 ) {
                response.setErrorMessage("Original Amount is Invalid");
            }
        }

    }

    private void validatePaymentRequestItems(Response response, PaymentAccountReq req) {
        validateAccountReq(req, response);

        if(ControllerUtils.isSuccessful(response)) {

            if(!personalAccountService.doesAccountExist(req.getPersonalAccountId())) {
                response.setErrorMessage("Personal Account is not specified.");
                return;

            } else {
                if(Utils.isEmpty(req.getUrl())) {
                    response.setErrorMessage("URL IS null or invalid");
                    return;
                }

                if(Utils.isEmpty(req.getTimeOfPayment())) {
                    response.setErrorMessage("Time Of Payment IS null or invalid");
                    return;

                } else {
                    try {
                        TimeOfMonth month = TimeOfMonth.valueOf(req.getTimeOfPayment());

                    } catch(Exception exe) {
                        response.setErrorMessage("Time Of Payment must be: FIRST, or SECOND");
                    }

                }
            }


        }


    }


}
