package com.turnersoft.budgeting.validation.Billing;


import com.turnersoft.budgeting.domain.request.billingCycle.BillPaymentReq;
import com.turnersoft.budgeting.domain.request.billingCycle.BillingCycleReq;
import com.turnersoft.budgeting.domain.request.billingCycle.DebtPaymentReq;
import com.turnersoft.budgeting.domain.request.billingCycle.PayBillingCycleReq;
import com.turnersoft.budgeting.domain.response.Response;
import com.turnersoft.budgeting.model.PersonalAccount;
import com.turnersoft.budgeting.model.auditing.BillAudit;
import com.turnersoft.budgeting.model.auditing.DebtAudit;
import com.turnersoft.budgeting.model.auditing.PaymentAudit;
import com.turnersoft.budgeting.model.banking.constants.TimeOfMonth;
import com.turnersoft.budgeting.model.billing.DebtAccount;
import com.turnersoft.budgeting.model.billing.constants.Month;
import com.turnersoft.budgeting.service.PersonalAccountService;
import com.turnersoft.budgeting.service.billing.BillingCycleService;
import com.turnersoft.budgeting.utils.Utils;
import com.turnersoft.budgeting.web.utils.ControllerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Calendar;

@Component("billingCycleValidator")
public class BillingCycleValidator {

    @Autowired
    BillingCycleService billingCycleService;

    @Autowired
    PersonalAccountService personalAccountService;

    public Response validateBillingCycleCreation(BillingCycleReq req) {
        Response response = new Response();

        PersonalAccount account = personalAccountService.findAccount(req.getPersonalAccId());

        if(account == null) {
            response.setErrorMessage("Account specified does not exist");
            return response;
        }

        if(req.getYear() <  Calendar.getInstance().get(Calendar.YEAR)) {
            response.setErrorMessage("The year specified has already passed");
            return response;

        }

        validateMonth(response, req.getMonth());

        if(ControllerUtils.isSuccessful(response)) {

            if(billingCycleService.retrieveMonthlyBillingCycle(req.getPersonalAccId(), req.getMonth(), req.getYear()) != null) {
                response.setErrorMessage("Billing Cycle for this month already exists");
            }
        }


        return response;
    }

    public Response validateBillPayment(BillPaymentReq request) {
        Response response = new Response();

        BillAudit billAudit = billingCycleService.findBillAudit(request.getBillAuditId());

        if(billAudit.isComplete()) {
            response.setErrorMessage("You have already paid this bill for this month.");
        }

        validatePaymentAudit(response, billAudit, request.getPersonalAccId());

        return response;

    }

    public Response validateDebtPayment(DebtPaymentReq request) {
        Response response = new Response();


        if(request.getAmount() < 0) {
            response.setErrorMessage("The amount specified is not valid");
            return  response;
        }

        DebtAudit debtAudit = billingCycleService.findDebtAudit(request.getDebtAuditId());

        if(ControllerUtils.isSuccessful(validatePaymentAudit(response, debtAudit , request.getPersonalAccId()))) {
            DebtAccount debtAccount = debtAudit.getDebtAccount();

            if(request.getAmount() > debtAccount.getTotalRemaining()) {
                response.setErrorMessage("You don't need to pay this much! You only owe $" + debtAccount.getTotalRemaining());
            }
        }

        return response;

    }

    private Response validatePaymentAudit(Response response, PaymentAudit paymentAudit, long personalAccId) {
        PersonalAccount personalAccount = personalAccountService.findAccount(personalAccId);

        if(personalAccount == null) {
            response.setErrorMessage("Account specified is invalid");
            return response;

        } else if(paymentAudit == null) {
            response.setErrorMessage("Payment Audit information could not be found.");
            return  response;

        } if  (paymentAudit.getPersonalAccId() != personalAccId ) {
            response.setErrorMessage("The account specified is not authorized to make this payment");
            return  response;
        }

        return  response;
    }


    public Response validateBillingCyclePayment(PayBillingCycleReq request) {
        Response response = new Response();

        if( personalAccountService.findAccount(request.getPersonalAccId()) == null) {
            response.setErrorMessage("Account specified is invalid");
            return response;
        }

        validateTimeOfMonth(response, request.getTimeOfMonth());

        return response;

    }


    private void validateMonth(Response response, String request) {
        Month requestValue = null;

        for(Month dateMonth : Month.values()) {

            if(Utils.isEqual(request, dateMonth.getValue())) {
                requestValue = dateMonth;
                break;
            }
        }

        if(requestValue == null) {
            response.setErrorMessage("Month specified is invalid");
        }

    }

    private void validateTimeOfMonth(Response response, String timeOfMonth) {
        try {
            TimeOfMonth month = TimeOfMonth.valueOf(timeOfMonth);

        } catch(Exception exe) {
            response.setErrorMessage("Time Of Payment must be: FIRST, or SECOND");
        }
    }
}
