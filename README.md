# README #

*Summary*
Budget Application used to organize and track spending patterns, debts, and personal accounts
 1.0-SNAPSHOT

*Local Hibernate Database*
From the root level of the project, use the following command in cmd to pull up hibernate's database console:
Command: [java -jar h2-1.4.190.jar]


*Development*
IDE: InteliJ
Run app: From Gradle window use the Spring bootRun plugin
Run on command line: (From Root of the project: gradle bootRun)
Build: gradle build
Clean Build: gradle clean build

